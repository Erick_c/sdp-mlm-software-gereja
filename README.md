# Sudah Commit dengan Git Bash
Erick, Samuel, Timotius, Mardhan

# Set Apache 
Setting pada apache httpd.conf ubah
```
DocumentRoot "C:/xampp/htdocs/projectGereja"
<Directory "C:/xampp/htdocs/projectGereja">
    #
    # Possible values for the Options directive are "None", "All",
    # or any combination of:
    #   Indexes Includes FollowSymLinks SymLinksifOwnerMatch ExecCGI MultiViews
    #
    # Note that "MultiViews" must be named *explicitly* --- "Options All"
    # doesn't give it to you.
    #
    # The Options directive is both complicated and important.  Please see
    # http://httpd.apache.org/docs/2.4/mod/core.html#options
    # for more information.
    #
    Options Indexes FollowSymLinks Includes ExecCGI

    #
    # AllowOverride controls what directives may be placed in .htaccess files.
    # It can be "All", "None", or any combination of the keywords:
    #   AllowOverride FileInfo AuthConfig Limit
    #
    AllowOverride All

    #
    # Controls who can get stuff from this server.
    #
    Require all granted
</Directory>
```
# Link-link yang dapat diakses
```
$route['beranda']='home';
$route['beranda/acara-khusus']='acara';
$route['beranda/acara-khusus/(:any)']='acara/detailkegiatan/$1';

$route['beranda/kegiatan-rutin']='cabang';
$route['beranda/kegiatan-rutin/cabang']='cabang';
$route['beranda/kegiatan-rutin/cabang/(:any)']='kegiatan/index/$1';

$route['beranda/pelayanan']='pelayanan';
$route['beranda/pelayanan/beranda-admin']='pelayanan/berandaadmin';
$route['beranda/pelayanan/beranda-admin/super-admin']='pelayanan/superadmin';
$route['beranda/pelayanan/beranda-admin/super-admin/profilpengguna']='pelayanan/superadmin';
$route['beranda/pelayanan/beranda-admin/super-admin/mst_keuangan/laporan']='keuangan/laporan';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-cabang']='pelayanan/mstcabang';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-cabang/data-baru']='cabang/insert';
$route['beranda/pelayanan/beranda-admin/keuangan']='pelayanan/mstkeuangan';
$route['beranda/pelayanan/masuk-pelayanan']='pelayanan/login';
```