<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'acara';
$route['404_override'] = 'app_errors';
$route['translate_uri_dashes'] = FALSE;

//yang lama, jika berubah pikiran
//$route['beranda']='home';
//$route['beranda/acara-khusus']='acara';
////biar bisa paging
//$route['beranda/acara-khusus/(:any)']='acara/index/$1';
//$route['beranda/acara-khusus/detail/(:any)']='acara/detailkegiatan/$1';//sebelumnya mengganggu paging
////jangan lupa index cabang dan kegiatan di comment
//$route['beranda/kegiatan-rutin']='cabang';
//$route['beranda/kegiatan-rutin/cabang']='cabang';
//$route['beranda/kegiatan-rutin/cabang/(:any)']='kegiatan/index/$1';

$route['beranda']='acara';
$route['beranda/(0|\d+0)']='acara/index/$1';
$route['beranda/detail/(K+[0-9]+[0-9]+[0-9]+[0-9])']='acara/detailkegiatan/$1';//sebelumnya mengganggu paging

$route['beranda/pelayanan']='pelayanan';
$route['beranda/pelayanan/beranda-admin']='pelayanan/berandaadmin';
$route['beranda/pelayanan/beranda-admin/logOut']='pelayanan/logOut';
$route['beranda/pelayanan/beranda-admin/super-admin']='pelayanan/superadmin';

$route['beranda/pelayanan/beranda-admin/super-admin/mst-ibadah-umum']='jemaat/mst_ibadah_umum';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-absensi/laporan']='jemaat/laporan_ibadah_umum';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-absensi/laporan/(:any)']='jemaat/detail_laporan_ibadah_umum/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-absensi/(:any)']='jemaat/absensi_ibadah_umum/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-absensi-ic']='ic/absensi';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-absensi-ic/laporan-absensi-ic/(:any)']='ic/laporan_absensi/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-absensi-ic/laporan-absensi-ic/insert/(:any)']='ic/insert_kehadiran_absensi_ic/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-absensi-ic/laporan-absensi-ic/delete/(:any)']='ic/delete_kehadiran_absensi_ic/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-absensi-ic/data-baru']='ic/insertabsensi';

$route['beranda/pelayanan/beranda-admin/super-admin/profilpengguna']='pelayanan/superadmin';

$route['beranda/pelayanan/beranda-admin/super-admin/mst-kategori']='pelayanan/mstkategori';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-kategori/data-baru']='kategori_kegiatan/insert';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-kategori/update/(:any)']='kategori_kegiatan/update/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-kategori/delete/(:any)']='kategori_kegiatan/delete/$1';

$route['beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan']='pelayanan/mstkegiatan';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan/detail/(:any)']='kegiatan/detail/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan/data-baru']='kegiatan/insert';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan/update/(:any)']='kegiatan/update/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan/delete/(:any)']='kegiatan/delete/$1';


//bagian master keuangan
$route['beranda/pelayanan/beranda-admin/super-admin/mst-keuangan']='keuangan';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-keuangan/data-baru']='keuangan/insert';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-keuangan/update/(:any)']='keuangan/update/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-keuangan/laporan']='keuangan/laporan';

$route['beranda/pelayanan/beranda-admin/super-admin/mst-inventaris']='pelayanan/mstinventaris';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/list-peminjaman/update']='inventaris/list_pengembalian';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/list-peminjaman']='inventaris/list_peminjaman';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/peminjaman']='inventaris/peminjaman';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/data-baru']='inventaris/insert';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/delete/(:any)']='inventaris/delete/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/update/(:any)']='inventaris/update/$1';
//sas
$route['beranda/pelayanan/beranda-admin/super-admin/mst-jemaat']='pelayanan/mstjemaat';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-jemaat/detail/(:any)']='jemaat/detail/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-jemaat/data-baru']='jemaat/insert';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-jemaat/update/(:any)']='jemaat/update/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-jemaat/inserthubkel/(:any)']='jemaat/inserthubkel/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-jemaat/deletehubkel/(:any)/(:any)']='jemaat/deletehubkel/$1/$2';

$route['beranda/pelayanan/beranda-admin/super-admin/mst-pembicara']='pelayanan/mstpembicara';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-pembicara/data-baru']='pembicara/insert';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-pembicara/update/(:any)']='pembicara/update/$1';

$route['beranda/pelayanan/beranda-admin/super-admin/mst-maintenance']='pelayanan/mstmaintenance';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-maintenance/data-baru']='maintenance/insert';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-maintenance/update/(:any)']='maintenance/update/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-maintenance/update2/(:any)']='maintenance/update_lewat_master/$1';
//sas
$route['beranda/pelayanan/beranda-admin/super-admin/mst-ic']='pelayanan/mstic';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-ic/data-baru']='ic/insert';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-ic/delete/(:any)']='ic/delete/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-ic/update/(:any)']='ic/update/$1';

$route['beranda/pelayanan/beranda-admin/super-admin/mst-cabang']='pelayanan/mstcabang';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-cabang/data-baru']='cabang/insert';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-cabang/delete/(:any)']='cabang/delete/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-cabang/update/(:any)']='cabang/update/$1';
//$route['beranda/pelayanan/beranda-admin/keuangan']='pelayanan/mstkeuangan';
$route['beranda/pelayanan/masuk-pelayanan']='pelayanan/login';


//////////////////////////////////////////////////////////

$route['beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan']='pelayanan/pelayanan_ibadah';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-penugasan/delete/(:any)/(:any)']='pelayanan/list_penugasan_delete/$1/$2';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-penugasan/insert/(:any)']='pelayanan/list_penugasan_insert/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-penugasan/(:any)']='pelayanan/list_penugasan/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-pelayanan/insert/(:any)']='pelayanan/insert_petugas/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-pelayanan/delete/(:any)/(:any)']='pelayanan/delete_petugas_pelayanan/$1/$2';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-pelayanan/(:any)']='pelayanan/list_tugas_pelayanan/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/insert-pelayanan']='pelayanan/insert_pelayanan';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/update-pelayanan/(:any)']='pelayanan/update_pelayanan/$1';
$route['beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/(:any)']='pelayanan/pelayanan_ibadah/$1';


$route['beranda/pelayanan/beranda-admin/super-admin/profilpengguna/(:any)']='pelayanan/pengingat_pelayanan/$1';
