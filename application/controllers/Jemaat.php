<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jemaat extends CI_Controller {
	public function detail_laporan_ibadah_umum($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else{
			$this->load->model('gereja');
			// $this->gereja->select_ibadah_umum();
			$data_chart 				   = $this->gereja->chart_select_absensi(['daftar_kehadiran.id_kegiatan'=>$id]);
			$daftar_orang_yang_hadir 	   = $this->gereja->select_absensi_yang_hadir($id);
			$daftar_orang_yang_tidak_hadir = $this->gereja->select_absensi_yang_tidak_hadir($id);
			$this->load->view('detail_laporan_ibadah_umum',['data_chart'=>$data_chart,'data_hadir'=>$daftar_orang_yang_hadir,'data_tidak_hadir'=>$daftar_orang_yang_tidak_hadir]);
		}
	}
	public function laporan_ibadah_umum()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else{
			date_default_timezone_set("Asia/Bangkok");
			$this->load->model('gereja');
			$data = $this->gereja->select_kegiatan_untuk_absensi(['substr(tanggal_kegiatan,6,2)'=>date('m'),'substr(tanggal_kegiatan,1,4)'=>date('Y'),'tanggal_kegiatan <='=>date('Y-m-d')]);
			$data_chart = $this->gereja->chart_select_absensi(['substr(tanggal_kegiatan,6,2)'=>date('m'),'substr(tanggal_kegiatan,1,4)'=>date('Y'),'tanggal_kegiatan <='=>date('Y-m-d')]);
			$this->load->view('laporan_absensi_umum',['data'=>$data,'data_chart'=>$data_chart]);
		}
	}
	public function mst_ibadah_umum(){		
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Absensi'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			date_default_timezone_set('asia/bangkok');
			$data = $this->gereja->cekKegiatanIbadahUmumSesuaiTGL(date('y-m-d'),date('H:i:m'));
			if ($data != null) {
				$_SESSION['ID_JEMAAT']='';
				$_SESSION['TANGGAL_LAHIR']='';
				$_SESSION['ALAMAT_JEMAAT']='Tidak ada';
				$_SESSION['FOTO']='';
				$_SESSION['NAMA_JEMAAT']='Jemaat Gereja';
				$_SESSION['NAMA_PELAYANAN']='Admin Absensi';
				redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/mst-absensi/'.$data));
			}
			else {				
				$this->load->view('absensi_ibadah_umum_belum_waktunya',['data'=>[]]);
			}
		}
	}
	public function absensi_ibadah_umum($id){
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));
		//Untuk Admin Jemaat dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Absensi'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			if (count($post)>0) {
				$id_jemaat = $post['id_jemaat'];
				$data = $this->gereja->select_jemaat(["id_jemaat"=>$id_jemaat]);
				
				date_default_timezone_set("Asia/Bangkok");
				if (count($data) > 0) $this->gereja->insert_absensi(
					['id_kegiatan'=>$id,'id_jemaat'=>$id_jemaat,'jam_datang'=>date('H:i:sa')]
				);
				return $this->load->view('absensi_ibadah_umum', ["data"=>$data]);
			}
			
			$this->load->view('absensi_ibadah_umum');
		}
	}
	public function detail($id_jemaat)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));		
		//Untuk Admin Jemaat dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else{
			$this->load->model('gereja');
			$data = $this->gereja->select_jemaat(['id_jemaat'=>$id_jemaat]);
			$data_hubkel = $this->gereja->select_hubkel($id_jemaat);
			if (count($data)==0) redirect('/beranda/pelayanan');
			$this->load->view('mst_detail_jemaat', ['data'=>$data,'data_hubkel'=>$data_hubkel]);
		}
	}
	public function insert()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));		
		//Untuk Admin Jemaat dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$post['foto']=$_FILES['foto']['tmp_name'];

				$sukses = $this->gereja->insert_jemaat($post);
				// var_dump($_FILES);
				if ($sukses == true) $pesan="<script> alert('Data telah tersimpan !!'); </script>"; 
			}
			$data_ic= $this->gereja->select_ic();
			$this->load->view('insert_jemaat',['data_ic'=>$data_ic,'pesan'=>$pesan]);
		}
	}
	
	public function update($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				//$post['foto']=$_FILES['foto']['tmp_name'];
				$sukses = $this->gereja->update_jemaat($post,$id);
				if ($sukses == true) 
					redirect('beranda/pelayanan/beranda-admin/super-admin/mst-jemaat');
				else $pesan="<script> alert('Data gagal tersimpan !!'); </script>"; 
			}
			
			$this->load->model('gereja');
			$data = $this->gereja->select_jemaat3($id);
			$data_ic= $this->gereja->select_ic();
			$data_cabang = $this->gereja->select_cabang();
			if (count($data) > 0)
			{
				$data_default = $data;
				$this->load->view('update_jemaat',['data_default'=>$data_default,'pesan'=>$pesan,'data_ic'=>$data_ic,'data_cabang'=>$data_cabang,'data'=>$data]);

			}
			else
			{
				redirect('beranda/pelayanan/beranda-admin/super-admin/mst-jemaat');
			}
		}
	}

	public function inserthubkel($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				try{
					$sukses = $this->gereja->insert_hubkel($post,$id);

				if ($sukses == true) {
					if ($post['status']==='Suami'){
						$id2 = $post['id_jemaat_bersangkutan'];
						$post['id_jemaat_bersangkutan']=$id;
						$post['status']= 'Istri';
						$this->gereja->insert_hubkel($post,$id2);
					}
					else if ($post['status']==='Istri'){
						$id2 = $post['id_jemaat_bersangkutan'];
						$post['id_jemaat_bersangkutan']=$id;
						$post['status']= 'Suami';
						$this->gereja->insert_hubkel($post,$id2);
					}
					else if ($post['status']==='Anak'){
						$id2 = $post['id_jemaat_bersangkutan'];
						$post['id_jemaat_bersangkutan']=$id;
						$post['status']= 'Orang Tua';
						$this->gereja->insert_hubkel($post,$id2);
					}
					else if ($post['status']==='Orang Tua'){
						$id2 = $post['id_jemaat_bersangkutan'];
						$post['id_jemaat_bersangkutan']=$id;
						$post['status']= 'Anak';
						$this->gereja->insert_hubkel($post,$id2);
					}
					else if ($post['status']==='Saudara'){
						$id2 = $post['id_jemaat_bersangkutan'];
						$post['id_jemaat_bersangkutan']=$id;
						$this->gereja->insert_hubkel($post,$id2);
					}
					redirect('beranda/pelayanan/beranda-admin/super-admin/mst-jemaat/inserthubkel/'.$id);
				}	
					
				else {$pesan="<script> alert('Data gagal tersimpan !!'); </script>"; }
				}
				catch(Exception $e){
					echo "<script> alert('Data gagal tersimpan !!'); </script>";
					redirect('beranda/pelayanan/beranda-admin/super-admin/mst-jemaat/inserthubkel/'.$id);
				}
				
			}
			
			$this->load->model('gereja');
			$data = $this->gereja->select_jemaat3($id);
			$data_ic= $this->gereja->select_ic();
			$data_jemaat= $this->gereja->select_jemaat4($id);
			$data_cabang = $this->gereja->select_cabang();
			$data_hubkel = $this->gereja->select_hubkel($id);
			$data_id = $id;
			if (count($data) > 0)
			{
				$data_default = $data;
				$this->load->view('insert_hubkel',['data_hubkel'=>$data_hubkel,'data_default'=>$data_default,'pesan'=>$pesan,'data_ic'=>$data_ic,'data_jemaat'=>$data_jemaat,'data_id'=>$data_id,'data_cabang'=>$data_cabang,'data'=>$data]);
			}
			else
			{
				redirect('beranda/pelayanan/beranda-admin/super-admin/mst-jemaat');
			}
		}
	}

	public function deletehubkel($id, $id2)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$sukses = $this->gereja->delete_hubkel($id,$id2);
			$this->gereja->delete_hubkel($id2,$id);
			if ($sukses == true) $pesan="<script> alert('Data berhasil dihapus !!'); </script>"; 
			redirect('beranda/pelayanan/beranda-admin/super-admin/mst-jemaat/inserthubkel/'.$id2);
		}
	}
	
}