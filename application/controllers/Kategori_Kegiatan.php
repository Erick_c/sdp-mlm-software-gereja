<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_Kegiatan extends CI_Controller {
    public function index()
    {
        $this->load->view('kategori_kegiatan');
    }
	
	public function insert()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}	
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$sukses = $this->gereja->insert_kategori_kegiatan($post);
				if ($sukses == true) $pesan="<script> alert('Data telah tersimpan !!'); </script>"; 
			}
			
			$this->load->view('insert_kategori_kegiatan',['pesan'=>$pesan]);
		}
	}
	
	public function delete($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}	
		else
		{
			$this->load->model('gereja');
			$data = $this->gereja->select_kategori_kegiatan($id);
			if (count($data) > 0) $this->gereja->delete_kategori_kegiatan($id);
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/mst-kategori'));
		}
	}
	
	public function update($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}	
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$sukses = $this->gereja->update_kategori_kegiatan($post,$id);
				if ($sukses == true) 
					redirect('beranda/pelayanan/beranda-admin/super-admin/mst-kategori');
				else $pesan="<script> alert('Data gagal tersimpan !!'); </script>"; 
			}
			
			$this->load->model('gereja');
			$data = $this->gereja->select_kategori_kegiatan($id);
			if (count($data) > 0)
			{
				$this->load->view('update_kategori_kegiatan',['pesan'=>$pesan,'data'=>$data]);

			}
			else
			{
				redirect('beranda/pelayanan/beranda-admin/super-admin/mst-kategori');
			}
		}
	}
}