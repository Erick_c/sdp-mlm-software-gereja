<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_Errors extends CI_Controller {
    public function index($title='Error 404',$data='')
    {
        $this->load->view('error',['title'=>$title,'data'=>$data]);
    }
}