<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelayanan extends CI_Controller {
    public function index()
    {
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));			
		else	
			redirect(base_url('/beranda/pelayanan/beranda-admin'));
    }
    public function Login()
    {
		$post = $this->input->post();
		$pesan = '';
		if (count($post)>0)
		{
			$this->load->model('gereja');

			$post['password'] = md5($post['password']);
			$data = $this->gereja->select_account($post);
			if (count($data)>0)
			{
				$session = ['ID_JEMAAT'=>$data[0]->ID_JEMAAT,'NAMA_JEMAAT'=>$data[0]->NAMA_JEMAAT,'ID_PELAYANAN'=>$data[0]->ID_PELAYANAN,'FOTO'=>$data[0]->FOTO,'NAMA_PELAYANAN'=>$data[0]->NAMA_PELAYANAN,'TANGGAL_LAHIR'=>$data[0]->TANGGAL_LAHIR,'ALAMAT_JEMAAT'=>$data[0]->ALAMAT_JEMAAT,'NO_IC'=>$data[0]->NO_IC];
				for($i=0;$i<count($data);$i++){
					if ((strtolower($data[$i]->NAMA_PELAYANAN) =='super admin')
					||(strtolower($data[$i]->NAMA_PELAYANAN) =='admin kegiatan')
					||(strtolower($data[$i]->NAMA_PELAYANAN) =='admin jemaat')
					||(strtolower($data[$i]->NAMA_PELAYANAN) =='admin inventaris')
					||(strtolower($data[$i]->NAMA_PELAYANAN) =='admin keuangan')
					||(strtolower($data[$i]->NAMA_PELAYANAN) =='ketua ic'))	{			
						$session = ['ID_JEMAAT'=>$data[$i]->ID_JEMAAT,'NAMA_JEMAAT'=>$data[$i]->NAMA_JEMAAT,'ID_PELAYANAN'=>$data[$i]->ID_PELAYANAN,'FOTO'=>$data[$i]->FOTO,'NAMA_PELAYANAN'=>$data[$i]->NAMA_PELAYANAN,'TANGGAL_LAHIR'=>$data[$i]->TANGGAL_LAHIR,'ALAMAT_JEMAAT'=>$data[$i]->ALAMAT_JEMAAT,'NO_IC'=>$data[$i]->NO_IC];
						$i = count($data);
					}
				}
				$this->session->set_userdata($session);
			}
			
			$pesan = 'tidak ada';
		}
		
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session !== null)
		{
			redirect(base_url('/beranda/pelayanan/beranda-admin'));
		}			
		else	
			$this->load->view('Login',['pesan'=>$pesan]);
		/** */
    }
	
	public function logOut()
	{
		$_SESSION=[];
		$data = $this->session->userdata();
		redirect(base_url('beranda/pelayanan'));
	}
	
    public function berandaAdmin()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));			
		else {
			redirect(base_url('/beranda/pelayanan/beranda-admin/super-admin'));
		}
	}
	
	public function superadmin()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));			
		else	
		{
			date_default_timezone_set("Asia/Bangkok");
			
			$this->load->model('gereja');
			
			$post = $this->input->post();
			if (count($post) > 0){
				$post['id_jemaat']=$_SESSION['ID_JEMAAT'];
				$post['id_pelayanan']=$_SESSION['ID_PELAYANAN'];
				$post['password']=md5($post['password']);
				$this->gereja->update_user_password($post);
			}

			$data['jemaat.id_jemaat']=$session;
			$data['kegiatan.tanggal_kegiatan>=']=date('Y-m-d');
			$data = $this->gereja->select_pelayanan_per_account(['jemaat.id_jemaat'=>$_SESSION['ID_JEMAAT'],'tanggal_berakhir_pelayanan'=>null]);
			$data_maintenance = $this->gereja->select_maintenance_penanggung_jawab($_SESSION['ID_JEMAAT']);
			$this->load->view('home_su',['data'=>$data,'data_maintenance'=>$data_maintenance]);
		}
	}
	
    
    public function mstCabang()
    {
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));
		//Untuk Admin Jemaat dan Super Admin saja -----------------------mungkin salah
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else	{
			$this->load->model('gereja');
			$data = $this->gereja->select_cabang();
			$this->load->view('mst_cabang',['data'=>$data]);
		}
	}
	public function mstKegiatan()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));
		//Untuk Admin kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}		
		else	
		{
			$this->load->model('gereja');
			$data = $this->gereja->select_kegiatan();
			$this->load->view('mst_kegiatan',['data'=>$data]);
		}
	}
	public function mstKategori()
	{
		
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));		
		//Untuk Admin Kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else	
		{
			$this->load->model('gereja');
			$data = $this->gereja->select_kategori_kegiatan();
			$this->load->view('mst_kategori',['data'=>$data]);
		}
	}
	public function mstIC()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));
		//Untuk Admin Jemaat dan Super Admin sajaa -----------------------mungkin salah
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat' && $_SESSION['NAMA_PELAYANAN'] !== 'Ketua IC'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else	
		{
			$this->load->model('gereja');
			$data = $this->gereja->select_ic();
			$this->load->view('mst_ic',['data'=>$data]);
		}
	}
	public function mstInventaris()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Inventaris dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Inventaris'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else	
		{
			$this->load->model('gereja');
			$data = $this->gereja->select_inventaris();
			$this->load->view('mst_inventaris',["data"=>$data]);
		}
	}
	public function mstJemaat()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));
		//Untuk Admin Jemaat dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else	
		{
			$this->load->model('gereja');
			$data = $this->gereja->select_jemaat([]);
			$data_ic= $this->gereja->select_ic();
			$this->load->view('mst_jemaat',["data"=>$data]);
		}
	}
	public function mstPembicara()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));
		//Untuk Admin Jemaat dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else	
		{
			$this->load->model('gereja');
			$data = $this->gereja->select_pembicara();
			$this->load->view('mst_pembicara',["data"=>$data]);
		}
	}

	public function mstMaintenance()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));
		//Untuk Admin Inventaris dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Inventaris'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else	
		{
			$this->load->model('gereja');
			$data = $this->gereja->select_maintenance2();
			$this->load->view('mst_maintenance',["data"=>$data]);
		}
	}

	public function pelayanan_ibadah($id=null)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));
		//Untuk Admin Jemaat dan Admin Kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else	
		{
			$this->load->model('gereja');
			$data_pelayanan = $this->gereja->select_pelayanan();
			$data_update_pelayanan = $this->gereja->select_pelayanan(['id_pelayanan'=>$id]);
			$data_kegiatan = $this->gereja->select_kegiatan();
			$this->load->view('mst_pelayanan',[
				"table_pelayanan"=>$data_pelayanan,
				"data_update_pelayanan"=>$data_update_pelayanan,
				"table_kegiatan"=>$data_kegiatan
			]);
		}
	}
	public function insert_pelayanan()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Admin Kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else	
		{
			$this->load->model('gereja');
			$data = $this->input->post();
			$this->gereja->insert_pelayanan($data);
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan'));
		}
	}
	public function update_pelayanan($id='')
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));			
		//Untuk Admin Jemaat dan Admin Kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else	
		{
			$this->load->model('gereja');
			$data = $this->input->post();
			$data['id_pelayanan']=$id;
			$this->gereja->update_pelayanan($data);
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan'));
		}
	}
	public function list_tugas_pelayanan($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Admin Kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else	
		{
			$this->load->model('gereja');
			$data_pelayanan = $this->gereja->select_pelayanan(['id_pelayanan'=>$id]);
			$data_tugas_pelayanan = $this->gereja->select_jemaat_where_tugas_pelayanan(['t.id_pelayanan'=>$id]);
			$data_jemaat = $this->gereja->select_jemaat([]);
			$this->load->view('mst_petugas_pelayanan',[
				"data_pelayanan"=>$data_pelayanan,
				"data_jemaat"=>$data_jemaat,
				"table_tugas_pelayanan"=>$data_tugas_pelayanan
			]);
		}
	}
	public function insert_petugas($id_pelayanan)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));		
		//Untuk Admin Jemaat dan Admin Kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else	
		{
			$this->load->model('gereja');
			$post = $this->input->post();
			$id_jemaat = $post['id_jemaat'];
			$this->gereja->insert_petugas_pelayanan(['id_jemaat'=>$id_jemaat,'id_pelayanan'=>$id_pelayanan]);
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-pelayanan/'.$id_pelayanan));
		}
	}
	public function delete_petugas_pelayanan($id_pelayanan,$id_jemaat)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Admin Kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else	
		{
			$this->load->model('gereja');
			$this->gereja->delete_petugas_pelayanan(['id_jemaat'=>$id_jemaat,'id_pelayanan'=>$id_pelayanan]);
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-pelayanan/'.$id_pelayanan));
		}
	}

	public function list_penugasan($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));			
		else	
		{
			$this->load->model('gereja');
			$data_pelayanan = $this->gereja->select_pelayanan("nama_pelayanan not like '%admin%' and nama_pelayanan not like '%ketua%'");
			$data_kegiatan = $this->gereja->select_kegiatan($id);
			$data_detail_pelayanan = $this->gereja->select_data_detail_pelayanan_kegiatan(['k.id_kegiatan'=>$id]);
			$this->load->view('mst_penugasan_pelayanan',[
				"table_tugas_pelayanan"=>$data_detail_pelayanan,
				"data_pelayanan"=>$data_pelayanan,
				"data_kegiatan"=>$data_kegiatan
			]);
		}
	}

	public function list_penugasan_insert($id_kegiatan)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));			
		else	
		{
			$this->load->model('gereja');
			$post = $this->input->post();
			$post['id_kegiatan']=$id_kegiatan;
			$this->gereja->insert_data_detail_pelayanan($post);
		}
		redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-penugasan/'.$id_kegiatan));
	}
	
	public function list_penugasan_delete($id_kegiatan,$id_pelayanan)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));			
		else	
		{
			$this->load->model('gereja');
			$this->gereja->delete_data_detail_pelayanan(['id_kegiatan'=>$id_kegiatan,'id_pelayanan'=>$id_pelayanan]);
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-penugasan/'.$id_kegiatan));
		}
	}
	public function pengingat_pelayanan($id_kegiatan)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));			
		else	
		{
			$this->load->model('gereja');
			$data['jemaat.id_jemaat']=$session;
			date_default_timezone_set("Asia/Bangkok");
			$data['kegiatan.tanggal_kegiatan>=']=date('Y-m-d');

			$data = $this->gereja->select_pelayanan_per_account(['jemaat.id_jemaat'=>$_SESSION['ID_JEMAAT'],'tanggal_berakhir_pelayanan'=>null]);
			$data_detail_pelayanan = $this->gereja->select_pelayanan_per_account(['jemaat.id_jemaat'=>$_SESSION['ID_JEMAAT'],'kegiatan.id_kegiatan'=>$id_kegiatan,'tanggal_berakhir_pelayanan'=>null]);
			$data_maintenance = $this->gereja->select_maintenance_penanggung_jawab($_SESSION['ID_JEMAAT']);
			$this->load->view('home_su',['data_detail_pelayanan'=>$data_detail_pelayanan,'data'=>$data,'data_maintenance'=>$data_maintenance]);
		}		
	}
}