<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IC extends CI_Controller {
	public function insert_kehadiran_absensi_ic($id_jemaat)
	{
		$id_kegiatan = $this->input->get('id_kegiatan');
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Super Admin sajaa -----------------------mungkin salah
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Ketua IC'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$absensi = $this->gereja->insert_absensi(['id_jemaat'=>$id_jemaat,'id_kegiatan'=>$id_kegiatan]);
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/mst-absensi-ic/laporan-absensi-ic/'.$id_kegiatan));
		}
	}
	public function delete_kehadiran_absensi_ic($id_jemaat)
	{
		$id_kegiatan = $this->input->get('id_kegiatan');
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Super Admin sajaa -----------------------mungkin salah
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Ketua IC'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$absensi = $this->gereja->delete_absensi_ic(['id_jemaat'=>$id_jemaat]);
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/mst-absensi-ic/laporan-absensi-ic/'.$id_kegiatan));
		}
	}
	
	public function laporan_absensi($id_kegiatan)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Super Admin sajaa -----------------------mungkin salah
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Ketua IC'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$data_chart = $this->gereja->chart_select_absensi_ic($_SESSION['NO_IC'],$id_kegiatan);
			$absensi_hadir = $this->gereja->select_absensi_ic_yang_hadir($_SESSION['NO_IC'],$id_kegiatan);
			$absensi_tidak_hadir = $this->gereja->select_absensi_ic_yang_tidak_hadir($_SESSION['NO_IC'],$id_kegiatan);
			$this->load->view('laporan_absensi_ic', ['data_hadir'=>$absensi_hadir,'data_tidak_hadir'=>$absensi_tidak_hadir,'data_chart'=>$data_chart]);
		}
	}
	
	public function absensi()
	{
		
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));
		//Untuk Admin Jemaat dan Super Admin sajaa -----------------------mungkin salah
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Ketua IC'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');

			$data = $this->gereja->select_kegiatan('%%',$_SESSION['NO_IC']);
			$data_chart = $this->gereja->chart_select_absensi_ic($_SESSION['NO_IC']);
			//$data=$this->gereja->select_jemaat_untuk_peric(['NO_IC'=>$_SESSION['NO_IC']]);
			$this->load->view('mst_absensi_ic', ['data'=>$data,'data_chart'=>$data_chart]);
		}
	}
	
	public function insertAbsensi(){		
		$post = $this->input->post();
		$session = $this->session->userdata('ID_JEMAAT');
		$pesan = '';
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));
		//Untuk Admin Jemaat dan Super Admin sajaa -----------------------mungkin salah
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Ketua IC'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$data_jemaat=$this->gereja->select_jemaat_untuk_peric(['NO_IC'=>$_SESSION['NO_IC']]);
			$data_kegiatan=$this->gereja->select_kegiatan('%%',$_SESSION['NO_IC']);
			if (count($post)>0) {
				if (!isset($post['id_jemaat'])) $pesan = '<script> alert("Mohon pilih salah satu jemaat !") </script>';	
				else
				{
					for($i=0;$i<count($post['id_jemaat']);$i++)
					{
						$this->gereja->insert_absensi(['id_kegiatan'=>$post['id_kegiatan'],'id_jemaat'=>$post['id_jemaat'][$i]]);
					}
					$pesan = '<script> alert("Data telah tersimpan !") </script>';	
				}
			}
			
			$this->load->view('insert_absensi_baru',['data_jemaat'=>$data_jemaat,'data_kegiatan'=>$data_kegiatan,'pesan'=>$pesan]);
		}
	}
	
	public function insert()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));
		//Untuk Admin Jemaat dan Super Admin sajaa -----------------------mungkin salah
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$sukses = $this->gereja->insert_ic($post);
				if ($sukses == true) $pesan="<script> alert('Data telah tersimpan !!'); </script>"; 
			}
			
			$this->load->view('insert_ic',['pesan'=>$pesan]);
		}
	}
	
	public function delete($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Super Admin sajaa -----------------------mungkin salah
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$data = $this->gereja->select_ic($id);
			if (count($data) > 0) $this->gereja->delete_ic($id);
			redirect('beranda/pelayanan/beranda-admin/super-admin/mst-ic');
		}
	}
	
	public function update($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Super Admin sajaa -----------------------mungkin salah
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat' && $_SESSION['NAMA_PELAYANAN'] !== 'Ketua IC'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$sukses = $this->gereja->update_ic($post,$id);
				if ($sukses == true) 
					redirect('beranda/pelayanan/beranda-admin/super-admin/mst-ic');
				else $pesan="<script> alert('Data gagal tersimpan !!'); </script>"; 
			}
			
			$this->load->model('gereja');
			$data = $this->gereja->select_ic($id);
			if (count($data) > 0)
			{
				$this->load->view('update_ic',['pesan'=>$pesan,'data'=>$data]);
			}
			else
			{
				redirect('beranda/pelayanan/beranda-admin/super-admin/mst-ic');
			}
		}
	}
}