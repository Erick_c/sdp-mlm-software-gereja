<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventaris extends CI_Controller {
	public function list_pengembalian(){
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Inventaris dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Inventaris'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$get = $this->input->get();
			$post = $this->input->post();
			$pesan = '';

			$data = [];
			$data_jemaat = [];

			if (count($get)==0){
				redirect('/beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/list-peminjaman');
			}

			else if(count($get)>0) {
				if (count($post) > 0) {
					$post['kode_barang']=$get['kode_barang'];
					$post['id_kegiatan']=$get['id_kegiatan'];
					$this->gereja->update_pemakaian($post);
					redirect('/beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/list-peminjaman');
				}
			}
			
			$data = $this->gereja->select_pemakaian(['p.kode_barang'=>$get['kode_barang'],'p.id_kegiatan'=>$get['id_kegiatan'],'p.penanggung_jawab'=>$get['id_jemaat']]);
			$data_jemaat = $this->gereja->select_jemaat([]);

			if (count($data)<=0){
				redirect('/beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/list-peminjaman');
			}

			$this->load->view('inventarisPengembalian',["data"=>$data,'pesan'=>$pesan,'data_jemaat'=>$data_jemaat]);
		}
	}

	public function list_peminjaman()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Inventaris dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Inventaris'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			date_default_timezone_set("Asia/Bangkok");
			$data = $this->gereja->select_pemakaian(['TANGGAL_DAN_WAKTU_PENGEMBALIAN <'=>date('Y-m-d')]);
			$this->load->view('mst_peminjaman',["data"=>$data]);
		}
	}
	public function peminjaman(){
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Inventaris dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Inventaris'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$pesan='';
			$post = $this->input->post();
			$this->load->model('gereja');
			
			$data_jemaat = $this->gereja->select_jemaat([]);
			$data_inventaris = $this->gereja->select_inventaris('%%', 'KB');
			$data_kegiatan = $this->gereja->select_kegiatan();
			
			if(count($post)>0){
				$sukses = $this->gereja->insert_pemakaian($post);
				if ($sukses == true) $pesan="<script> alert('Data telah tersimpan !!'); </script>"; 
			}
			
			$this->load->view('inventarisPeminjaman',['data_kegiatan'=>$data_kegiatan,'data_inventaris'=>$data_inventaris, 'pesan'=>$pesan,'data_jemaat'=>$data_jemaat]);
		}
	}
	public function insert()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Inventaris dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Inventaris'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$sukses = $this->gereja->insert_inventaris($post);
				if ($sukses == true) $pesan="<script> alert('Data telah tersimpan !!'); </script>"; 
			}
			
			$data_cabang = $this->gereja->select_cabang();
			$this->load->view('insert_inventaris',['data_cabang'=>$data_cabang,'pesan'=>$pesan]);
		}
	}
	
	public function delete($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));		
		//Untuk Admin Inventaris dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Inventaris'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$data = $this->gereja->select_inventaris($id);
			if (count($data) > 0) $this->gereja->delete_inventaris($id);
			redirect('beranda/pelayanan/beranda-admin/super-admin/mst-inventaris');
		}
	}
	
	public function update($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Inventaris dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Inventaris'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$sukses = $this->gereja->update_inventaris($post,$id);
				if ($sukses == true) 
					redirect('beranda/pelayanan/beranda-admin/super-admin/mst-inventaris');
				else $pesan="<script> alert('Data gagal tersimpan !!'); </script>"; 
			}
			
			$this->load->model('gereja');
			$data = $this->gereja->select_inventaris($id);
			$data_cabang = $this->gereja->select_cabang();
			if (count($data) > 0)
			{
				$this->load->view('update_inventaris',['pesan'=>$pesan,'data_cabang'=>$data_cabang,'data'=>$data]);

			}
			else
			{
				redirect('beranda/pelayanan/beranda-admin/super-admin/mst-inventaris');
			}
		}
	}
}