<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('modelkeuangan','uang');
		$this->load->model('gereja');
	}
	//select
	public function index(){
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null){redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));}
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Keuangan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else{
			$data = $this->uang->select();
			$this->load->view("Keuangan/mst_Keuangan",['data'=>$data]);
		}
	}
	//insert
	public function insert(){
		$session = $this->session->userdata('ID_JEMAAT');
		if ($session === null){redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));}
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Keuangan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else{
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				//mungkin error karena masih belum diubah-ubah waktu di tes
				$sukses = $this->uang->insert($post);
				//semoga berhasil
				if ($sukses == true) 
					$pesan=
					"<script>
					$(function () {
					$(document).Toasts('create', {
						title: 'Berhasil!',
						autohide: true,
						delay: 1200,
						width: 400,
						height: 200,
						body: 'Data telah tersimpan di database.'
					});});
					</script>"; 
			}
			
			//data 
			$data_barang = $this->gereja->select_inventaris();//ada
			$data_cabang = $this->gereja->select_cabang();//ada
			$data_maintenance = $this->gereja->select_barang_punya_timo();
			$data_jemaat = $this->gereja->select_jemaat2();
			
			$this->load->view("Keuangan/insert_keuangan",['data_barang'=>$data_barang,'data_cabang'=>$data_cabang,'data_maintenance'=>$data_maintenance,'data_jemaat'=>$data_jemaat,'data_debit'=>ModelKeuangan::$data_debit,'data_kredit'=>ModelKeuangan::$data_kredit,"pesan"=>$pesan]);
		}
	}
	//update
	public function update($id){
		$session = $this->session->userdata('ID_JEMAAT');
		if ($session === null){redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));}
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Keuangan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else{
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$sukses = $this->uang->update($post,$id);
				if ($sukses == true) 
					redirect('beranda/pelayanan/beranda-admin/super-admin/mst-keuangan');
				else $pesan="<script> alert('Data gagal tersimpan !!'); </script>"; 
			}	
			
			$data = $this->uang->select($id);
			if (count($data) > 0)
			{	
				//biar user friendly
				$data_default = $data;
				$data_barang = $this->gereja->select_inventaris();//ada
				$data_cabang = $this->gereja->select_cabang();//ada
				$data_maintenance = $this->gereja->select_barang_punya_timo();
				$data_jemaat = $this->gereja->select_jemaat2();
				
				$this->load->view("Keuangan/update_keuangan",['data_default'=>$data_default,'data_barang'=>$data_barang,'data_cabang'=>$data_cabang,'data_maintenance'=>$data_maintenance,'data_jemaat'=>$data_jemaat,'data_debit'=>ModelKeuangan::$data_debit,'data_kredit'=>ModelKeuangan::$data_kredit,"pesan"=>$pesan]);
			}
			else
			{
				redirect('beranda/pelayanan/beranda-admin/super-admin/mst-keuangan');
			}
		}
	}
	
	//laporannya belum jadi
	public function laporan(){
		$session = $this->session->userdata('ID_JEMAAT');
		if ($session === null){redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));}
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Keuangan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else{
		$uang = $this->uang->select();
		$this->load->view('laporan_keuangan',['uang'=>$uang]);
		}
	}
}