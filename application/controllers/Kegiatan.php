<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan extends CI_Controller {
    public function index($namacabang)
    {
        $lnamacabang = '';
        for ($i=0; $i < strlen($namacabang); $i++) { 
            if ($namacabang[$i]=='-') $lnamacabang.=' ';
            else $lnamacabang.=$namacabang[$i];
        }
        
        $this->load->view('kegiatan',['nama'=>strtoupper($lnamacabang),'link'=>$namacabang]);
    }
	public function detail($id_kegiatan)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));		
		//Untuk Admin kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else{
			$this->load->model('gereja');
			$data = $this->gereja->select_kegiatan($id_kegiatan);
			if (count($data) > 0)
			{			
				$data_cabang = $this->gereja->select_cabang();
				$data_ic = $this->gereja->select_ic();
				$data_pembicara = $this->gereja->select_pembicara();
				$data_kategori = $this->gereja->select_kategori_kegiatan();
				$this->load->view('mst_detail_kegiatan',['pesan'=>'','data_cabang'=>$data_cabang,'data_ic'=>$data_ic,'data_pembicara'=>$data_pembicara,'data_kategori'=>$data_kategori,'data'=>$data]);
			}
			else
			{
				redirect('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan');
			}
		}
	}
	public function insert()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));		
		//Untuk Admin kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}	
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				//var_dump($post);
				if (isset($post['DataTables_Table_0_length'])) unset($post['DataTables_Table_0_length']);
				if (isset($post['DataTables_Table_1_length'])) unset($post['DataTables_Table_1_length']);
				$sukses = $this->gereja->insert_kegiatan($post);
				if ($sukses == true) $pesan="<script> alert('Data telah tersimpan !!'); </script>"; 
			}
			
			$data_cabang = $this->gereja->select_cabang();
			$data_ic = $this->gereja->select_ic();
			$data_pembicara = $this->gereja->select_pembicara();
			$data_kategori = $this->gereja->select_kategori_kegiatan();
			$this->load->view('insert_kegiatan',['pesan'=>$pesan,'data_cabang'=>$data_cabang,'data_ic'=>$data_ic,'data_pembicara'=>$data_pembicara,'data_kategori'=>$data_kategori]);
		}
	}
	public function update($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}	
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$sukses = $this->gereja->update_kegiatan($post,$id);
				if ($sukses == true) 
					redirect('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan');
				else $pesan="<script> alert('Data gagal tersimpan !!'); </script>"; 
			}
			
			$data = $this->gereja->select_kegiatan($id);
			if (count($data) > 0)
			{			
				$data_cabang = $this->gereja->select_cabang();
				$data_ic = $this->gereja->select_ic();
				$data_pembicara = $this->gereja->select_pembicara();
				$data_kategori = $this->gereja->select_kategori_kegiatan();
				$this->load->view('update_kegiatan',['pesan'=>$pesan,'data_cabang'=>$data_cabang,'data_ic'=>$data_ic,'data_pembicara'=>$data_pembicara,'data_kategori'=>$data_kategori,'data'=>$data]);
			}
			else
			{
				redirect('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan');
			}
		}
	}
	public function delete($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin kegiatan dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Kegiatan'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}	
		else
		{
			$this->load->model('gereja');
			$data = $this->gereja->select_kegiatan($id);
			if (count($data) > 0) $this->gereja->delete_kegiatan($id);
			redirect('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan');
		}
	}
}