<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanKeuangan extends CI_Controller {
	//untuk laporan keuangan
	static $data_kategori;
	public function __construct(){
		parent::__construct();
		$this->load->model('modellaporankeuangan','lapuang');
		$this->load->model('modelkeuangan','uang');//untuk list kategori transaksi
		
		self::$data_kategori = array_merge(ModelKeuangan::$data_debit,ModelKeuangan::$data_kredit);
	}
	//select
	public function index(){
		if (!$this->input->is_ajax_request()) {
			redirect(base_url('beranda'));
		}
		echo 'error';
	}
	public function perTanggal(){
		if (!$this->input->is_ajax_request()) {
			redirect(base_url('beranda'));
		}
		$post = $this->input->post();
		if($post['tahun2']!= 0){
			$date1 = new DateTime($post['hari1']."/".$post['bulan1']."/".$post['tahun1']);
			$date2 = new DateTime($post['hari2']."/".$post['bulan2']."/".$post['tahun2']);
			if($date2 < $date1){
				$temphari = $post['hari2'];
				$tempbulan = $post['bulan2'];
				$temptahun = $post['tahun2'];
				$post['hari2'] = $post['hari1'];
				$post['bulan2'] = $post['bulan1'];
				$post['tahun2'] = $post['tahun1'];
				$post['hari1'] = $temphari;
				$post['bulan1'] = $tempbulan;
				$post['tahun1'] = $temptahun;
			}
		}
		$data = [];
		$data['pengeluaran'] = $this->lapuang->jenisTransaksi($post,'K');
		$data['pemasukkan'] = $this->lapuang->jenisTransaksi($post,'D');
		$data['label_debit']= ModelKeuangan::$data_debit;
		$data['label_kredit']= ModelKeuangan::$data_kredit;
		$data['balance']= $this->lapuang->balance($post);
		for ($k = 0; $k < count(self::$data_kategori);$k++){
			$data[self::$data_kategori[$k]] = $this->lapuang->kategoriTransaksi($post,self::$data_kategori[$k]);
		}
		echo json_encode($data);
	}
	public function perBulan(){
		if (!$this->input->is_ajax_request()) {
			redirect(base_url('beranda'));
		}
		
	}
	public function perTahun(){
		if (!$this->input->is_ajax_request()) {
			redirect(base_url('beranda'));
		}
		
	}
}