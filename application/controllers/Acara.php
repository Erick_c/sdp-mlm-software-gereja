	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acara extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('modelacara','acara');
	}
	//kasih paging mungkin
    public function index($pageKe = "0")
    {
		$config['base_url'] = site_url("beranda");
		$get=$this->input->get();
		if(count($get)<=0){
			$get['tanggal_acara'] = '';
			$get['lokasi_acara'] = '';
			$get['kata_yang_terkait'] = '';
		}
		//echo $hasil->jumlah;
        $config['total_rows'] = $this->acara->countAcara(false,$get);
		$config['per_page'] = 10;
		
        $config['first_link'] = '<<';
        $config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';
        $config['num_links'] = 3; //kiri kanan mau ada berapa angka
		
		$config['full_tag_open'] = '<ul class="pagination justify-content-center">'; 
		
		$config['first_tag_open'] = '<li class="page-item">';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="'.base_url('beranda/'.$pageKe).'">';
		$config['num_tag_open'] = '<li class="page-item">';
		
		$config['first_tag_close'] = '</li>';
		$config['last_tag_close'] = '</li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_close'] = '</li>';
		
		$config['full_tag_close'] = '</ul>';
		
		$config['attributes'] = array('class' => 'page-link');
		$config['first_url'] = $config['base_url'].'/0?'.http_build_query($_GET);
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        
		
		
		$data = [];
			
			$data = $this->acara->listAcara(false,$config['per_page'],$pageKe,$get);
		$this->pagination->initialize($config);
		$this->load->view('acara',['data'=>$data,"get"=>$get]);
    }
    public function detailkegiatan($id)
    {
        $data = $this->acara->cariAcara($id);
		$id_kegiatan = $data[0]->ID_KEGIATAN;
		$pelayan = $this->acara->cariPelayan($id_kegiatan);
		$kategori = $this->acara->cariPelayanan($id_kegiatan);
        $this->load->view('detail_kegiatan',['data'=>$data,'pelayan'=>$pelayan,'kategori'=>$kategori]);
    }
}