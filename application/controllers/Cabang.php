<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cabang extends CI_Controller {
    public function index()
    {
        $this->load->view('cabang');
    }
	
	public function insert()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Super Admin saja -----------------------mungkin salah
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$sukses = $this->gereja->insert_cabang($post);
				if ($sukses == true) $pesan="<script> alert('Data telah tersimpan !!'); </script>"; 
			}
			
			$this->load->view('insert_cabang',['pesan'=>$pesan]);
		}
	}
	
	public function delete($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));		
		//Untuk Admin Jemaat dan Super Admin saja -----------------------mungkin salah
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$data = $this->gereja->select_cabang($id);
			if (count($data) > 0) $this->gereja->delete_cabang($id);
			redirect('beranda/pelayanan/beranda-admin/super-admin/mst-cabang');
		}
	}
	
	public function update($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));	
		//Untuk Admin Jemaat dan Super Admin saja -----------------------mungkin salah
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Jemaat'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$sukses = $this->gereja->update_cabang($post,$id);
				if ($sukses == true) 
					redirect('beranda/pelayanan/beranda-admin/super-admin/mst-cabang');
				else $pesan="<script> alert('Data gagal tersimpan !!'); </script>"; 
			}
			
			$this->load->model('gereja');
			$data = $this->gereja->select_cabang($id);
			if (count($data) > 0)
			{
				$this->load->view('update_cabang',['pesan'=>$pesan,'data'=>$data]);

			}
			else
			{
				redirect('beranda/pelayanan/beranda-admin/super-admin/mst-cabang');
			}
		}
	}
}