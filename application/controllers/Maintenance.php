<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenance extends CI_Controller {
	public function insert()
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));			
		//Untuk Admin Inventaris dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Inventaris'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$sukses = $this->gereja->insert_maintenance($post);
				if ($sukses == true) $pesan="<script> alert('Data telah tersimpan !!'); </script>"; 
			}
            $data_inventaris= $this->gereja->select_inventaris();
            $data_penanggung_jawab = $this->gereja->select_petugas_maintenance();
			$this->load->view('insert_maintenance',['data_penanggung_jawab'=>$data_penanggung_jawab,'data_inventaris'=>$data_inventaris,'pesan'=>$pesan]);
		}
	}
	
	public function update($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));		
		//Untuk Admin Inventaris dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Inventaris'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$post['terakhir_maintenance'] = date('Y-m-d');
				$sukses = $this->gereja->update_tanggal_maintenance($post,$id);
				if ($sukses == true) 
					redirect('beranda/pelayanan/beranda-admin/super-admin/');
				else $pesan="<script> alert('Data gagal tersimpan !!'); </script>"; 
			}
			$data = $this->gereja->select_maintenance($id);
			$this->load->model('gereja');
			if (count($data) > 0)
			{
				$this->load->view('update_tanggal_maintenance',['pesan'=>$pesan,'data'=>$data]);

			}
			else
			{
				redirect('beranda/pelayanan/beranda-admin/super-admin/');
			}
		}
	}

	public function update_lewat_master($id)
	{
		$session = $this->session->userdata('ID_JEMAAT');
		
		if ($session === null)
			redirect(base_url('/beranda/pelayanan/masuk-pelayanan'));		
		//Untuk Admin Inventaris dan Super Admin saja
		else if ($_SESSION['NAMA_PELAYANAN'] !== 'Super Admin' && $_SESSION['NAMA_PELAYANAN'] !== 'Admin Inventaris'){
			redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna'));
		}
		else
		{
			$this->load->model('gereja');
			$post=$this->input->post();
			
			$pesan = '';
			if (count($post)>0) 
			{
				$sukses = $this->gereja->update_maintenance($post,$id);
				if ($sukses == true) 
					redirect('beranda/pelayanan/beranda-admin/super-admin/mst-maintenance');
				else $pesan="<script> alert('Data gagal tersimpan !!'); </script>"; 
			}
			
			$this->load->model('gereja');
			$data = $this->gereja->select_maintenance($id);
			$data_inventaris= $this->gereja->select_inventaris();
            $data_penanggung_jawab = $this->gereja->select_petugas_maintenance();
			if (count($data) > 0)
			{
				$data_default = $data;
				$this->load->view('update_maintenance',['data_inventaris'=>$data_inventaris,'data_penanggung_jawab'=>$data_penanggung_jawab,'data_default'=>$data_default,'pesan'=>$pesan,'data'=>$data]);
			}
			else
			{
				redirect('beranda/pelayanan/beranda-admin/super-admin/mst-maintenance');
			}
		}
	}
	
}