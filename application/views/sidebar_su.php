<?php if (isset($menu_open) == false) $menu_open='';?>
<?php $FOTO=$this->session->userdata('FOTO');?>
<?php $NAMA_JEMAAT=$this->session->userdata('NAMA_JEMAAT');?>
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?=base_url('beranda')?>" class="brand-link">
      <img src="<?=base_url('img/core-img/logo2.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light" style="text-transform:capitalize"><?=$_SESSION['NAMA_PELAYANAN']?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <div style='background-image:url("<?php if($_SESSION['FOTO']!='') echo "data:image/jpeg;base64,".base64_encode($_SESSION['FOTO']); else echo base_url('img/icons/Profile.png')?>");height :33px;width:33px;background-size:auto 100%;background-position:center;'class="img-circle elevation-2" alt="User Image"></div>
        </div>
        <div class="info">
          <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/profilpengguna')?>" class="d-block"><?=$NAMA_JEMAAT?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
	  <?php if (
		strpos(strtolower($_SESSION['NAMA_PELAYANAN']),"admin")>-1
		||strpos(strtolower($_SESSION['NAMA_PELAYANAN']),"ketua")>-1
	  
	  ){?>
      <nav class="user-panel mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
		  <?php	if (
			strtolower($_SESSION['NAMA_PELAYANAN'])=="super admin"
		  ){?>
          <li class="nav-item has-treeview <?php if (strpos($menu_open,"cabang")>-1) echo "menu-open";?> ">
            <a href="" class="nav-link <?php if (strpos($menu_open,"cabang")>-1) echo "active";?>">
              <i class="nav-icon fa fa-code-branch"></i>
              <p>
                Cabang
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-cabang/data-baru')?>" class="nav-link <?php if (strpos($menu_open,"cabang baru")>-1) echo "active";?>">
                  <i class="fas fa-file nav-icon"></i>
                  <p>Cabang Baru</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-cabang')?>" class="nav-link <?php if (strpos($menu_open,"list cabang")>-1) echo "active";?>">
                  <i class="fas fa-table nav-icon"></i>
                  <p>Lihat Semua Data</p>
                </a>
              </li>
            </ul>
          </li>
		  <?php } if (
			strtolower($_SESSION['NAMA_PELAYANAN'])=="super admin"
			|| strtolower($_SESSION['NAMA_PELAYANAN'])=="admin jemaat"
		  ){?>
          <li class="nav-item has-treeview <?php if (strpos($menu_open,"pembicara")<=-1 && strpos($menu_open,"ic")>-1 && strpos($menu_open,"absensi")<=-1) echo "menu-open";?>">
            <a href="#" class="nav-link <?php if (strpos($menu_open,"pembicara")<=-1&&strpos($menu_open,"ic")>-1 && strpos($menu_open,"absensi")<=-1) echo "active";?>">
              <i class="nav-icon fa fa-group"></i>
              <p>
                IC
				<i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-ic/data-baru')?>" class="nav-link <?php if (strpos($menu_open,"pembicara baru")<=-1 && strpos($menu_open,"ic baru")>-1 && strpos($menu_open,"absensi")<=-1) echo "active";?>">
                  <i class="fas fa-file nav-icon"></i>
                  <p>IC Baru</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-ic')?>" class="nav-link <?php if (strpos($menu_open,"list pembicara")<=-1 && strpos($menu_open,"list ic")>-1 && strpos($menu_open,"absensi")<=-1) echo "active";?>">
                  <i class="fas fa-table nav-icon"></i>
                  <p>Lihat Semua Data</p>
                </a>
              </li>
            </ul>
          </li>
		  <?php } if (
			strtolower($_SESSION['NAMA_PELAYANAN'])=="super admin"
			|| strtolower($_SESSION['NAMA_PELAYANAN'])=="admin jemaat"
			|| strtolower($_SESSION['NAMA_PELAYANAN'])=="admin absensi"
		  ){?>
		  
          <li class="nav-item has-treeview <?php if (strpos($menu_open,"list ibadah umum")>-1) echo "menu-open";?>">
            <a href="<?=base_url('/beranda/pelayanan/beranda-admin/super-admin/mst-ibadah-umum')?>" class="nav-link <?php if (strpos($menu_open,"list ibadah umum")>-1) echo "active";?>">
              <i class="nav-icon fas fa-calculator"></i>
              <p>
                Absensi Umum
				<!--<i class="fas fa-angle-left right"></i>-->
              </p>
            </a>
          </li>
		  <?php } if (
			strtolower($_SESSION['NAMA_PELAYANAN'])=="super admin"
			|| strtolower($_SESSION['NAMA_PELAYANAN'])=="ketua ic"
		  ){?>
          <li class="nav-item has-treeview <?php if (strpos($menu_open,"absensi ic")>-1) echo "menu-open";?>">
            <a href="#" class="nav-link <?php if (strpos($menu_open,"absensi ic")>-1) echo "active";?>">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Absensi IC
				<i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-absensi-ic/data-baru')?>" class="nav-link <?php if (strpos($menu_open,"absensi ic baru")>-1) echo "active";?>">
                  <i class="fas fa-file nav-icon"></i>
                  <p>Masukkan Data</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-absensi-ic')?>" class="nav-link <?php if (strpos($menu_open,"list absensi ic")>-1) echo "active";?>">
                  <i class="fas fa-chart-line nav-icon"></i>
                  <p>Laporan Absensi</p>
                </a>
              </li>
            </ul>
          </li>
		  <?php } if (
			strtolower($_SESSION['NAMA_PELAYANAN'])=="super admin"
			|| strtolower($_SESSION['NAMA_PELAYANAN'])=="admin inventaris"
		  ){?>
          <li class="nav-item has-treeview <?php if (strpos($menu_open,"inventaris")>-1) echo "menu-open";?>">
            <a href="#" class="nav-link <?php if (strpos($menu_open,"inventaris")>-1) echo "active";?>">
              <i class="nav-icon fas fa-paste"></i>
              <p>
                Inventaris
				<i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/data-baru')?>" class="nav-link <?php if (strpos($menu_open,"inventaris baru")>-1) echo "active";?>">
                  <i class="fas fa-file nav-icon"></i>
                  <p>Inventaris Baru</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/peminjaman')?>" class="nav-link <?php if (strpos($menu_open,"list")<=-1 && strpos($menu_open,"peminjaman inventaris")>-1) echo "active";?>">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>Peminjaman</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/list-peminjaman')?>" class="nav-link <?php if (strpos($menu_open,"list peminjaman inventaris")>-1) echo "active";?>">
                  <i class="fas fa-book nav-icon"></i>
                  <p>List Peminjaman</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-inventaris')?>" class="nav-link <?php if (strpos($menu_open,"list inventaris")>-1) echo "active";?>">
                  <i class="fas fa-table nav-icon"></i>
                  <p>Lihat Semua Data</p>
                </a>
              </li>
            </ul>
          </li>
		  <?php } if (
			strtolower($_SESSION['NAMA_PELAYANAN'])=="super admin"
			|| strtolower($_SESSION['NAMA_PELAYANAN'])=="admin jemaat"
		  ){?>
          <li class="nav-item has-treeview <?php if (strpos($menu_open,"jemaat")>-1) echo "menu-open";?>">
            <a href="#" class="nav-link <?php if (strpos($menu_open,"jemaat")>-1) echo "active";?>">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Jemaat
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-jemaat/data-baru')?>" class="nav-link <?php if (strpos($menu_open,"jemaat baru")>-1) echo "active";?>">
                  <i class="fas fa-file nav-icon"></i>
                  <p>Jemaat Baru</p>
                </a>
              </li>
			  <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-jemaat')?>" class="nav-link <?php if (strpos($menu_open,"list jemaat")>-1) echo "active";?>">
                  <i class="fas fa-table nav-icon"></i>
                  <p>Lihat Semua Jemaat</p>
                </a>
              </li>

            </ul>
          </li>
		  <?php } if (
			strtolower($_SESSION['NAMA_PELAYANAN'])=="super admin"
			|| strtolower($_SESSION['NAMA_PELAYANAN'])=="admin kegiatan"
		  ){?>
		  <li class="nav-item has-treeview <?php if (strpos($menu_open,"kegiatan")>-1) echo "menu-open";?>">
            <a href="#" class="nav-link <?php if (strpos($menu_open,"kegiatan")>-1) echo "active";?>">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
                Kegiatan
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan/data-baru')?>" class="nav-link <?php if (strpos($menu_open,"kegiatan baru")>-1) echo "active";?>">
                  <i class="fas fa-file nav-icon"></i>
                  <p>Kegiatan Baru</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan')?>" class="nav-link <?php if (strpos($menu_open,"list kegiatan")>-1) echo "active";?>">
                  <i class="fas fa-table nav-icon"></i>
                  <p>Lihat Semua Data</p>
                </a>
              </li>
			  <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-absensi/laporan')?>" class="nav-link <?php if (strpos($menu_open,"list absensi kegiatan")>-1) echo "active";?>">
                  <i class="fas fa-chart-line nav-icon"></i>
                  <p>Laporan Absensi</p>
                </a>
              </li>
            </ul>
          </li>
		  <?php } if (
			strtolower($_SESSION['NAMA_PELAYANAN'])=="super admin"
			|| strtolower($_SESSION['NAMA_PELAYANAN'])=="admin kegiatan"
		  ){?>
		  <li class="nav-item has-treeview <?php if (strpos($menu_open,"kategori")>-1) echo "menu-open";?>">
			<a href="#" class="nav-link <?php if (strpos($menu_open,"kategori")>-1) echo "active";?>">
			  <i class="fas fa-list-alt nav-icon"></i>
			  <p>
				Kategori Kegiatan
				<i class="fas fa-angle-left right"></i>
			  </p>
			</a>
			<ul class="nav nav-treeview">
			  <li class="nav-item">
				<a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-kategori/data-baru')?>" class="nav-link <?php if (strpos($menu_open,"kategori baru")>-1) echo "active";?>">
				  <i class="fas fa-file nav-icon"></i>
				  <p>Kategori Baru</p>
				</a>
			  </li>
			  <li class="nav-item">
				<a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-kategori')?>" class="nav-link <?php if (strpos($menu_open,"list kategori")>-1) echo "active";?>">
				  <i class="fas fa-table nav-icon"></i>
				  <p>Lihat Semua Kategori</p>
				</a>
			  </li>
			</ul>
		  </li>
		  <?php } if (
			strtolower($_SESSION['NAMA_PELAYANAN'])=="super admin"
			||strtolower($_SESSION['NAMA_PELAYANAN'])=="admin keuangan"
		  ){?>
          <li class="nav-item has-treeview <?php if (strpos($menu_open,"Transaksi")>-1) echo "menu-open";?>" >
            <a href="#" class="nav-link <?php if (strpos($menu_open,"Transaksi")>-1) echo "active";?>" >
              <i class="nav-icon fas fa-coins"></i>
              <p>
                Keuangan
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
				<li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-keuangan/data-baru')?>" class="nav-link <?php if (strpos($menu_open,"Transaksi Baru")>-1) echo "active";?>">
                  <i class="fas fa-file nav-icon"></i>
                  <p>Transaksi Baru</p>
                </a>
              </li>
			  <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-keuangan/')?>" class="nav-link <?php if (strpos($menu_open,"Tabel Transaksi")>-1) echo "active";?>">
                  <i class="fas fa-table nav-icon"></i>
                  <p>Lihat Semua Transaksi</p>
                </a>
              </li>
			  <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-keuangan/laporan')?>" class="nav-link <?php if (strpos($menu_open,"Laporan Transaksi")>-1) echo "active";?>">
                  <i class="fas fa-chart-line nav-icon"></i>
                  <p>Laporan</p>
                </a>
              </li>
            </ul>
          </li>      
		  <?php } if (
			strtolower($_SESSION['NAMA_PELAYANAN'])=="super admin"
			|| strtolower($_SESSION['NAMA_PELAYANAN'])=="admin inventaris"
		  ){?>
          <li class="nav-item <?php if (strpos($menu_open,"maintenance")>-1) echo "menu-open";?>">
            <a href="#" class="nav-link <?php if (strpos($menu_open,"maintenance")>-1) echo "active";?>">
              <i class="nav-icon fa fa-wrench"></i>
              <p>
                Maintenance
                <i class="fas fa-angle-left right"></i>
			  </p>
            </a>
            <ul class="nav nav-treeview">
			  <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-maintenance/data-baru')?>" class="nav-link <?php if (strpos($menu_open,"maintenance baru")>-1) echo "active";?>">
                  <i class="fas fa-file nav-icon"></i>
                  <p>Maintenance Baru</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-maintenance/')?>" class="nav-link <?php if (strpos($menu_open,"list maintenance")>-1) echo "active";?>">
                  <i class="fas fa-table nav-icon"></i>
                  <p>Daftar Maintenance</p>
                </a>
              </li>
			 </ul>
          </li>
		  <?php } if (
			strtolower($_SESSION['NAMA_PELAYANAN'])=="super admin"
			|| strtolower($_SESSION['NAMA_PELAYANAN'])=="admin kegiatan"
		  ){?>
          <li class="nav-item has-treeview  <?php if (strpos($menu_open,"pembicara")>-1) echo 'menu-open';?>">
            <a href="#" class="nav-link <?php if (strpos($menu_open,"pembicara")>-1) echo 'active';?>">
              <i class="nav-icon fa fa-user"></i>
              <p>
				Pembicara
				<i class="fas fa-angle-left right"></i>
			  </p>
            </a>
            <ul class="nav nav-treeview">
			  <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pembicara/data-baru')?>" class="nav-link <?php if (strpos($menu_open,"pembicara baru")>-1) echo "active";?>">
                  <i class="fas fa-file nav-icon"></i>
                  <p>Pembicara Baru</p>
                </a>
              </li>
			  <li class="nav-item">
                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pembicara')?>" class="nav-link <?php if (strpos($menu_open,"list pembicara")>-1) echo "active";?>">
                  <i class="fas fa-table nav-icon"></i>
                  <p>Lihat Semua Data</p>
                </a>
              </li>
            </ul>
          </li>
		  <?php } if (true){?>
          <li class="nav-item has-treeview">
			<a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan')?>" class="nav-link <?php if (strpos($menu_open,"pelayanan")>-1) echo "active";?>">
              <i class="nav-icon fa fa-cloud"></i>
              <p>
                Pelayanan
              </p>
            </a>
          </li>
		  <?php }?>
		</ul>
      </nav>
      <!-- /.sidebar-menu -->
	  <?php }?>
      <div class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="<?=base_url('beranda/pelayanan/beranda-admin/logOut')?>" class="nav-link">
			  <link rel="stylesheet" href="<?=base_url('fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">
              <span class="fa fa-sign-out nav-icon"></span>&nbsp;
              <p>
                Keluar
              </p>
            </a>
		  </li>
		</ul>
      </div>

    </div>
    <!-- /.sidebar -->
  </aside>