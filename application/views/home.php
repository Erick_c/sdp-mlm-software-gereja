<?php $this->load->view('header',['title'=>'Beranda']);?>
    
    <!-- ##### Hero Area Start ##### -->
    <section class="hero-area hero-post-slides owl-carousel">
        <!-- Single Hero Slide -->
        <div class="single-hero-slide bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(img/bg-img/1.jpg);">
            <!-- Post Content -->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="hero-slides-content">
                            <h2 data-animation="fadeInUp" data-delay="100ms">Bangun Harapan</h2>
                            <p data-animation="fadeInUp" data-delay="300ms">Baca mengenai misi kami, kepercayaan kami, dan harapan kami pada Yesus.</p>
                            <a href="#welcome" class="btn crose-btn" data-animation="fadeInUp" data-delay="500ms">Mengenai Kami</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Hero Slide -->
        <div class="single-hero-slide bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(img/bg-img/2.jpg);">
            <!-- Post Content -->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="hero-slides-content">
                            <h2 data-animation="fadeInUp" data-delay="100ms">Buat Yesus Tahu</h2>
                            <p data-animation="fadeInUp" data-delay="300ms">Baca mengenai misi kami, kepercayaan kami, dan harapan kami pada Yesus.</p>
                            <a href="#welcome" class="btn crose-btn" data-animation="fadeInUp" data-delay="500ms">Mengenai Kami</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### About Area Start ##### -->
    <a id="welcome">
        <section class="about-area section-padding-100-0">
            <div class="container">
                <div class="row">
                    <!-- Section Heading -->
                    <div class="col-12">
                        <div class="section-heading mb-100">
                            <img src="img/core-img/logo.png" width="200px" alt=""> <br><br>
                            <h2>Bethel Tabernakel</h2>
                            <p>Mari bergabung dan rasakan hadirat Tuhan serta indahnya persekutuan dalam ibadah dan kegiatan jemaat</p> <br>
                            <a href="<?=base_url('beranda/kegiatan-rutin')?>" class="btn crose-btn"> Lihat Jadwal Kegiatan </a>
                            <a href="<?=base_url('beranda/pelayanan')?>" class="btn crose-btn"> Masuk Pelayanan </a>
                        </div>
                    </div>
                </div>
                <div class="row about-content justify-content-center">
                </div>
            </div>
        </section>
    </a>
    <!-- ##### About Area End ##### -->

    <!-- ##### Upcoming Events Area Start ##### -->
    <section class="upcoming-events-area section-padding-0-100">
        <!-- Upcoming Events Heading Area -->
        <div class="upcoming-events-heading bg-img bg-overlay bg-fixed" style="background-image: url(img/bg-img/1.jpg);">
            <div class="container">
                <div class="row">
                    <!-- Section Heading -->
                    <div class="col-12">
                        <div class="section-heading text-left white">
                            <h2>Acara Mendatang</h2>
                            <p>
                                Pastikan untuk mengunjungi halaman Acara Mendatang kami secara teratur untuk mendapatkan informasi
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Upcoming Events Slide -->
        <div class="upcoming-events-slides-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="upcoming-slides owl-carousel">
							<?php 
							if(count($data) > 0){
								for($k = 0; $k < count($data);$k++){?>
								<?php if($k % 3 === 0){//apakah berhasil?>
								<div class="single-slide">
								<?php }?>
									<!-- Single Upcoming Events Area -->
									<div class="single-upcoming-events-area d-flex flex-wrap align-items-center">
										<!-- Thumbnail -->
										<div class="upcoming-events-thumbnail">
											<img src="img/acara-img/<?= $data[$k]->ID_KEGIATAN;?>.jpg" alt="">
										</div>
										<!-- Content -->
										<div class="upcoming-events-content d-flex flex-wrap align-items-center">
											<div class="events-text">
												<h4><?= $data[$k]->NAMA_KEGIATAN?></h4>
												<div class="events-meta">
													<a href="#"><i class="fa fa-calendar" aria-hidden="true"></i><?= $data[$k]->TANGGAL_KEGIATAN;?></a> <br>
													<a href="#"><i class="fa fa-clock-o" aria-hidden="true"></i><?= $data[$k]->WAKTU_KEGIATAN;?></a><br>
													<a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i><?= $data[$k]->LOKASI_KEGIATAN;?></a>
												</div>
											</div>
											<div class="find-out-more-btn">
												<a href="<?='beranda/acara-khusus/detail/'. $data[$k]->ID_KEGIATAN;?>" class="btn crose-btn btn-2">Detail Kegiatan</a>
											</div>
										</div>
									</div>
								<?php if(($k+1) % 3 === 0){//apakah berhasil?>
								</div>    
								<?php }?>
                            <?php 
								}
							}
							else{
							?>
								<div class="single-slide">
									<!-- Single Upcoming Events Area -->
									<div class="single-upcoming-events-area d-flex flex-wrap align-items-center">
										<!-- Thumbnail -->
										<div class="upcoming-events-thumbnail">
											<img src="<?=base_url('img/acara-img/0.jpg')?>" alt="">
										</div>
										<!-- Content -->
										<div class="upcoming-events-content d-flex flex-wrap align-items-center">
											<div class="events-text">
												<h4>Belum ada acara</h4>
												<div class="events-meta">
													<a href="#"><i class="fa fa-calendar" aria-hidden="true"></i>Acara</a><br>
													<a href="#"><i class="fa fa-clock-o" aria-hidden="true"></i>Tidak</a><br>
													<a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>Tersedia</a><br>
												</div>
											</div>
											<div class="find-out-more-btn">
												<a href="<?=base_url('beranda')?>" class="btn crose-btn btn-2">Kembali</a>
											</div>
										</div>
									</div>
								</div>
							<?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Upcoming Events Area End ##### -->
<?php $this->load->view('footer');?>