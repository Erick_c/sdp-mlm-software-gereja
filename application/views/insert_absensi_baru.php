<?php $this->load->view('header_admin',['title'=>'Absensi IC Baru']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'absensi ic baru']);?>
<link rel="stylesheet" href="<?=base_url('plugins/select2/css/select2.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/icheck-bootstrap/icheck-bootstrap.min.css')?>">
<style>
	span.select2-selection.select2-selection--single
	{
		height:initial;
	}
</style>
<div class="content-wrapper" style="min-height: 1200.88px;">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Absensi IC</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Beranda</a></li>
              <li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
              <li class="breadcrumb-item active">Absensi IC</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <form action="" method="post" role="form">
                            <div class="card-body">
                                <div class="form-group">
									<label for="">
										Kegiatan
                                    </label>									
									<?php
										$tampilan[''] = '-- Pilih Kegiatan --';
										for ($i=0;$i<count($data_kegiatan);$i++)
										{
											$nama = $data_kegiatan[$i]->NAMA_KEGIATAN;
											if ($nama == null) $nama = $data_kegiatan[$i]->NAMA_KATEGORI;
											$tampilan[$data_kegiatan[$i]->ID_KEGIATAN] = $nama;
										}
										echo form_dropdown('id_kegiatan',$tampilan,'',["class"=>"form-control select2","required"=>"required","style"=>"width:100%"]);
									?>
								</div>
                                <div class="form-group">
									<label for="">
										Jemaat<br>
										<sup>Klik jemaat yang hadir dalam kegiatan tersebut</sup>
                                    </label>
								</div>
									<?php
										$tampilan = [];
										echo '<div style="width:50%;float:left">';
										for ($i=0;$i<count($data_jemaat)/2;$i++)
										{
											echo 
											'<div class="form-group clearfix">'.
												'<div class="icheck-primary d-inline">'.
													'<input type="checkbox" id="checkboxPrimary'.$i.'" name="id_jemaat[]" value="'.$data_jemaat[$i]->ID_JEMAAT.'">'.
													'<label for="checkboxPrimary'.$i.'" style="font-weight:inherit;width:200px;">'.
														'<div class="profile-user-img img-fluid img-circle"'.
														"style='".'background-image:url("data:image/jpeg;base64,'.base64_encode($data_jemaat[$i]->FOTO).'"'.");height :100px;width:100px;background-size:auto 100%;background-position:center;'></div>".
														'<center>'.
															$data_jemaat[$i]->NAMA_JEMAAT.
														'</center>'.
													'</label>'.
												'</div>'.
											'</div>';
										}
										echo '</div>';
										echo '<div style="width:50%;float:left">';
										$j=count($data_jemaat)/2;
										if (count($data_jemaat)%2==1) $j+=1;
										for ($i=$j;$i<count($data_jemaat);$i++)
										{
											echo 
											'<div class="form-group clearfix">'.
												'<div class="icheck-primary d-inline">'.
													'<input type="checkbox" id="checkboxPrimary'.$i.'" name="id_jemaat[]" value="'.$data_jemaat[$i]->ID_JEMAAT.'">'.
													'<label for="checkboxPrimary'.$i.'" style="font-weight:inherit;width:200px;">'.
														'<div class="profile-user-img img-fluid img-circle"'.
														"style='".'background-image:url("data:image/jpeg;base64,'.base64_encode($data_jemaat[$i]->FOTO).'"'.");height :100px;width:100px;background-size:auto 100%;background-position:center;'></div>".
														'<center>'.
															$data_jemaat[$i]->NAMA_JEMAAT.
														'</center>'.
													'</label>'.
												'</div>'.
											'</div>';
										}
										echo '</div>';
										
									?><br>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-absensi-ic')?>" class="btn btn-secondary"> Batal </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
</div>

<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<script src="<?=base_url('plugins/select2/js/select2.full.min.js')?>"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Initialize Select2 Elements
    $('.select2').select2()
  })
  
</script>
<?php echo $pesan?>