
    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <!-- Main Footer Area -->
        <div class="main-footer-area">
            <div class="container">
                <div class="row">

                    <!-- Single Footer Widget -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="single-footer-widget mb-70">
                            <br>
                            <a href="#" class="footer-logo"><img src="<?=base_url('img/core-img/logo2.png')?>" alt=""></a>
                        </div>
                    </div>

                    <!-- Single Footer Widget -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="single-footer-widget mb-70">
                            <h5 class="widget-title">Link Cepat</h5>
                            <nav class="footer-menu">
                                <ul>
                                    <li><a href="<?=base_url('beranda/pelayanan')?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Pelayanan </a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>

                    <!-- Single Footer Widget -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="single-footer-widget mb-70">
                            <h5 class="widget-title">Lokasi Kami</h5>
                            <div class="contact-information">
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> Jln. Jendral Sudirman, Surabaya, Indonesia</p>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> Jln. Ir. Soekarno, Surabaya, Indonesia</p>
                            </div>
                        </div>
                    </div>

                    <!-- Single Footer Widget -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="single-footer-widget mb-70">
                            <h5 class="widget-title">Kontak Kami</h5>

                            <div class="contact-information">
                                <a href="callto:081300161852"><i class="fa fa-phone" aria-hidden="true"></i> 081300161852 </a>
                                <a href="mailto:info.deercreative@gmail.com"><i class="fa fa-envelope" aria-hidden="true"></i> info.bethel_tabernakel_sby@gmail.com</a>
                                <p><i class="fa fa-clock-o" aria-hidden="true"></i> Senin - Jumat: 08.00 - 18.00</p>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php $this->load->view('loading.php');?>
</body>

</html>