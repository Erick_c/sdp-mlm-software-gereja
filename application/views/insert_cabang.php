<?php $this->load->view('header_admin',['title'=>'Cabang Baru']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'cabang baru']);?>
<div class="content-wrapper" style="min-height: 1200.88px;">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Cabang Baru</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Beranda</a></li>
              <li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
              <li class="breadcrumb-item active">Cabang Baru</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <form action="" method="post" role="form">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">
                                        Nama Cabang
                                    </label>
                                    <input type="text" name="nama_cabang" class="form-control" name="nama_cabang" required=required>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        Alamat
                                    </label>
                                    <input type="text" name="alamat_cabang" class="form-control" required=required>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-cabang')?>" class="btn btn-secondary"> Batal </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
</div>
<?php echo $pesan?>
<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>