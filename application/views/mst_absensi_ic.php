<?php $this->load->view('header_admin',['title'=>'Master Super Admin']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'list absensi ic']);?>
<style type="text/css">/* Chart.js */
@keyframes chartjs-render-animation{from{opacity:.99}to{opacity:1}}.chartjs-render-monitor{animation:chartjs-render-animation 1ms}.chartjs-size-monitor,.chartjs-size-monitor-expand,.chartjs-size-monitor-shrink{position:absolute;direction:ltr;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1}.chartjs-size-monitor-expand>div{position:absolute;width:1000000px;height:1000000px;left:0;top:0}.chartjs-size-monitor-shrink>div{position:absolute;width:200%;height:200%;left:0;top:0}</style>
<link rel="stylesheet" href="<?=base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <div class="container-fluid">
		<div class="row mb-2">
		  <div class="col-sm-6">
			<h1>Absensi IC</h1>
			<sup> Silahkan pilih jemaat untuk melihat grafik kedatangan </sup>
		  </div>
		  <div class="col-sm-6">
			<ol class="breadcrumb float-sm-right">
			  <li class="breadcrumb-item"><a href="#">Beranda</a></li>
			  <li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
			  <li class="breadcrumb-item active">Absensi IC</li>
			</ol>
		  </div>
		</div>
	  </div><!-- /.container-fluid -->
	</section>
	<!-- /.content-header -->

    <section class="content">
		<div class="col-md-12">
			<!-- BAR CHART -->
			<div class="card card-success">
				<div class="card-header">
					<h3 class="card-title">10 Kegiatan Terakhir</h3>

					<div class="card-tools">
						<!-- <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
						<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button> -->
					</div>
				</div>
				<div class="card-body">
					<div class="chart">
						<canvas id="barChart" style="height:230px; min-height:230px"></canvas>
					</div>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div>
      <!-- Table content -->    
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Semua Data</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
				  <?php 
					$this->table->set_template([
						'table_open'=>'<table id="" class="with_search table table-bordered table-strip">',
					]);
					
					//$tampilkan = [[['style'=>'width:100px;min-width:100px','data'=>'Foto'],'Nama','Jenis Kelamin','Alamat Jemaat',['style'=>'min-width:120px;']]];
					$tampilkan = [['NAMA KEGIATAN', 'TANGGAL KEGIATAN', 'LOKASI KEGIATAN','']];
					for($i=0;$i<count($data);$i++){
						$isi = [];
						$isi[] = $data[$i]->NAMA_KEGIATAN;
						$isi[] = $data[$i]->TANGGAL_KEGIATAN;
						$isi[] = $data[$i]->LOKASI_KEGIATAN;
						$isi[] = '<center><a href="./mst-absensi-ic/laporan-absensi-ic/'.$data[$i]->ID_KEGIATAN.'" class="btn btn-primary">LIHAT ABSENSI </a></center>';
						$tampilkan[]=$isi;
					}
					echo $this->table->generate($tampilkan);
				  ?>
			  </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div><!-- /.container-fluid -->
      <!-- /.content -->
    </section>
  </div>
  <!-- /.content-wrapper -->
<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<!-- Chart -->
<script src="<?=base_url('plugins/chart.js/Chart.min.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
<script>
	$(function () {
		var areaChartData = {
		  labels  : [
				<?php 
					for($i=count($data)-1;$i>=0;$i--){
						echo '"'.$data_chart[$i]->NAMA_KEGIATAN.'",';
					}
				?>
		  ],
		  datasets: [
			{
			  label               : 'Jemaat Yang Hadir',
			  backgroundColor     : 'rgba(60,141,188,0.9)',
			  borderColor         : 'rgba(60,141,188,0.8)',
			  pointRadius          : false,
			  pointColor          : '#3b8bba',
			  pointStrokeColor    : 'rgba(60,141,188,1)',
			  pointHighlightFill  : '#fff',
			  pointHighlightStroke: 'rgba(60,141,188,1)',
			  data                : [
				<?php 
					for($i=count($data)-1;$i>=0;$i--){
						echo $data_chart[$i]->JUMLAH_JEMAAT_HADIR.',';
					}
				?>
			  ]
			},
			{
			  label               : 'Jemaat Yang Tidak Hadir',
			  backgroundColor     : 'rgba(210, 214, 222, 1)',
			  borderColor         : 'rgba(210, 214, 222, 1)',
			  pointRadius         : false,
			  pointColor          : 'rgba(210, 214, 222, 1)',
			  pointStrokeColor    : '#c1c7d1',
			  pointHighlightFill  : '#fff',
			  pointHighlightStroke: 'rgba(220,220,220,1)',
			  data                : [
				<?php 
					for($i=count($data)-1;$i>=0;$i--){
						echo $data_chart[$i]->JUMLAH_JEMAAT_TIDAK_HADIR.',';
					}
				?>]
			},
		  ]
		}

		var areaChartOptions = {
		  maintainAspectRatio : false,
		  responsive : true,
		  legend: {
			display: false
		  },
		  scales: {
			xAxes: [{
			  gridLines : {
				display : false,
			  }
			}],
			yAxes: [{
			  gridLines : {
				display : false,
			  }
			}]
		  }
		}
			
		//-------------
		//- BAR CHART -
		//-------------
		var barChartCanvas = $('#barChart').get(0).getContext('2d')
		var barChartData = jQuery.extend(true, {}, areaChartData)
		var temp0 = areaChartData.datasets[0]
		var temp1 = areaChartData.datasets[1]
		barChartData.datasets[0] = temp1
		barChartData.datasets[1] = temp0

		var barChartOptions = {
		responsive              : true,
		maintainAspectRatio     : false,
		datasetFill             : false
		}

		var barChart = new Chart(barChartCanvas, {
		type: 'bar', 
		data: barChartData,
		options: barChartOptions
		})
	})
</script>