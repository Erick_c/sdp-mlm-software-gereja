<?php $this->load->view('header_admin',['title'=>'Cabang Baru']);?>
<?php $this->load->view('sidebar_su');?>
<link rel="stylesheet" href="<?=base_url('plugins/select2/css/select2.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')?>">
<style>
	span.select2-selection.select2-selection--single
	{
		height:initial;
	}
</style>
<div class="content-wrapper" style="min-height: 1200.88px;">
    <section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Ubah Kegiatan</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?=base_url('/')?>">Beranda</a></li>
						<li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
						<li class="breadcrumb-item active">Ubah Kegiatan</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<form action="" method="post" role="form">
							<div class="card-body">
								<div class="form-group">
									<label for="">
                                        Nama Kegiatan
                                    </label>
									<input type="text" name="nama_kegiatan" value="<?=$data[0]->NAMA_KEGIATAN?>" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="">
                                        Nama Cabang
                                    </label>
									<?php
										$tampilan = [''=>'-- Pilih Cabang --'];
										for($i=0; $i<count($data_cabang); $i++){
											$tampilan[$data_cabang[$i]->ID_CABANG]=$data_cabang[$i]->NAMA_CABANG;
										}
										echo form_dropdown('id_cabang',$tampilan,$data[0]->ID_CABANG,["class"=>"form-control select2","required"=>'required'])
									?>
								</div>
								<?php if(strtolower($data[0]->NAMA_KATEGORI)=='ibadah ic') { ?>
								<div class="form-group">
									<label for="">
										Nama IC
                                    </label>									
									<?php
										$tampilan = [''=>'-- Pilih IC --'];
										for($i=0; $i<count($data_ic); $i++){
											$tampilan[$data_ic[$i]->NO_IC]=$data_ic[$i]->NAMA_IC;
										}
										echo form_dropdown('no_ic',$tampilan,$data[0]->NO_IC,["class"=>"form-control select2",'required'=>'required'])
									?>
								</div>
								<?php } ?>
								<div class="form-group">
									<label for="">
										Pembicara
                                    </label>									
									<?php
										$tampilan = [''=>'-- Pilih Pembicara --'];
										for($i=0; $i<count($data_pembicara); $i++){
											$tampilan[$data_pembicara[$i]->ID_PEMBICARA]=$data_pembicara[$i]->NAMA_PEMBICARA;
										}
										echo form_dropdown('id_pembicara',$tampilan,$data[0]->ID_PEMBICARA,["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<?php if($data[0]->LOKASI_KEGIATAN!='') { ?>
								<div class="form-group">
									<label for="">
                                        Lokasi Kegiatan
                                    </label>
									<input type="text" name="lokasi_kegiatan" value="<?=$data[0]->LOKASI_KEGIATAN?>" class="form-control"required>
								</div>
								<?php } ?>
								<div class="form-group">
									<label for="">
                                        Waktu Kegiatan
                                    </label>
									<input type="time" name="waktu_kegiatan" value="<?=$data[0]->WAKTU_KEGIATAN?>" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="">
                                        Tanggal Kegiatan
                                    </label>
									<input type="date" name="tanggal_kegiatan" class="form-control" value="<?=$data[0]->TANGGAL_KEGIATAN?>" required>
								</div>
							</div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan')?>" class="btn btn-secondary"> Batal </a>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
<?=$pesan?>
<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<script src="<?=base_url('plugins/select2/js/select2.full.min.js')?>"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Initialize Select2 Elements
    $('.select2').select2()
  })
  
</script>