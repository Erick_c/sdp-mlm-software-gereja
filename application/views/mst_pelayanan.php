<?php $this->load->view('header_admin',['title'=>$_SESSION['NAMA_PELAYANAN']]);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'pelayanan'])?>
<?php date_default_timezone_set("Asia/Bangkok");?>
<link rel="stylesheet" href="<?=base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>"><!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Pelayanan</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Beranda</a></li>
            <li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
            <li class="breadcrumb-item active">Pelayanan</li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            <div class="card-header p-2">
            <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link <?=(count($data_update_pelayanan)==0)?'active':''?> " href="#pelayanan" data-toggle="tab">Pelayanan</a></li>
                <li class="nav-item"><a class="nav-link" href="#insert_pelayanan" data-toggle="tab">Tambah Kategori</a></li>
                <li class="nav-item"><a class="nav-link" href="#penugasan" data-toggle="tab">Penugasan</a></li>
            </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
            <div class="tab-content">
                <div class="<?=(count($data_update_pelayanan)==0)?'active':''?> tab-pane" id="pelayanan">		
					<style>
						table.table-responsive th{
							width:100%;
							min-width:200px;
						}
						
						table.table-responsive td
						{
							line-height:auto;
						}
					</style>
					  <?php 
						$this->table->set_template([
							'table_open'=>'<table id="" class="with_search table table-bordered table-strip table-responsive">',
						]);
						
						//$tampilkan = [[['style'=>'width:100px;min-width:100px','data'=>'Foto'],'Nama','Jenis Kelamin','Alamat Jemaat',['style'=>'min-width:120px;']]];
						$tampilkan = [[['style'=>'width:255px;min-width:255px', 'data'=>'NAMA PELAYANAN'], 'DESKRIPSI PELAYANAN',['style'=>'width:150px;min-width:150px'],['style'=>'width:150px;min-width:150px']]];
						for($i=0;$i<count($table_pelayanan);$i++){
                            $data = [];
                            $data[] = $table_pelayanan[$i]->NAMA_PELAYANAN;
                            $data[] = $table_pelayanan[$i]->DESKRIPSI_PELAYANAN;
                            $id_pelayanan = $table_pelayanan[$i]->ID_PELAYANAN;
                            $data[] = "<center><a href='".base_url('/beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-pelayanan/'.$id_pelayanan)."' class='btn btn-primary'>List Pelayanan</a></center>";
                            $data[] = "<center><a href='".base_url('/beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/'.$id_pelayanan)."' class='btn btn-primary'>Ubah Data</a></center>";
                            $tampilkan[]=$data;
						}
						echo $this->table->generate($tampilkan);
					  ?>
				</div>
                <div class="tab-pane" id="insert_pelayanan">		
                    <form class="form-horizontal" action='<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/insert-pelayanan')?>' method="post">
                        <div class="form-group row">
                            <label for="nama_pelayanan" class="col-sm-2 col-form-label">Nama Pelayanan</label>
                            <div class="col-sm-10">
                                <input type="text" name="nama_pelayanan" value="" class="form-control" id="nama_pelayanan" placeholder="Nama Pelayanan" required=required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="deskripsi_pelayanan" class="col-sm-2 col-form-label">Deskripsi Pelayanan</label>
                            <div class="col-sm-10">
                                <textarea name="deskripsi_pelayanan" style="resize:none;" class="form-control" id="deskripsi_pelayanan" placeholder="Deskripsi Pelayanan"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Kirim</button>
                            </div>
                        </div>
                    </form>
                </div>
                <?php if(count($data_update_pelayanan) > 0) { ?>
                    <div class="active tab-pane" id="update_pelayanan">		
                        <form class="form-horizontal" action='<?=base_url("beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/update-pelayanan/".$data_update_pelayanan[0]->ID_PELAYANAN)?>' method="post">
                            <div class="form-group row">
                                <label for="nama_pelayanan1" class="col-sm-2 col-form-label">Nama Pelayanan</label>
                                <div class="col-sm-10">
                                    <input type="text" name="nama_pelayanan" value="<?=$data_update_pelayanan[0]->NAMA_PELAYANAN?>" class="form-control" id="nama_pelayanan1" placeholder="Nama Pelayanan" required=required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="deskripsi_pelayanan1" class="col-sm-2 col-form-label">Deskripsi Pelayanan</label>
                                <div class="col-sm-10">
                                    <textarea name="deskripsi_pelayanan" style="resize:none;" class="form-control" id="deskripsi_pelayanan1" placeholder="Deskripsi Pelayanan" ><?=$data_update_pelayanan[0]->DESKRIPSI_PELAYANAN?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">Kirim</button>
                                </div>
                            </div>
                        </form>
                    </div>
                <?php } ?>
                <div class="tab-pane" id="penugasan">
                    <style>
						table#table-responsive th{
							width:100%;
							min-width:200px;
						}
						
						table#table-responsive td
						{
							line-height:auto;
						}
					</style>
					  <?php 
						$this->table->set_template([
							'table_open'=>'<table id="" class="with_search table table-bordered table-strip table-responsive">',
						]);
						
						//$tampilkan = [[['style'=>'width:100px;min-width:100px','data'=>'Foto'],'Nama','Jenis Kelamin','Alamat Jemaat',['style'=>'min-width:120px;']]];
						$tampilkan = [[['style'=>'width:300px;min-width:300px', 'data'=>'NAMA KEGIATAN'], 'CABANG',['style'=>'width:500px;min-width:500px', 'data'=>'DESKRIPSI KEGIATAN'],['style'=>'width:150px;min-width:150px']]];
						for($i=0;$i<count($table_kegiatan);$i++){
                            $data = [];
                            $data[] = $table_kegiatan[$i]->NAMA_KEGIATAN;
                            // $data[] = $table_kegiatan[$i]->ID_KEGIATAN;
                            $data[] = $table_kegiatan[$i]->NAMA_CABANG;
                            $data[] = $table_kegiatan[$i]->NAMA_KATEGORI;
                            $id_kegiatan = $table_kegiatan[$i]->ID_KEGIATAN;
                            $data[] = "<center><a href='".base_url('/beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-penugasan/'.$id_kegiatan)."' class='btn btn-primary'>List Penugasan</a></center>";
                            $tampilkan[]=$data;
						}
						echo $this->table->generate($tampilkan);
					  ?>
                </div>
            </div>
            <!-- /.tab-content -->
            </div><!-- /.card-body -->
        </div>
        <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>

<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>