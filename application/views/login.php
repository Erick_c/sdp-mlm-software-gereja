<?php $this->load->view('header',['title'=>'Masuk Pelayanan']);?>
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=base_url('modification-login/bootstrap/css/bootstrap.min.css')?>">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=base_url('fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=base_url('modification-login/animate/animate.css')?>">
    <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="<?=base_url('modification-login/css-hamburgers/hamburgers.min.css')?>">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=base_url('modification-login/select2/select2.min.css')?>">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?=base_url('css/util.css')?>">
        <link rel="stylesheet" type="text/css" href="<?=base_url('css/main.css')?>">
    <!--===============================================================================================-->
	<!-- <div class="limiter"> -->
		<div class="container-login100" style="background:none;background-image:linear-gradient(silver,white,silver);background-attachment:fixed;">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="<?=base_url('img/core-img/logo.png')?>" alt="IMG" width='100%'>
				</div>

				<form class="login100-form validate-form" method="post">
					<span class="login100-form-title">
						Masuk Pelayanan
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Nama pengguna harus terisi">
						<input class="input100" type="text" name="nama_jemaat" placeholder="Nama Pengguna">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password harus terisi">
						<input class="input100" type="password" name="password" placeholder="Kata Sandi">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<?php if ($pesan!='') echo '<center><b style="color:red"> Akun anda tidak diketahui </b></center>';?>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Masuk
						</button>
					</div>

					<div class="text-center p-t-136">
					</div>
				</form>
			</div>
		<!-- </div> -->
	<!-- </div> -->
	
	

	
<!--===============================================================================================-->	
	<script src="<?=base_url('modification-login/jquery/jquery-3.2.1.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?=base_url('modification-login/bootstrap/js/popper.js')?>"></script>
	<script src="<?=base_url('modification-login/bootstrap/js/bootstrap.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?=base_url('modification-login/select2/select2.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?=base_url('modification-login/tilt/tilt.jquery.min.js')?>"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="<?=base_url('js/main.js')?>"></script>

<?php $this->load->view('loading');?>