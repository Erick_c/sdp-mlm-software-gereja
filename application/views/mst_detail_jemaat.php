<?php $this->load->view('header_admin',['title'=>$_SESSION['NAMA_PELAYANAN']]);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'list jemaat'])?>
<?php date_default_timezone_set("Asia/Bangkok");?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Detail Jemaat</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Beranda</a></li>
            <li class="breadcrumb-item active">Detail Jemaat</li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <div class="profile-user-img img-fluid img-circle" style='background-image:url(<?php echo "data:image/jpeg;base64,".base64_encode($data[0]->FOTO);?>);height :100px;width:100px;background-size:auto 100%;background-position:center;'></div>
                    </div>
                    <h3 class="profile-username text-center"><?=$data[0]->NAMA_JEMAAT?></h3>
                    <p class="text-muted text-center"><?=$data[0]->ID_JEMAAT?></p>
                    <center><a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-jemaat')?>" class="btn btn-primary"><i class='fa fa-arrow-left nav-icon'></i> Kembali </a></center>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <!-- About Me Box -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Biodata</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <strong><i class="fas fa-book mr-1"></i> No Telepon </strong>

                    <p class="text-muted">
                        <?=$data[0]->NO_TELPON?>
                    </p>

                    <hr>

                    <strong><i class="fas fa-book mr-1"></i> Tanggal Lahir </strong>

                    <p class="text-muted">
                        <?=date('d F o',strtotime($data[0]->TANGGAL_LAHIR))?>
                    </p>

                    <hr>

                    <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>
                    
                    <p class="text-muted">
                        <?=$_SESSION['ALAMAT_JEMAAT']?>
                    </p>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                <h3 class="card-title">Keluarga <b> <?=$data[0]->NAMA_JEMAAT; ?></b></h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <?php 
                    $this->table->set_template([
                        'table_open'=>'<table id="" class="without_search table table-bordered table-strip">',
                    ]);
                    
                    $tampilkan = [['Nama Jemaat','Status']];
                    for ($i=0;$i<count($data_hubkel);$i++)
                    {
                        $tampilkan[] = [
                            $data_hubkel[$i]->NAMA_JEMAAT,
                            $data_hubkel[$i]->STATUS
                        ];
                    }

                    echo $this->table->generate($tampilkan);
                    ?>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
    </div>
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>

<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>