<?php $this->load->view('header_admin',['title'=>'Jemaat Baru']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'jemaat baru']);?>
<link rel="stylesheet" href="<?=base_url('plugins/select2/css/select2.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')?>">
<style>
	span.select2-selection.select2-selection--single
	{
		height:initial;
	}
</style>
<div class="content-wrapper" style="min-height: 1200.88px;">
    <section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Jemaat Baru</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?=base_url('/')?>">Beranda</a></li>
						<li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
						<li class="breadcrumb-item active">Jemaat Baru</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<form action="" role="form" method="post"enctype="multipart/form-data">
							<div class="card-body">
								<div class="form-group">
									<label for="">
                                        Nama IC
                                    </label>
									<?php 
										$tampilan = [''=>'-- Pilih IC --'];
										for($i=0; $i<count($data_ic); $i++){
											$tampilan[$data_ic[$i]->NO_IC]=$data_ic[$i]->NAMA_IC;
										}
										echo form_dropdown('no_ic',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Nama Jemaat
                                    </label>
									<input type="text" class="form-control" name="nama_jemaat" required>
								</div>
								<div class="form-group">
									<label for="">
                                        Jenis Kelamin
                                    </label>
									<div class="form-check">
									  <input class="form-check-input" type="radio" id="jkL" name="jenis_kelamin" value="L"required>
									  <label class="form-check-label" for="jkL">Laki-laki</label>
									</div>
									<div class="form-check">
									  <input class="form-check-input" type="radio" id="jkP" name="jenis_kelamin" value="P"required>
									  <label class="form-check-label" for="jkP">Perempuan</label>
									</div>
								</div>
								<div class="form-group">
									<label for="">
                                        Tanggal Lahir
                                    </label>
									<input type="date" class="form-control" min="1000" max="3000" name="tanggal_lahir"required>
								</div>
								<div class="form-group">
									<label for="">
                                        Alamat
                                    </label>
									<textarea class="form-control" style="resize:none;"name="alamat_jemaat"required></textarea>
								</div>
								<div class="form-group">
									<label for="">
                                        No Telp
                                    </label>
									<input type="number" class="form-control" name="no_telpon">
								</div>
								<div class="form-group">
				                    <label for="exampleInputFile">Foto Jemaat (besar maksimum = 1000Kb, yang diperbolehkan .jpg , .jpeg, .png)</label>
									<div class="input-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input" required accept=".jpg,.jpeg,.png" name="foto" id="exampleInputFile">
										<label class="custom-file-label" for="exampleInputFile">Choose file</label>
									</div>
									<div class="input-group-append">
									</div>
									</div>
								</div>
								<div class="form-group">
									<label for="">
                                        Status Baptis
                                    </label>
									<div class="form-check">
									  <input class="form-check-input" type="radio" id="sudah-baptis" name="status_baptis" value="T"required>
									  <label class="form-check-label" for="sudah-baptis">Sudah Baptis</label>
									</div>
									<div class="form-check">
									  <input class="form-check-input" type="radio" id="belum-baptis" name="status_baptis" value="F"required>
									  <label class="form-check-label" for="belum-baptis">Belum Baptis</label>
									</div>
								</div>

							</div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-jemaat')?>" class="btn btn-secondary"> Batal </a>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
<?=$pesan?>	
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<script src="<?=base_url('plugins/select2/js/select2.full.min.js')?>"></script>
<script src="<?=base_url('dist/js/bs-custom-file-input.min.js')?>"></script>
<script src="<?=base_url('dist/js/bs-custom-file-input.js')?>"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Initialize Select2 Elements
    $('.select2').select2()
  })

  $(document).ready(function () {
  bsCustomFileInput.init()
})
</script>