<?php $this->load->view('header_admin',['title'=>'Kegiatan Baru']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'kegiatan baru']);?>
<link rel="stylesheet" href="<?=base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2/css/select2.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')?>">
<style>
	span.select2-selection.select2-selection--single
	{
		height:initial;
	}
</style>
<div class="content-wrapper" style="min-height: 1200.88px;">
    <section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Kegiatan Baru</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?=base_url('/')?>">Beranda</a></li>
						<li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
						<li class="breadcrumb-item active">Kegiatan Baru</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="card">
			<div class="card-header p-2">
				<ul class="nav nav-pills">
					<li class="nav-item"><a class="nav-link active" href="#mingguan" data-toggle="tab">Mingguan</a></li>
					<li class="nav-item"><a class="nav-link" href="#ic" data-toggle="tab">IC</a></li>
					<li class="nav-item"><a class="nav-link" href="#lainnya" data-toggle="tab">Lain-lain</a></li>
				</ul>
			</div>
			<!-- /.card-header -->
			<div class="card-body">
				<div class="tab-content">
					<div class="tab-pane active" id="mingguan">
						<form action="" method="post" role="form">
							<div class="card-body">
								<div class="form-group">
									<label for="">
										Topik Kegiatan Mingguan
									</label>
									<input type="text" name="nama_kegiatan" required class="form-control">
								</div>
								<!-- <div class="form-group">
									<label for="">
										Pembicara atau Pendeta
									</label>									
									<?php
										$tampilan = [''=>'-- Pilih Pembicara --'];
										for($i=0; $i<count($data_pembicara); $i++){
											$tampilan[$data_pembicara[$i]->ID_PEMBICARA]=$data_pembicara[$i]->NAMA_PEMBICARA;
										}
										echo form_dropdown('id_pembicara',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div> -->
								<div class="form-group">
									<label for="">
										Kategori Kegiatan
									</label>									
									<?php
										$tampilan = [''=>'-- Pilih Kategori --'];
										for($i=0; $i<count($data_kategori); $i++){
											$tampilan[$data_kategori[$i]->ID_KATEGORI]=$data_kategori[$i]->NAMA_KATEGORI;
										}
										echo form_dropdown('id_kategori',$tampilan,'',["class"=>"form-control select2","required"=>"required"]);
									?>
								</div>
								<div class="form-group">
									<label for="">
										Nama Cabang
									</label>
									<?php
										$tampilan = [''=>'-- Pilih Cabang --'];
										for($i=0; $i<count($data_cabang); $i++){
											$tampilan[$data_cabang[$i]->ID_CABANG]=$data_cabang[$i]->NAMA_CABANG;
										}
										echo form_dropdown('id_cabang',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<div class="form-group">
									<label for="">
										Waktu Kegiatan
									</label>
									<input type="time" name="waktu_kegiatan" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="">
										Tanggal Kegiatan
									</label>
									<input type="date" name="tanggal_kegiatan" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="">
										Pembicara atau Pendeta
									</label>

									<?php 
										$this->table->set_template([
											'table_open'=>'<table id="" class="with_search table table-bordered table-strip">',
										]);
										
										$tampilan = [['','Nama Pembicara atau Pendeta','Asal Pembicara atau Pendeta']];

										for ($i=0; $i < count($data_pembicara); $i++) { 
											$tampilan[$i+1][] = "<input type='radio' style='float:right;margin-top:5px' name='id_pembicara' id='".$data_pembicara[$i]->ID_PEMBICARA."' value='".$data_pembicara[$i]->ID_PEMBICARA."'/>";
											$tampilan[$i+1][] = "<label for='".$data_pembicara[$i]->ID_PEMBICARA."' style='font-weight:inherit'>".$data_pembicara[$i]->NAMA_PEMBICARA."</label>";
											$tampilan[$i+1][] = "<label for='".$data_pembicara[$i]->ID_PEMBICARA."' style='font-weight:inherit'>".$data_pembicara[$i]->ALAMAT_RUMAH."</label>";
										}

										echo $this->table->generate($tampilan);
									?>
								</div>
							</div>
							<div class="card-footer">
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>
						</form>
					</div>
					<div class="tab-pane" id="ic">
						<form action="" method="post" role="form">
							<div class="card-body">
								<div class="form-group">
									<label for="">
										Topik Kegiatan IC
									</label>
									<input type="text" name="nama_kegiatan" required class="form-control">
								</div>
								<div class="form-group">
									<label for="">
										Pembicara atau Pendeta
									</label>									
									<?php
										$tampilan = [''=>'-- Pilih Pembicara --'];
										for($i=0; $i<count($data_pembicara); $i++){
											$tampilan[$data_pembicara[$i]->ID_PEMBICARA]=$data_pembicara[$i]->NAMA_PEMBICARA;
										}
										echo form_dropdown('id_pembicara',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Lokasi Kegiatan
                                    </label>
									<input type="text" name="lokasi_kegiatan" class="form-control"required>
								</div>
								<div class="form-group">
									<label for="">
										IC Yang Bersangkutan
                                    </label>									
									<?php
										$tampilan = [''=>'-- Pilih IC --'];
										for($i=0; $i<count($data_ic); $i++){
											$tampilan[$data_ic[$i]->NO_IC]=$data_ic[$i]->NAMA_IC;
										}
										echo form_dropdown('no_ic',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
									<input type="hidden" name="id_kategori" value="<?php
										for ($i=0; $i < count($data_kategori); $i++) { 
											if(strpos(strtolower($data_kategori[$i]->NAMA_KATEGORI),'ic') > -1) echo $data_kategori[$i]->ID_KATEGORI;
										}	
									?>">
								</div>
								<div class="form-group">
									<label for="">
										Waktu Kegiatan
									</label>
									<input type="time" name="waktu_kegiatan" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="">
										Tanggal Kegiatan
									</label>
									<input type="date" name="tanggal_kegiatan" class="form-control" required>
								</div>
							</div>
							<div class="card-footer">
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>
						</form>
					</div>
					<div class="tab-pane" id="lainnya">
						<form action="" method="post" role="form">
							<div class="card-body">
								<div class="form-group">
									<label for="">
										Topik Kegiatan 
									</label>
									<input type="text" name="nama_kegiatan" required class="form-control">
								</div>
								<div class="form-group">
									<label for="">
										Pembicara atau Pendeta
									</label>									
									<?php
										$tampilan = [''=>'-- Pilih Pembicara --'];
										for($i=0; $i<count($data_pembicara); $i++){
											$tampilan[$data_pembicara[$i]->ID_PEMBICARA]=$data_pembicara[$i]->NAMA_PEMBICARA;
										}
										echo form_dropdown('id_pembicara',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Lokasi Kegiatan
                                    </label>
									<input type="text" name="lokasi_kegiatan" class="form-control"required>
								</div>
								<div class="form-group">
									<label for="">
										IC Yang Bersangkutan
                                    </label>									
									<?php
										$tampilan = [''=>'-- Pilih IC --'];
										for($i=0; $i<count($data_ic); $i++){
											$tampilan[$data_ic[$i]->NO_IC]=$data_ic[$i]->NAMA_IC;
										}
										echo form_dropdown('no_ic',$tampilan,'',["class"=>"form-control select2"])
									?>
								</div>
								<div class="form-group">
									<label for="">
										Kategori Kegiatan
									</label>									
									<?php
										$tampilan = [''=>'-- Pilih Kategori --'];
										for($i=0; $i<count($data_kategori); $i++){
											$tampilan[$data_kategori[$i]->ID_KATEGORI]=$data_kategori[$i]->NAMA_KATEGORI;
										}
										echo form_dropdown('id_kategori',$tampilan,'',["class"=>"form-control select2","required"=>"required"]);
									?>
								</div>
								<div class="form-group">
									<label for="">
										Nama Cabang
									</label>
									<?php
										$tampilan = [''=>'-- Pilih Cabang --'];
										for($i=0; $i<count($data_cabang); $i++){
											$tampilan[$data_cabang[$i]->ID_CABANG]=$data_cabang[$i]->NAMA_CABANG;
										}
										echo form_dropdown('id_cabang',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<div class="form-group">
									<label for="">
										Waktu Kegiatan
									</label>
									<input type="time" name="waktu_kegiatan" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="">
										Tanggal Kegiatan
									</label>
									<input type="date" name="tanggal_kegiatan" class="form-control" required>
								</div>
							</div>
							<div class="card-footer">
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php /*
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<form action="" method="post" role="form">
							<div class="card-body">
								<div class="form-group">
									<label for="">
                                        Nama Kegiatan (*)
                                    </label>
									<input type="text" name="nama_kegiatan" class="form-control">
								</div>
								<div class="form-group">
									<label for="">
										Kategori Kegiatan
                                    </label>									
									<?php
										$tampilan = [''=>'-- Pilih Kategori --'];
										for($i=0; $i<count($data_kategori); $i++){
											$tampilan[$data_kategori[$i]->ID_KATEGORI]=$data_kategori[$i]->NAMA_KATEGORI;
										}
										echo form_dropdown('id_kategori',$tampilan,'',["class"=>"form-control select2"]);
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Nama Cabang (*)
                                    </label>
									<?php
										$tampilan = [''=>'-- Pilih Cabang --'];
										for($i=0; $i<count($data_cabang); $i++){
											$tampilan[$data_cabang[$i]->ID_CABANG]=$data_cabang[$i]->NAMA_CABANG;
										}
										echo form_dropdown('id_cabang',$tampilan,'',["class"=>"form-control select2"])
									?>
								</div>
								<div class="form-group">
									<label for="">
										Nama IC (*)
                                    </label>									
									<?php
										$tampilan = [''=>'-- Pilih IC --'];
										for($i=0; $i<count($data_ic); $i++){
											$tampilan[$data_ic[$i]->NO_IC]=$data_ic[$i]->NAMA_IC;
										}
										echo form_dropdown('no_ic',$tampilan,'',["class"=>"form-control select2"])
									?>
								</div>
								<div class="form-group">
									<label for="">
										Pembicara
                                    </label>									
									<?php
										$tampilan = [''=>'-- Pilih Pembicara --'];
										for($i=0; $i<count($data_pembicara); $i++){
											$tampilan[$data_pembicara[$i]->ID_PEMBICARA]=$data_pembicara[$i]->NAMA_PEMBICARA;
										}
										echo form_dropdown('id_pembicara',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Lokasi Kegiatan
                                    </label>
									<input type="text" name="lokasi_kegiatan" class="form-control"required>
								</div>
								<div class="form-group">
									<label for="">
                                        Waktu Kegiatan
                                    </label>
									<input type="time" name="waktu_kegiatan" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="">
                                        Tanggal Kegiatan
                                    </label>
									<input type="date" name="tanggal_kegiatan" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="">
                                        Lokasi Berkumpul (*)
                                    </label>
									<input type="text" name="tempat_berkumpul" class="form-control" >
								</div>
								<div class="form-group">
									<label for="">
                                        Waktu Berkumpul (*)
                                    </label>
									<input type="time" name="waktu_berkumpul" class="form-control">
								</div>
							</div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan')?>" class="btn btn-secondary"> Batal </a>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	/** */ ?>
<?=$pesan?>

<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- Select2 -->
<script src="<?=base_url('plugins/select2/js/select2.full.min.js')?>"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="<?=base_url('plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js')?>"></script>
<!-- InputMask -->
<!-- <script src="<?=base_url('plugins/moment/moment.min.js')?>"></script> -->
<!-- <script src="<?=base_url('plugins/inputmask/min/jquery.inputmask.bundle.min.js')?>"></script> -->
<!-- date-range-picker -->
<script src="<?=base_url('plugins/daterangepicker/daterangepicker.js')?>"></script>
<!-- bootstrap color picker -->
<script src="<?=base_url('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')?>"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=base_url('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')?>"></script>
<!-- Bootstrap Switch -->
<script src="<?=base_url('plugins/bootstrap-switch/js/bootstrap-switch.min.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Initialize Select2 Elements
    $('.select2').select2()
/*
    //Datemask dd/mm/yyyy
    //$('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    //$('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    //$('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
/** */
  })
</script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>