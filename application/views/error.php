<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('header',['title'=>'Error']);?>
    <section class="about-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- Section Heading -->
                <div class="col-12">
                    <div class="section-heading mb-100">
                        <h2><?=$title?></h2>
						<p><?=$data?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('footer');?>