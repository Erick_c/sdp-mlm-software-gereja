<?php $this->load->view('header_admin',['title'=>'Ubah Data Jemaat']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'list jemaat']);?>
<link rel="stylesheet" href="<?=base_url('plugins/select2/css/select2.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')?>">
<style>
	span.select2-selection.select2-selection--single
	{
		height:initial;
	}
</style>
<div class="content-wrapper" style="min-height: 1200.88px;">
    <section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Ubah Data Jemaat</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?=base_url('/')?>">Beranda</a></li>
						<li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
						<li class="breadcrumb-item active">Ubah Data Jemaat</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<form action="" role="form" method="post">
							<div class="card-body">
								<div class="form-group">
									<label for="">
                                        Nama IC
                                    </label>
									<?php 
										$tampilan = [''=>'-- Pilih IC --'];
										for($i=0; $i<count($data_ic); $i++){
											$tampilan[$data_ic[$i]->NO_IC]=$data_ic[$i]->NAMA_IC;
										}
										$a = $data_default[0]->NO_IC;
										echo form_dropdown('no_ic',$tampilan,$a,["class"=>"form-control select2"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Nama Jemaat
                                    </label>
									<input type="text" class="form-control" name="nama_jemaat" value="<?=$data_default[0]->NAMA_JEMAAT;?>" required>
								</div>
								<div class="form-group">
									<label for="">
                                        Jenis Kelamin
                                    </label>
									<div class="form-check">
									  <input class="form-check-input" type="radio" id="jkL" name="jenis_kelamin" <?php if ($data_default[0]->JENIS_KELAMIN == 'L'){echo "checked";};?> value="L"required>
									  <label class="form-check-label" for="jkL">Laki-laki</label>
									</div>
									<div class="form-check">
									  <input class="form-check-input" type="radio" id="jkP" name="jenis_kelamin" <?php if ($data_default[0]->JENIS_KELAMIN == 'P'){echo "checked";};?> value="P"required>
									  <label class="form-check-label" for="jkP">Perempuan</label>
									</div>
								</div>
								<div class="form-group">
									<label for="">
                                        Tanggal Lahir
                                    </label>
									<input type="date" class="form-control" min="1000" max="3000" name="tanggal_lahir" value="<?=$data_default[0]->TANGGAL_LAHIR;?>" required>
								</div>
								<div class="form-group">
									<label for="">
                                        Alamat
                                    </label>
									<input type="textarea" class="form-control" style="resize:none;"name="alamat_jemaat" value="<?=$data_default[0]->ALAMAT_JEMAAT;?>" required></textarea>
								</div>
								<div class="form-group">
									<label for="">
                                        No Telp
                                    </label>
									<input type="number" class="form-control" name="no_telpon" value="<?=$data_default[0]->NO_TELPON;?>">
								</div>
								<div class="form-group">
									<label for="">
                                        Status Baptis
                                    </label>
									<div class="form-check">
									  <input class="form-check-input" type="radio" id="sudah-baptis" name="status_baptis" <?php if ($data_default[0]->STATUS_BAPTIS == 'T'){echo "checked";};?> value="T"required>
									  <label class="form-check-label" for="sudah-baptis">Sudah Baptis</label>
									</div>
									<div class="form-check">
									  <input class="form-check-input" type="radio" id="belum-baptis" name="status_baptis" <?php if ($data_default[0]->STATUS_BAPTIS == 'F'){echo "checked";};?> value="F"required>
									  <label class="form-check-label" for="belum-baptis">Belum Baptis</label>
									</div>
								</div>

							</div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-jemaat')?>" class="btn btn-secondary"> Batal </a>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
<?=$pesan?>	
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<script src="<?=base_url('plugins/select2/js/select2.full.min.js')?>"></script>
<script src="<?=base_url('dist/js/bs-custom-file-input.min.js')?>"></script>
<script src="<?=base_url('dist/js/bs-custom-file-input.js')?>"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Initialize Select2 Elements
    $('.select2').select2()
  })

  $(document).ready(function () {
  bsCustomFileInput.init()
})
</script>