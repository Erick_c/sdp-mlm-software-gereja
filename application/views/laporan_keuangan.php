<?php $this->load->view('header_admin',['title'=>'Laporan Keuangan']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'Laporan Transaksi']);?>
<style type="text/css">/* Chart.js */
@keyframes chartjs-render-animation{from{opacity:.99}to{opacity:1}}.chartjs-render-monitor{animation:chartjs-render-animation 1ms}.chartjs-size-monitor,.chartjs-size-monitor-expand,.chartjs-size-monitor-shrink{position:absolute;direction:ltr;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1}.chartjs-size-monitor-expand>div{position:absolute;width:1000000px;height:1000000px;left:0;top:0}.chartjs-size-monitor-shrink>div{position:absolute;width:200%;height:200%;left:0;top:0}</style>
<div class="content-wrapper" style="min-height: 976.13px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Laporan Keuangan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Beranda</a></li>
              <li class="breadcrumb-item active">Laporan Keuangan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
			<div class="col-md-12">
				<div class="card">
					<!-- Start of "Transaction Date Search" -->
					<div class="card-body">
						<!-- Title -->
						<div class="row">
							<label class="col" for="">
								Range Tanggal Transaksi
							</label>
						</div>
						<!-- Radiobutton -->
						<div class="row">
							<div class=" col-md-6 form-group form-check form-check inline">
							<input class="form-check-input" type="checkbox" name="ccari" value="ketemu" id="ccari">
								<label class="form-check-label" for="ccari">Pencarian Detail</label>
							</div>
							<div class=" col-md-2 form-group form-check form-check inline">
								<input class="form-check-input" type="radio" name="radio_tanggal" value="Hari" id="rhari" checked required>
								<label class="form-check-label" for="rhari">Per Hari</label>
							</div>
							<div class=" col-md-2 form-group form-check form-check inline">
								<input class="form-check-input" type="radio" name="radio_tanggal" value="Bulan" id="rbulan" required>
								<label class="form-check-label" for="rbulan">Per Bulan</label>
							</div>
							<div class=" col-md-2 form-group form-check form-check inline">
								<input class="form-check-input" type="radio" name="radio_tanggal" value="Tahun" id="rtahun" required>
								<label class="form-check-label" for="rtahun">Per Tahun</label>
							</div>
						</div>
						<!-- Dropdown -->
						<div class="row">
							
							<div class=" col-md-3 form-group">
								<?php 
										$tampilan = [];
										for($i=1; $i<32; $i++){
											$tampilan[$i]=$i;
										}
										echo form_dropdown('sort_hari',$tampilan,date('d'),["class"=>"form-control select2","id"=>"sort_hari"])
									?>
							</div>
							<div class=" col-md-5 form-group">
								<?php 
										$tampilan = [1=>"Januari",2=>"Februari",3=>"Maret",4=>"April",5=>"Mei",6=>"Juni",7=>"Juli",8=>"Agustus",9=>"September",10=>"Oktober",11=>"November",12=>"Desember"];
										echo form_dropdown('sort_bulan',$tampilan,date('m'),["class"=>"form-control select2","id"=>"sort_bulan"])
									?>
							</div>
							<div class=" col-md-4 form-group">
								<?php	
										$tampilan = [];
										for($i=1970; $i<2101; $i++){
											$tampilan[$i]=$i;
										}
										echo form_dropdown('sort_tahun',$tampilan,date('Y'),["class"=>"form-control select2","id"=>"sort_tahun"])
									?>
							</div>
						</div>
						<div class="row" id="hideme">
							
							<div class=" col-md-3 form-group">
								<?php 
										$tampilan = [];
										for($i=1; $i<32; $i++){
											$tampilan[$i]=$i;
										}
										echo form_dropdown('sort_hari2',$tampilan,date('d'),["class"=>"form-control select2","id"=>"sort_hari2"])
									?>
							</div>
							<div class=" col-md-5 form-group">
								<?php 
										$tampilan = [1=>"Januari",2=>"Februari",3=>"Maret",4=>"April",5=>"Mei",6=>"Juni",7=>"Juli",8=>"Agustus",9=>"September",10=>"Oktober",11=>"November",12=>"Desember"];
										echo form_dropdown('sort_bulan2',$tampilan,date('m'),["class"=>"form-control select2","id"=>"sort_bulan2"])
									?>
							</div>
							<div class=" col-md-4 form-group">
								<?php	
										$tampilan = [];
										for($i=1970; $i<2101; $i++){
											$tampilan[$i]=$i;
										}
										echo form_dropdown('sort_tahun2',$tampilan,date('Y'),["class"=>"form-control select2","id"=>"sort_tahun2"])
									?>
							</div>
						</div>
					</div>
					<!-- End of "Transaction Date Search" -->
				</div>
			</div>
			
          <div class="col-md-12">
						
            <!-- BAR CHART -->
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Kategori</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                  <canvas id="barChart" style="height: 230px; min-height: 230px; display: block; width: 974px;" width="487" height="230" class="chartjs-render-monitor"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
		   </div>
		   
		   <div class="col-md-6">			
            <!-- LAPORAN KEUANGAN -->
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Laporan Keuangan</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
				<div class="row" style="height: 115px; min-height: 115px;">
					<div class="col-md-6">
						<h3 id="h3pengeluaran">Rp. 0</h3>
						<h5>Total Pengeluaran</h5>
					</div>
					<div class="col-md-6">
						<h3 id="h3pemasukkan">Rp. 0</h3>
						<h5>Total Pemasukkan</h5>
					</div>
				</div>
				<div class="row"style="height: 115px; min-height: 115px;">
					<div class="col-md-6">
						<h3 id="h3balance">Rp. 0</h3>
						<h5>Total Balance</h5>
					</div>
					<div class="col-md-6">
						<h3 id="h3kas">Rp. 0</h3>
						<h5>Kas Gereja</h5>
					</div>
				</div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
		   </div>
		   
		   <div class="col-md-6">
            <!-- LINE CHART -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Line Chart</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                  <canvas id="lineChart" style="height: 230px; min-height: 230px; display: block; width: 487px;" width="487" height="230" class="chartjs-render-monitor"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-6">
            <!-- DONUT CHART -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Pemasukkan</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                <canvas id="donutChart" class="Pemasukkan" style="height: 230px; min-height: 230px; display: block; width: 487px;" width="487" height="230" class="chartjs-render-monitor"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
		   </div>
		   <div class="col-md-6">
            <!-- DONUT CHART -->
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Pengeluaran</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                <canvas id="donutChart" class="Pengeluaran" style="height: 230px; min-height: 230px; display: block; width: 487px;" width="487" height="230" class="chartjs-render-monitor"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('plugins/chart.js/Chart.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */
	 
	 var barchart = '';
	 var lineChart = '';
	 var donutChartPengeluaran = '';
	 var donutChartPemasukkan = '';
	//settingan chart
	//settingan chart bar
	var barChartOptions = {
		datasetFill : false,
		maintainAspectRatio : false,
		responsive : true,
		tooltips:{mode:'index',intersect:false},
		legend: {
			display: true
		},
		scales: {
			xAxes: [{
				stacked:true,
				gridLines : {
					display : false,
				}
			}],
			yAxes: [{
				stacked:true,
				gridLines : {
					display : false,
				}
			}]
		}
    }
	//settingan chart line
	var lineChartOptions = {
		datasetFill : false,
		maintainAspectRatio : false,
		responsive : true,
		smooth : false,
		legend: {
			display: true
		},
		scales: {
			xAxes: [{
				gridLines : {
					display : false,
				}
			}],
			yAxes: [{
				gridLines : {
					display : false,
				}
			}]
		}
	}
	//settingan chart donut
	var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
	  cutoutPercentage:80
    }
	//setting warna
	var warna_pengeluaran=['#75F4F4','#D17B88','#B8B3E9','#706F6F','#98C1D9','#2E0E02','#337357','#1E2019','#FFCF56','#EDEAD0'];
	var warna_bg_pengeluaran=['#90E0F3','#D999B9','#E0FBFC','#B0B5B3','#3D5A80','#581908','#6D9F71','394032','#D5B942','#BEB7A4'];
	var warna_pemasukkan=['#EF3054','#432818','#D5FFF3'];
	var warna_bg_pemasukkan=['#FF6F59','#99582A','C7EDE4'];
	var warna_line=['#A8F9FF','#6CAE75'];
	var warna_bg_line=['#9AE5E6','#8BBD8B'];
	//memunculkan chart waktu page load
	//coba diperbagus dan/atau dipersingkat
	//buatBarChart();
	//buatLineChart();
	//buatDonutPengeluaranChart();
	//buatDonutPemasukkanChart();
	//nanti panggil perTanggal()
	//jadi createSemuaChart()
	createSemuaChart();//membuat semua chart waktu page diload
	
	//---------------------------------------------Membuat bar chart
	function buatBarChart(data_chart){
		var datasets = [];
		for (var k = 0; k< data_chart['label_kredit'].length;k++ ){
			var data = [];
			for (var l = 0; l< data_chart[data_chart['label_kredit'][k]].length;l++ ){
				data.push(data_chart[data_chart['label_kredit'][k]][l]['uang']);
			}
			datasets.push({
				label: data_chart['label_kredit'][k],
				backgroundColor: warna_bg_pengeluaran[k],
				borderColor: warna_pengeluaran[k],
				stack:'Pengeluaran',
				data: data
			});
		}
		for (var k = 0; k< data_chart['label_debit'].length;k++ ){
			var data = [];
			for (var l = 0; l< data_chart[data_chart['label_debit'][k]].length;l++ ){
				data.push(data_chart[data_chart['label_debit'][k]][l]['uang']);
			}
			datasets.push({
				label: data_chart['label_debit'][k],
				backgroundColor: warna_bg_pemasukkan[k],
				borderColor: warna_pemasukkan[k],
				stack:'Pemasukkan',
				data: data
			});
		}
		var labels = [];
		for (var k = 0; k< data_chart['pengeluaran'].length;k++ ){
			labels.push(data_chart['pengeluaran'][k]['label']);
		}
		
		var barChartData = {
			labels: labels,
			datasets: datasets
		};
		
		//buat chartnya
		var barChartCanvas = $('#barChart').get(0).getContext('2d');
			barChart = new Chart(barChartCanvas, {
				type: 'bar',
				data: barChartData,
				options: barChartOptions
			});
		
	}
	//---------------------------------------------Membuat line chart
	function buatLineChart(data_keluar, data_masuk){
		var data_k = [];
		var data_d = [];
		var labels = [];
		for (var k = 0; k< data_keluar.length;k++ ){
			data_k.push(data_keluar[k]['uang']);
			data_d.push(data_masuk[k]['uang']);
			labels.push(data_keluar[k]['label']);
		}
		var lineChartData = {
		labels  : labels,
		datasets: [
				{
				label               : 'Pemasukan',
				backgroundColor     : warna_bg_line[0],
				borderColor         : warna_line[0],
				pointColor          :  warna_bg_line[0],
				pointStrokeColor    : warna_line[0],
				pointHighlightFill  : '#fff',
				pointHighlightStroke: warna_bg_line[0],
				fill 				: false,
				lineTension			: 0	,
				data                : data_d
				},
				{
				label               : 'Pengeluaran',
				backgroundColor     : warna_bg_line[1],
				borderColor         : warna_line[1],
				pointColor          : warna_bg_line[1],
				pointStrokeColor    : warna_line[1],
				pointHighlightFill  : '#fff',
				pointHighlightStroke: warna_bg_line[1],
				fill 				: false,
				lineTension			: 0,
				data                : data_k
				},
			]
		}
		
		
		var lineChartCanvas = $('#lineChart').get(0).getContext('2d')
	
			lineChart = new Chart(lineChartCanvas, { 
		type: 'line',
		data: lineChartData, 
		options: lineChartOptions
		})
	}
	//---------------------------------------------Membuat donut chart pemasukkan
	function buatDonutPemasukkanChart(data_chart){
		var data = [];
		for (var k = 0; k< data_chart['label_debit'].length;k++ ){
			var jumlah = 0;
			for (var l = 0; l< data_chart[data_chart['label_debit'][k]].length;l++ ){
				jumlah += data_chart[data_chart['label_debit'][k]][l]['uang'];
			}
			data.push(jumlah);
		}
		
		var donutChartCanvas = $('#donutChart.Pemasukkan').get(0).getContext('2d')
		var donutData        = {
		labels: data_chart['label_debit'],
		datasets: [
			{
			data: data,
			backgroundColor : warna_bg_pemasukkan,
			borderColor : warna_pemasukkan
			}
		]
		} 
		donutChartPemasukkan = new Chart(donutChartCanvas, {
		type: 'doughnut',
		data: donutData,
		options: donutOptions      
		})
	}
	//---------------------------------------------Membuat donut chart pengeluaran
	function buatDonutPengeluaranChart(data_chart){
		var data = [];
		for (var k = 0; k< data_chart['label_kredit'].length;k++ ){
			var jumlah = 0;
			for (var l = 0; l< data_chart[data_chart['label_kredit'][k]].length;l++ ){
				jumlah += data_chart[data_chart['label_kredit'][k]][l]['uang'];
			}
			data.push(jumlah);
		}
		
		donutChartCanvas = $('#donutChart.Pengeluaran').get(0).getContext('2d')
		donutData        = {
		labels: data_chart['label_kredit'],
		datasets: [
			{
				
			data: data,
			backgroundColor : warna_bg_pengeluaran,
			borderColor : warna_pemasukkan
			}
		]
		}
		//Create pie or douhnut chart
		// You can switch between pie and douhnut using the method below.
		donutChartPengeluaran = new Chart(donutChartCanvas, {
		type: 'doughnut',
		data: donutData,
		options: donutOptions      
		})
	}
    
	//isi balance laporan
    function isiTitleLaporan($data){
		$('#h3pengeluaran').text("Rp. "+$data['total_pengeluaran']);
		$('#h3pemasukkan').text("Rp. "+$data['total_pemasukkan']);
		$('#h3balance').text("Rp. "+$data['total_balance']);
		$('#h3kas').text("Rp. "+$data['total_kas']);
	}
    
	//---------------------------------------------Membuat bar chart
	function updateBarChart(data_chart){
		var datasets = [];
		for (var k = 0; k< data_chart['label_kredit'].length;k++ ){
			var data = [];
			for (var l = 0; l< data_chart[data_chart['label_kredit'][k]].length;l++ ){
				data.push(data_chart[data_chart['label_kredit'][k]][l]['uang']);
			}
			datasets.push({
				label: data_chart['label_kredit'][k],
				backgroundColor: warna_bg_pengeluaran[k],
				borderColor: warna_pengeluaran[k],
				stack:'Pengeluaran',
				data: data
			});
		}
		for (var k = 0; k< data_chart['label_debit'].length;k++ ){
			var data = [];
			for (var l = 0; l< data_chart[data_chart['label_debit'][k]].length;l++ ){
				data.push(data_chart[data_chart['label_debit'][k]][l]['uang']);
			}
			datasets.push({
				label: data_chart['label_debit'][k],
				backgroundColor: warna_bg_pemasukkan[k],
				borderColor: warna_pemasukkan[k],
				stack:'Pemasukkan',
				data: data
			});
		}
		var labels = [];
		for (var k = 0; k< data_chart['pengeluaran'].length;k++ ){
			labels.push(data_chart['pengeluaran'][k]['label']);
		}
		
		var barChartData = {
			labels: labels,
			datasets: datasets
		};
		
		//buat chartnya
			barChart.data= barChartData;
			//barChart.data
			barChart.update();
		
	}
	//---------------------------------------------Membuat line chart
	function updateLineChart(data_keluar, data_masuk){
		var data_k = [];
		var data_d = [];
		var labels = [];
		for (var k = 0; k< data_keluar.length;k++ ){
			data_k.push(data_keluar[k]['uang']);
			data_d.push(data_masuk[k]['uang']);
			labels.push(data_keluar[k]['label']);
		}
		var lineChartData = {
		labels  : labels,
		datasets: [
				{
				label               : 'Pemasukan',
				backgroundColor     : warna_bg_line[0],
				borderColor         : warna_line[0],
				pointColor          :  warna_bg_line[0],
				pointStrokeColor    : warna_line[0],
				pointHighlightFill  : '#fff',
				pointHighlightStroke: warna_bg_line[0],
				fill 				: false,
				lineTension			: 0	,
				data                : data_d
				},
				{
				label               : 'Pengeluaran',
				backgroundColor     : warna_bg_line[1],
				borderColor         : warna_line[1],
				pointColor          : warna_bg_line[1],
				pointStrokeColor    : warna_line[1],
				pointHighlightFill  : '#fff',
				pointHighlightStroke: warna_bg_line[1],
				fill 				: false,
				lineTension			: 0,
				data                : data_k
				},
			]
		}
		
		
			lineChart.data= lineChartData;
			lineChart.update();
	}
	//---------------------------------------------Membuat donut chart pemasukkan
	function updateDonutPemasukkanChart(data_chart){
		var data = [];
		for (var k = 0; k< data_chart['label_debit'].length;k++ ){
			var jumlah = 0;
			for (var l = 0; l< data_chart[data_chart['label_debit'][k]].length;l++ ){
				jumlah += data_chart[data_chart['label_debit'][k]][l]['uang'];
			}
			data.push(jumlah);
		}
		
		var donutChartCanvas = $('#donutChart.Pemasukkan').get(0).getContext('2d')
		var donutData        = {
		labels: data_chart['label_debit'],
		datasets: [
			{
			data: data,
			backgroundColor : warna_bg_pemasukkan,
			borderColor : warna_pemasukkan
			}
		]
		} 
		donutChartPemasukkan.data= donutData;
		donutChartPemasukkan.update();
	}
	//---------------------------------------------Membuat donut chart pengeluaran
	function updateDonutPengeluaranChart(data_chart){
		var data = [];
		for (var k = 0; k< data_chart['label_kredit'].length;k++ ){
			var jumlah = 0;
			for (var l = 0; l< data_chart[data_chart['label_kredit'][k]].length;l++ ){
				jumlah += data_chart[data_chart['label_kredit'][k]][l]['uang'];
			}
			data.push(jumlah);
		}
		
		donutChartCanvas = $('#donutChart.Pengeluaran').get(0).getContext('2d')
		donutData        = {
		labels: data_chart['label_kredit'],
		datasets: [
			{
				
			data: data,
			backgroundColor : warna_bg_pengeluaran,
			borderColor : warna_pemasukkan
			}
		]
		}
		donutChartPengeluaran.data= donutData;
		donutChartPengeluaran.update();
	}
	
	
	
	//eventHandler biar kelihatan keren
	setHari1();//biar awal-awal kelihatan bagus
	showCari();
	$('#ccari').change(showCari);
	$('#rhari').change(enableHari);
	$('#rbulan').change(enableBulan);
	$('#rtahun').change(enableTahun);
	$('select[name="sort_bulan"]').change(setHari1);
	$('select[name="sort_tahun"]').change(setHari1);
	$('#ccari').change(setHari2);
	$('select[name="sort_bulan2"]').change(setHari2);
	$('select[name="sort_tahun2"]').change(setHari2);
	
	//eventHandler----------------------------------------------------------------------------
	//eventHandler untuk laporan
	$('select[name="sort_hari"]').change(updateSemuaChart);
	$('select[name="sort_bulan"]').change(updateSemuaChart);
	$('select[name="sort_tahun"]').change(updateSemuaChart);
	$('select[name="sort_hari2"]').change(updateSemuaChart);
	$('select[name="sort_bulan2"]').change(updateSemuaChart);
	$('select[name="sort_tahun2"]').change(updateSemuaChart);
	
	$('#ccari').change(updateSemuaChart);
	$('#rhari').change(updateSemuaChart);
	$('#rbulan').change(updateSemuaChart);
	$('#rtahun').change(updateSemuaChart);
	
	//------------------------------------------------------------------------------------------ajax
	//mencari transaksi per tanggal
	function createSemuaChart(){
		var hari = getHari1();
		var bulan = getBulan1();
		var tahun = getTahun1();
		var harii = getHari2();
		var bulann = getBulan2();
		var tahunn = getTahun2();
		$.ajax({
			type  : 'POST',
			url   : '<?php echo site_url("LaporanKeuangan/perTanggal")?>',
			dataType : 'JSON',
			data : {hari1:hari,bulan1:bulan,tahun1:tahun,hari2:harii,bulan2:bulann,tahun2:tahunn},
			success : function(data){
				buatBarChart(data);
				buatLineChart(data['pengeluaran'],data['pemasukkan']);
				buatDonutPemasukkanChart(data);
				buatDonutPengeluaranChart(data);
				isiTitleLaporan(data['balance']);
			}

		});
	}
	function updateSemuaChart(){
		var hari = getHari1();
		var bulan = getBulan1();
		var tahun = getTahun1();
		var harii = getHari2();
		var bulann = getBulan2();
		var tahunn = getTahun2();
		$.ajax({
			type  : 'POST',
			url   : '<?php echo site_url("LaporanKeuangan/perTanggal")?>',
			dataType : 'JSON',
			data : {hari1:hari,bulan1:bulan,tahun1:tahun,hari2:harii,bulan2:bulann,tahun2:tahunn},
			success : function(data){
				updateBarChart(data);
				updateLineChart(data['pengeluaran'],data['pemasukkan']);
				updateDonutPemasukkanChart(data);
				updateDonutPengeluaranChart(data);
				isiTitleLaporan(data['balance']);
			}

		});
	}
	
	
	//function tampilan combobox dan radiobutton----------------------------------------------------------
	//hanya tampilan saja memakai javascript
	// get combobox hari pertama
	function showCari(){
		if ($('#ccari').is(":checked")){
			$('#hideme').show();
		}
		else{
			$('#hideme').hide();
		}
	}
	function enableHari(){
		if ($('#rhari').is(":checked")){
			$('select[name="sort_hari"]').prop('disabled', false);
			$('select[name="sort_bulan"]').prop('disabled', false);
			$('select[name="sort_hari2"]').prop('disabled', false);
			$('select[name="sort_bulan2"]').prop('disabled', false);
			//kalau mau tambah range
		}
	}
	function enableBulan(){
		if ($('#rbulan').is(":checked")){
			$('select[name="sort_hari"]').prop('disabled', true);
			$('select[name="sort_bulan"]').prop('disabled', false);
			$('select[name="sort_hari2"]').prop('disabled', true);
			$('select[name="sort_bulan2"]').prop('disabled', false);
			//kalau mau tambah range
		}
	}
	function enableTahun(){
		if ($('#rtahun').is(":checked")){
			$('select[name="sort_hari"]').prop('disabled', true);
			$('select[name="sort_bulan"]').prop('disabled', true);
			$('select[name="sort_hari2"]').prop('disabled', true);
			$('select[name="sort_bulan2"]').prop('disabled', true);
			//kalau mau tambah range
		}
	}
	//------------------------------------------------------------------- Untuk Tanggal Pertama
	function getHari1(){
		if (!$('#sort_hari').is(":disabled"))
		{
			return $('#sort_hari').val();
		}
		else{
			return "0";
		}
	}
	// set combobox hari pertama
	function setHari1(){
		var hari = cariHari(getBulan1(),getTahun1());
		var tanggal = Number(getHari1());
		
		var option = '';
		for (var i=0;i<hari.length;i++){
			if(hari[i]==tanggal){
				option += '<option value="'+ hari[i] + '"selected>' + hari[i] + '</option>';
			}
			else{
				option += '<option value="'+ hari[i] + '">' + hari[i] + '</option>';
			}
		}
		$('#sort_hari').empty();
		$('#sort_hari').append(option);
	}
	// get combobox bulan pertama
	function getBulan1(){
		if (!$('#sort_bulan').is(":disabled"))
		{
			return $('#sort_bulan').val();
		}
		else{
			return "0";
		}
	}
	// get combobox tahun pertama
	function getTahun1(){
		if(!$('#sort_tahun').is(":disabled")){
			return $('#sort_tahun').val();
		}
		else{
			return "0";
		}
	}
	
	//------------------------------------------------------------------- Untuk Tanggal Kedua
	function getHari2(){
		if (!$('#sort_hari').is(":disabled") && $('#ccari').is(":checked"))
		{
			return $('#sort_hari2').val();
		}
		else{
			return "0";
		}
	}
	// set combobox hari pertama
	function setHari2(){
		var hari = cariHari(getBulan2(),getTahun2());
		var tanggal = Number(getHari2());
		
		var option = '';
		for (var i=0;i<hari.length;i++){
			if(hari[i]==tanggal){
				option += '<option value="'+ hari[i] + '"selected>' + hari[i] + '</option>';
			}
			else{
				option += '<option value="'+ hari[i] + '">' + hari[i] + '</option>';
			}
		}
		$('#sort_hari2').empty();
		$('#sort_hari2').append(option);
	}
	// get combobox bulan kedua
	function getBulan2(){
		if (!$('#sort_bulan2').is(":disabled") && $('#ccari').is(":checked") )
		{
			return $('#sort_bulan2').val();
		}
		else{
			return "0";
		}
	}
	// get combobox tahun kedua
	function getTahun2(){
		if(!$('#sort_tahun2').is(":disabled") && $('#ccari').is(":checked")){
			return $('#sort_tahun2').val();
		}
		else{
			return "0";
		}
	}
	//mencari jumlah hari dalam sebulan
	//bisa dipakai untuk labeling chartData
	//diubah biar kedua range bisa pakai ini
	function cariHari(bulan, tahun){
		bulan = Number(bulan);
		tahun = Number(tahun);
		var hari31 = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"];
		var hari30 = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30"];
		var hari29 = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29"];
		var hari28 = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28"];
		var hari = [];
		if( bulan == 2 ){
			if (tahun % 4 == 0){
				hari = hari29;
			}
			else{
				hari = hari28;
			}
		}
		else if (bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11){
			hari = hari30;
		}
		else{
			hari = hari31;
		}
		return hari;
	}
	
	//kalau range bisa copy paste
  });
  
  
</script>