
    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="<?=base_url('js/jquery/jquery-2.2.4.min.js')?>"></script>
    <!-- Popper js -->
    <script src="<?=base_url('js/bootstrap/popper.min.js')?>"></script>
    <!-- Bootstrap js -->
    <script src="<?=base_url('js/bootstrap/bootstrap.min.js')?>"></script>
    <!-- All Plugins js -->
    <script src="<?=base_url('js/plugins/plugins.js')?>"></script>
    <!-- Active js -->
    <script src="<?=base_url('js/active.js')?>"></script>