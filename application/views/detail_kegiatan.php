<?php $this->load->view('header',['title'=>$data[0]->NAMA_KEGIATAN]);?>
<link rel="stylesheet" href="<?=base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>">
    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?=base_url('beranda')?>">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">...</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->
    <!-- ##### About Area Start ##### -->
	<section class="about-area section-padding-100-0">
        <div class="container">
            <div class="row about-content justify-content-center">
                <!-- Single About Content -->
                <div class="col-12" style="text-align:center">
                    <div class="about-us-content mb-100">
                        <div class='about-text' style="border-bottom:1px solid silver;">
                            <p style="font-size:16pt"><?=$data[0]->NAMA_KEGIATAN;?></p>
                        </div>
                        <div class='about-text' style="float:left;text-align:right; width:52%;">
                            <br>
                            <?php if($data[0]->NO_IC != "") {?>
                                <p>
                                    <b> Nama IC : </b> 
                                </p>
                            <?php } ?>
                            <p>
                                <b> Pembicara Yang Bertugas : </b> 
                            </p>
                            <p>
                                <b> Lokasi Acara Diadakan : </b> 
                            </p>
                            <p>
                                <b> Tanggal Acara berlangsung : </b> 
                            </p>
                            <p>
                                <b> Jam Acara dimulai : </b> 
                            </p>
                        </div>
                        <div class='about-text' style="float:right;text-align:left;width:47%">
                            <br>
                            <?php if($data[0]->NO_IC != "") {?>
                                <p>
                                    <?php echo $data[0]->NAMA_IC;?> 
                                </p>
                            <?php } ?>
                            <p> 
                                <?=$data[0]->NAMA_PEMBICARA;?> 
                            </p>
                            <p> 
                                <?=$data[0]->LOKASI_KEGIATAN;?>
                            </p>
                            <p> 
                                <?=$data[0]->TANGGAL_KEGIATAN;?> 
                            </p>
                            <p> 
                                <?=$data[0]->WAKTU_KEGIATAN;?> 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row about-content justify-content-center">
				
                    &nbsp;
			</div>
			<div class="row about-content justify-content-center">
                <div class='col-12'>
                    <div class='about-text' style="border-bottom:1px solid silver;">
                        <p style="text-align:center;font-size:16pt">List Petugas Pelayanan</p>
                    </div>
                    <br>
                    <div class="table-responsive mb-100">
                        <?php
                            $this->table->set_template([
                                'table_open'=>'<table class="table table-bordered table-strip">'
                            ]);
							$arrayheader = [0 => 'Tidak ada'];
							$arraycount = [0 => 0];
							$arraydata = [];
							for($k = 0; $k < count($kategori);$k++){
								$arrayheader[$k] = $kategori[$k]->NAMA_PELAYANAN;
								$arraydata[$arrayheader[$k]] = [];
								for($l = 0; $l < count($pelayan);$l++){
									if($pelayan[$l]->NAMA_PELAYANAN == $arrayheader[$k]){
										array_push($arraydata[$arrayheader[$k]],$pelayan[$l]->NAMA_JEMAAT);
									}
								}
								$arraycount[$k] = count($arraydata[$arrayheader[$k]]);
							}
							$this->table->set_heading($arrayheader);
							
							for ($k = 0; $k < max($arraycount);$k++){
								$arrayinsert = [];
								for($l = 0; $l < count($arrayheader);$l++){
									if($k < count($arraydata[$arrayheader[$l]])){
									$arrayinsert[$l] = $arraydata[$arrayheader[$l]][$k];
									}
									else{
										$arrayinsert[$l] = '';
									}
								}
								$this->table->add_row($arrayinsert);
							}
                            echo $this->table->generate();
                        ?>
                    </div>
                </div>
    		</div>
		</div>
    </section>    
	<!-- ##### About Area End ##### -->
<?php $this->load->view('footer');