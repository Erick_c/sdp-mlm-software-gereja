<?php $this->load->view('header',['title'=>'Acara Khusus']);?>
    <!-- ##### Breadcrumb Area Start ##### -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!--<nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?=base_url('beranda')?>">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="<?=base_url('beranda/acara-khusus')?>">Acara Khusus</a></li>
                        </ol>
                    </nav>-->
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Events Area Start ##### -->
    <div class="events-area section-padding-100">
        <div class="container">
            <div class="row">

                <!-- Event Search Form -->
                <div class="col-12">
                    <div class="event-search-form mb-50">
                        <form action="#" method="get">
                            <div class="row align-items-end">
                                <div class="col-12 col-md">
                                    <div class="form-group mb-0">
                                        <label for="eventDate">Tanggal Acara</label>
                                        <input type="date" name="tanggal_acara" value="<?=$get['tanggal_acara']?>" class="form-control" id="eventDate" placeholder="Tanggal Acara">
                                    </div>
                                </div>
                                <div class="col-12 col-md">
                                    <div class="form-group mb-0">
                                        <label for="eventLocation">Lokasi Acara</label>
                                        <input type="text" name="lokasi_acara" value="<?=$get['lokasi_acara']?>" class="form-control" id="eventLocation" placeholder="Lokasi Acara">
                                    </div>
                                </div>
                                <div class="col-12 col-md">
                                    <div class="form-group mb-0">
                                        <label for="eventKeyword">Kata Yang Terkait</label>
                                        <input type="text" name="kata_yang_terkait" value="<?=$get['kata_yang_terkait']?>" class="form-control" id="eventKeyword" placeholder="Masukkan kata-kata terkait">
                                    </div>
                                </div>
                                <div class="col-12 col-md-3 col-lg-2">
                                    <button type="submit" class="btn crose-btn">Cari Acara</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <!-- Events Title -->
                <div class="col-12">
                    <div class="events-title">
                        <h2>Semua Acara</h2>
                    </div>
                </div>
				<?php 
				if (count($data) > 0){
				
					for($k = 0; $k < count($data);$k++){
					
				?>
				
                <div class="col-12">
                    <!-- Single Upcoming Events Area -->
                    <div class="single-upcoming-events-area d-flex flex-wrap align-items-center">
                        <!-- Thumbnail -->
                        <div class="upcoming-events-thumbnail">
							<!--<img src="<?=base_url('img/acara-img/'.$data[$k]->ID_KEGIATAN.'.jpg')?>" alt="">-->
                            <img src="<?=base_url('img/core-img/logo.png')?>" style="height:200px; min-height:200px;width:200px; min-width:200px;" alt="">
                        </div>
                        <!-- Content -->
                        <div class="upcoming-events-content d-flex flex-wrap align-items-center">
                            <div class="events-text">
                                <h4><?= $data[$k]->NAMA_KEGIATAN;?></h4>
                                <div class="events-meta">
                                    <a href="#"><i class="fa fa-calendar" aria-hidden="true"></i><?= $data[$k]->TANGGAL_KEGIATAN;?></a><br>
                                    <a href="#"><i class="fa fa-clock-o" aria-hidden="true"></i><?= $data[$k]->WAKTU_KEGIATAN;?></a><br>
                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i><?= $data[$k]->LOKASI_KEGIATAN;?></a><br>
                                </div>
                            </div>
                            <div class="find-out-more-btn">
								<!--<a href="<?=base_url('beranda/acara-khusus/detail/'. $data[$k]->ID_KEGIATAN)?>" class="btn crose-btn btn-2">Detail Acara</a>-->
                                <a href="<?=base_url('beranda/detail/'. $data[$k]->ID_KEGIATAN)?>" class="btn crose-btn btn-2">Detail Acara</a>
                            </div>
                        </div>
                    </div>
                </div>

                <?php 
					}
				
				}
				else{
				?>
				<div class="col-12">
                    <!-- Single Upcoming Events Area -->
                    <div class="single-upcoming-events-area align-items-center">
                        <div style="width:100%;text-align:center;padding:150px;">
                            Tidak ada acara
                        </div>
                    </div>
                </div>
				<?php } ?>
                <div class="col-12">
                    <div class="pagination-area mt-70">
                        <nav aria-label="Page navigation example">
                            <!--<ul class="pagination justify-content-center">
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#"><i class="fa fa-angle-right"></i></a></li>
                            </ul>-->
							<?= $this->pagination->create_links();?>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Events Area End ##### -->
<?php $this->load->view('footer');?>