<?php $this->load->view('header_admin',['title'=>'Master Super Admin']);?>
<?php $this->load->view('sidebar_su');?>
<link rel="stylesheet" href="<?=base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Semua Data</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Beranda</a></li>
              <li class="breadcrumb-item active">Semua Data</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->    
	<section class="content">
      <!-- Table Cabang content -->    
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Cabang</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
				<div class="table-responsive">
				  <table id="" class="without_search table table-bordered table-strip">
					<thead>
					<tr>
					  <th>ID Cabang</th>
					  <th>Nama Cabang</th>
					  <th>Alamat Cabang</th>
					  <th>Email Cabang</th>
					  <th>No Telepon</th>
					</tr>
					</thead>
					<tbody>
					<tr>
					  <td>C0001</td>
					  <td>GBT Soekarno</td>
					  <td>Jln. Soekarno no.5 Selatan</td>
					  <td>info.gbt_soekarno@gmail.com</td>
					  <td>085658081071</td>
					</tr>
					<tr>
					  <td>C0002</td>
					  <td>GBT Sudirman</td>
					  <td> Jln. Jenderal Sudirman no.6 Selatan</td>
					  <td>info.gbt_sudirman@gmail.com</td>
					  <td>085658081071</td>
					</tr>
					</tbody>
					<tfoot>
					<tr>
					  <th>ID Cabang</th>
					  <th>Nama Cabang</th>
					  <th>Alamat Cabang</th>
					  <th>Email Cabang</th>
					  <th>No Telepon</th>
					</tr>
					</tfoot>
				  </table>
				</div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </div><!-- /.container-fluid -->
	  </div>
	  <!-- /.Table Cabang content -->    
      <!-- Table Jemaat content -->    
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Jemaat</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
			  <div class="table-responsive">
				  <table id="" class="with_search table table-bordered table-striped">
					<thead>
					<tr>
					  <th>ID Jemaat</th>
					  <th>Nama Jemaat</th>
					  <th>Jenis Kelamin</th>
					  <th>Tanggal Lahir</th>
					  <th>Alamat Jemaat</th>
					  <th>Babtis</th>
					</tr>
					</thead>
					<tbody>
					<tr>
					  <td>C0001</td>
					  <td>Erick Christiansen</td>
					  <td>Laki-Laki</td>
					  <td>Jln. Soekarno no.5 Selatan</td>
					  <td>12 November 1999</td>
					  <td> Sudah Babtis </td>
					</tr>
					<tr>
					  <td>C0002</td>
					  <td>Mardhan Krisnabayu</td>
					  <td>Laki-Laki</td>
					  <td>Jln. Sudirman no.5 Selatan</td>
					  <td>12 Jaunari 1998</td>
					  <td> Sudah Babtis </td>
					</tr>
					<tr>
					  <td>C0003</td>
					  <td>Timotius Kevin</td>
					  <td>Laki-Laki</td>
					  <td>Jln. Boogieman no.5 Selatan</td>
					  <td>14 November 2000</td>
					  <td> Belum Babtis </td>
					</tr>
					<tr>
					  <td>C0004</td>
					  <td>Samuel Suhan Putra</td>
					  <td>Laki-Laki</td>
					  <td>Jln. Maya no.5 Selatan</td>
					  <td>10 November 1997</td>
					  <td> Sudah Babtis </td>
					</tr>
					<tr>
					  <td>C0005</td>
					  <td>Dodi Krisnawan</td>
					  <td>Laki-Laki</td>
					  <td>Jln. Maya no.6 Selatan</td>
					  <td>12 November 2000</td>
					  <td> Sudah Babtis </td>
					</tr>
					</tbody>
					<tfoot>
					<tr>
					  <th>ID Jemaat</th>
					  <th>Nama Jemaat</th>
					  <th>Jenis Kelamin</th>
					  <th>Tanggal Lahir</th>
					  <th>Alamat Jemaat</th>
					  <th>Babtis</th>
					</tr>
					</tfoot>
				  </table>
				</div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </div><!-- /.container-fluid -->
	  </div>
	  <!-- /.Table Jemaat content -->    
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>