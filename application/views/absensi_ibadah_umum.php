<?php $this->load->view('header_admin',['title'=>'absensi umum']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'absensi umum']);?>
<style>
	#invisible{
		position:absolute;
		opacity : 0;
	}
	#text {
		border:0px;
		background:none;
		text-transform:uppercase;
	}
</style>
<script>
	window.onkeydown = function(){
		document.getElementById("text").focus();
	}
</script>
<div class="content-wrapper" style="min-height: 1200.88px;">
	<form method="post" id="invisible" action="">
		<input id="text" name="id_jemaat" autocomplete="off">
	</form>
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <div class="container-fluid">
		<div class="row mb-2">
		  <div class="col-sm-6">
			<h1>Absensi Ibadah Umum</h1>
		  </div>
		  <div class="col-sm-6">
			<ol class="breadcrumb float-sm-right">
			  <li class="breadcrumb-item"><a href="#">Beranda</a></li>
			  <li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
			  <li class="breadcrumb-item active">Absensi..</li>
			</ol>
		  </div>
		</div>
	  </div><!-- /.container-fluid -->
	</section>
	<!-- /.content-header -->

	<section class="content">
		<?php if (isset($data) == false) { ?>
			<!-- Profile Image -->
			<div class="card card-primary card-outline">
			  <div class="card-body box-profile">
				<div class="text-center" style="padding:25px;">
					  <div class="profile-user-img img-fluid img-circle"
						   style='background-image:url("<?=base_url('img/icons/Profile.png')?>");height:100px;width:100px;background-size:auto 100%;background-position:center;'
						>
					  </div><br>
					<h4> Silahkan Masukkan inputan lewat Scanner !! </h4> 
				</div>
			  </div>
			  <!-- /.card-body -->
			</div>
			<!-- /.card -->
		<?php }else if(count($data) > 0){?>
			<!-- Profile Image -->
			<div class="card card-primary card-outline">
			  <div class="card-body box-profile">
				<div class="text-center">
				  <h5>Selamat Datang</h5>
				  <div class="profile-user-img img-fluid img-circle"
					   style='background-image:url("data:image/jpeg;base64,<?=base64_encode($data[0]->FOTO)?>");height :100px;width:100px;background-size:auto 100%;background-position:center;'
					>
				  </div>
				</div>

				<h3 class="profile-username text-center"><?=$data[0]->NAMA_JEMAAT?></h3>

				<p class="text-muted text-center"><?=$data[0]->ID_JEMAAT?></p>
			  </div>
			  <!-- /.card-body -->
			</div>
			<!-- /.card -->
		<?php } else { ?>
			<!-- Profile Image -->
			<div class="card card-primary card-outline">
			  <div class="card-body box-profile">
				<div class="text-center" style="padding:25px;">				
					  <div class="profile-user-img img-fluid img-circle"
						   style='background-image:url("<?=base_url('img/icons/Warning.png')?>");height:100px;width:100px;background-size:auto 100%;background-position:center;'
						>
					  </div><br>
					<h4> Masukkan inputan tidak ditemukkan !! </h4> 
				</div>
			  </div>
			  <!-- /.card-body -->
			</div>
			<!-- /.card -->
		<?php } ?>
	</section>
</div>

<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>