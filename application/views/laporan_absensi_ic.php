<?php $this->load->view('header_admin',['title'=>'Laporan Absensi IC']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'list absensi ic']);?>
<link rel="stylesheet" href="<?=base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>">
<div class="content-wrapper" style="min-height: 976.13px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Laporan Absensi</h1>
			<sup>Absensi IC sesuai range tanggal yang di tentukan </sup>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Beranda</a></li>
              <li class="breadcrumb-item"><a href="#">Super Admin</a></li>
              <li class="breadcrumb-item"><a href="#">Absensi IC</a></li>
              <li class="breadcrumb-item active">...</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
		<!--<div class="col-md-12">-->
			<!-- LINE CHART --><!--
			<div class="card card-info">
				<div class="card-header">
					<h3 class="card-title">Line Chart</h3>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
						<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
					</div>
				</div>
				<div class="card-body">
					<div class="chart">
						<canvas id="lineChart" style="height:250px; min-height:250px"></canvas>
					</div>
				</div>-->
			<!-- /.card-body --><!--
			</div>-->
		<!-- /.card -->
		<!--</div>-->
		<div class="col-12">
			 <!-- DONUT CHART -->
			 <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Kegiatan <?php if ($data_chart[0]->NAMA_KEGIATAN==null) echo 'Ibadah IC'; else echo $data_chart[0]->NAMA_KEGIATAN;?></h3>

                <div class="card-tools">
                  <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button> -->
                </div>
              </div>
              <div class="card-body">
                <canvas id="donutChart" style="height:230px; min-height:230px"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
		</div><!-- /.container-fluid -->
		<div class="col-12">
			<!-- /.card -->
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Jemaat yang hadir</h3>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<style>
						table.table-responsive th{
							width:100%;
							min-width:200px;
						}

						table.table-responsive td
						{
							line-height:100px;
						}
					</style>
					<?php 
						$this->table->set_template([
							'table_open'=>'<table id="" class="with_search table table-bordered table-strip table-responsive">',
						]);

						//$tampilkan = [[['style'=>'width:100px;min-width:100px','data'=>'Foto'],'Nama','Jenis Kelamin','Alamat Jemaat',['style'=>'min-width:120px;']]];
						$tampilkan = [[['style'=>'width:255px;min-width:255px', 'data'=>'FOTO'], 'NAMA JEMAAT', ['style'=>'width:150px;min-width:150px']]];
						for($i=0;$i<count($data_hadir);$i++){
							$isi = [];
							$isi[] = '<div class="profile-user-img img-fluid img-circle"'.
							"style='".'background-image:url("data:image/jpeg;base64,'.base64_encode($data_hadir[$i]->FOTO).'"'.");height :100px;width:100px;background-size:auto 100%;background-position:center;'></div>";							
							$isi[] = $data_hadir[$i]->NAMA_JEMAAT;
							$isi[] = '<center><a href="./delete/'.$data_hadir[$i]->ID_JEMAAT.'/?id_kegiatan='.$data_hadir[$i]->ID_KEGIATAN.'" class="btn btn-primary">Tidak Hadir</a></center>';
							$tampilkan[]=$isi;
						}
						echo $this->table->generate($tampilkan);
					?>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div><!-- /.container-fluid -->
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Jemaat yang tidak hadir</h3>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<style>
						table.table-responsive th{
							width:100%;
							min-width:200px;
						}

						table.table-responsive td
						{
							line-height:100px;
						}
					</style>
					<?php 
						$this->table->set_template([
							'table_open'=>'<table id="" class="with_search table table-bordered table-strip table-responsive">',
						]);

						//$tampilkan = [[['style'=>'width:100px;min-width:100px','data'=>'Foto'],'Nama','Jenis Kelamin','Alamat Jemaat',['style'=>'min-width:120px;']]];
						$tampilkan = [[['style'=>'width:255px;min-width:255px', 'data'=>'FOTO'], 'NAMA JEMAAT', ['style'=>'width:150px;min-width:150px']]];
						for($i=0;$i<count($data_tidak_hadir);$i++){
							$isi = [];
							$isi[] = '<div class="profile-user-img img-fluid img-circle"'.
							"style='".'background-image:url("data:image/jpeg;base64,'.base64_encode($data_tidak_hadir[$i]->FOTO).'"'.");height :100px;width:100px;background-size:auto 100%;background-position:center;'></div>";							
							$isi[] = $data_tidak_hadir[$i]->NAMA_JEMAAT;
							$isi[] = '<center><a href="./insert/'.$data_tidak_hadir[$i]->ID_JEMAAT.'/?id_kegiatan='.$data_tidak_hadir[$i]->ID_KEGIATAN.'" class="btn btn-primary">Hadir ke Acara</a></center>';
							$tampilkan[]=$isi;
						}
						echo $this->table->generate($tampilkan);
					?>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.card -->
		</div><!-- /.container-fluid -->
		<br>
    </section>
    <!-- /.content -->
</div>

<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('plugins/chart.js/Chart.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": true,
    });
  });
</script>

<script>
	$(function () {
			
		//-------------
		//- DONUT CHART -
		//-------------
		// Get context with jQuery - using jQuery's .get() method.
		var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
		var donutData        = {
		labels: [
			'Yang hadir', 
			'Yang tidak hadir'
		],
		datasets: [
			{
			data: [<?php
					echo $data_chart[0]->JUMLAH_JEMAAT_HADIR.',';
					echo $data_chart[0]->JUMLAH_JEMAAT_TIDAK_HADIR.',';
				?>],
			backgroundColor : ['#00a65a', '#f56954', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
			}
		]
		}
		var donutOptions     = {
		maintainAspectRatio : false,
		responsive : true,
		}
		//Create pie or douhnut chart
		// You can switch between pie and douhnut using the method below.
		var donutChart = new Chart(donutChartCanvas, {
		type: 'doughnut',
		data: donutData,
		options: donutOptions      
		})
	})
</script>