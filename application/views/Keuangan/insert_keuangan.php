<?php $this->load->view('header_admin',['title'=>'Transaksi Baru']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'Transaksi Baru']);?>
<link rel="stylesheet" href="<?=base_url('plugins/select2/css/select2.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')?>">
<style>
	span.select2-selection.select2-selection--single
	{
		height:initial;
	}
</style>
<div class="content-wrapper" style="min-height: 1200.88px;">
    <section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Transaksi Baru</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?=base_url('/')?>">Beranda</a></li>
						<li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
						<li class="breadcrumb-item active">Transaksi Baru</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<form action="" method="post" role="form">
							<div class="card-body">
								<div class="form-group">
									<label for="">
                                        Jenis Transaksi
                                    </label>
									<?php
										$tampilan = array('D'=>'Debit','K'=>'Kredit');
										echo form_dropdown('jenis_transaksi',$tampilan,'D',["class"=>"form-control select2","id"=>"jenis_transaksi","required"=>"required"]);
									?>
								</div>
								<div class="form-group">
									<label for="">
										Kategori
                                    </label>	<!-- mungkin menampilkan kategori berdasarkan kredit atau debit-->								
									<?php
										$tampilan = array_merge($data_debit,$data_kredit);
										echo form_dropdown('kategori',$tampilan,'',["class"=>"form-control select2","id"=>"selectKategori","required"=>"required"]);
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Nominal Uang
                                    </label>
									<input type="number" name="nominal_uang" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="">
										Jemaat Penanggung Jawab
                                    </label>									
									<?php
										$tampilan = [];
										for($i=0; $i<count($data_jemaat); $i++){
											$tampilan[$data_jemaat[$i]->ID_JEMAAT]=$data_jemaat[$i]->NAMA_JEMAAT;
										}
										echo form_dropdown('id_penanggung_jawab',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Maintenance (*)<!-- mungkin ada checklist untuk membuka menu tambahan ini -->
                                    </label><!--mungkin kalau menyediakan id maintenance, id barang langsung ada-->
									<?php
										$tampilan = [''=>'Tidak Ada'];
										for($i=0; $i<count($data_maintenance); $i++){
											$tampilan[$data_maintenance[$i]->ID_MAINTENANCE]="Maintenance ".$data_maintenance[$i]->NAMA_ATAU_JENIS_BARANG;
											//mungkin jadi "Maintenance <NAMA_ATAU_JENIS_BARANG>"
										}
										echo form_dropdown('id_maintenance',$tampilan,'',["class"=>"form-control select2"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Nama Barang (*) <!-- mungkin ada checklist untuk membuka menu tambahan ini -->
                                    </label> <!-- maintenance perlu barang, tapi barang mungkin tidak perlu maintenance -->
									<?php
										$tampilan = [''=>'Tidak Ada'];
										for($i=0; $i<count($data_barang); $i++){
											$tampilan[$data_barang[$i]->KODE_BARANG]=$data_barang[$i]->NAMA_ATAU_JENIS_BARANG;
										}
										echo form_dropdown('kode_barang',$tampilan,'',["class"=>"form-control select2"])
									?>
								</div>
								<div class="form-group">
									<label for="">
										Cabang (*)
                                    </label>									
									<?php
										$tampilan = [''=>'Tidak Ada'];
										for($i=0; $i<count($data_cabang); $i++){
											$tampilan[$data_cabang[$i]->ID_CABANG]=$data_cabang[$i]->NAMA_CABANG;
										}
										echo form_dropdown('id_cabang',$tampilan,'',["class"=>"form-control select2"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Deskripsi Transaksi
                                    </label>
									<input type="textarea" name="deskripsi" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="">
                                        Tanggal Transaksi
                                    </label>
									<input type="date" name="tanggal" class="form-control" required>
								</div>
								
							</div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-keuangan')?>" class="btn btn-secondary"> Batal </a>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
<?=$pesan?>
<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<script src="<?=base_url('plugins/select2/js/select2.full.min.js')?>"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
	/*
	if($(".pesan")){
	 $(document).Toasts('create', {
        title: 'Berhasil!',
        autohide: true,
        delay: 1200,
		width: 400,
		height: 200,
        body: 'Data telah tersimpan di database.'
      });
	}
	/** */
    //Initialize Select2 Elements
    $('.select2').select2()
	
	//untuk kategori keuangan
	ubah();
	
	//fungsi merubah select kategori keuangan
	function ubah(){
		var debt = <?= json_encode($data_debit); ?>;
		var kred = <?= json_encode($data_kredit); ?>;
		var jenis = $('#jenis_transaksi').val();
		var selected = $('#selectKategori option:selected').text();
		var kategori = [];
		if(jenis == 'K'){
			kategori = kred;
		}
		else if (jenis == 'D'){
			kategori = debt;
		}
		var option = '';
		for (var i=0;i<kategori.length;i++){
			if(kategori[i]==selected){
				option += '<option value="'+ kategori[i] + '" selected>' + kategori[i] + '</option>';
			}
			else{
				option += '<option value="'+ kategori[i] + '">' + kategori[i] + '</option>';
			}
		}
		$('#selectKategori').empty();
		$('#selectKategori').append(option);
	}
	$('#jenis_transaksi').change(ubah);
  })
  
</script>