<?php $this->load->view('header',['title'=>'Cabang']);?>
	<!-- ##### Breadcrumb Area Start ##### -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?=base_url('beranda')?>">Beranda</a></li>
                            <li class="breadcrumb-item"><a href="<?=base_url('beranda/kegiatan-rutin')?>">Kegiatan Rutin</a></li>
                            <li class="breadcrumb-item"><a href="<?=base_url('beranda/kegiatan-rutin/cabang')?>">Cabang</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->
	<!-- ##### Cabang Area Start ##### -->
    <section class="about-area section-padding-100-0">
        <div class="container">
			<div class="row about-content justify-content-center" style="text-align:center">
				<!-- Single Cabang Content -->
				<div class="col-12 col-md-6 col-lg-4">
                    <div class="about-us-content mb-100">
                        <div style="font-size:30pt"> <i class="fa fa-map-marker" aria-hidden="true"></i> </div>
						<div class="about-text">
							<br>
                            <h4>GBT. Jendral Sudirman</h4>
                            <p>
								<a href="callto:001-1234-88888"><i class="fa fa-phone" aria-hidden="true"></i> 001-1234-88888</a> <br>
								<a href="mailto:info.deercreative@gmail.com"><i class="fa fa-envelope" aria-hidden="true"></i> info.deercreative@gmail.com</a>
							</p>
                            <a href="<?=base_url('beranda/kegiatan-rutin/cabang/gbt-jenderal-sudirman')?>">Info Lebih Lanjut <i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
				<!-- Single Cabang Content -->
				<div class="col-12 col-md-6 col-lg-4">
                    <div class="about-us-content mb-100">
                        <div style="font-size:30pt"> <i class="fa fa-map-marker" aria-hidden="true"></i> </div>
						<div class="about-text">
							<br>
                            <h4>GBT. Ir. Soekarno</h4>
                            <p>
								<a href="callto:001-1234-88888"><i class="fa fa-phone" aria-hidden="true"></i> 001-1234-88888</a> <br>
								<a href="mailto:info.deercreative@gmail.com"><i class="fa fa-envelope" aria-hidden="true"></i> info.deercreative@gmail.com</a>
							</p>
                            <a href="<?=base_url('beranda/kegiatan-rutin/cabang/gbt-ir-soekarno')?>">Info Lebih Lanjut <i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
			</div>
        </div>
    </section>
	<!-- ##### Cabang Area End ##### -->
	<?php $this->load->view('footer');?>