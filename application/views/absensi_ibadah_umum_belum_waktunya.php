<?php $this->load->view('header_admin',['title'=>'absensi umum']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'absensi umum']);?>
<style>
	#invisible{
		position:absolute;
		opacity : 0;
	}
	#text {
		border:0px;
		background:none;
		text-transform:uppercase;
	}
</style>
<div class="content-wrapper" style="min-height: 1200.88px;">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <div class="container-fluid">
		<div class="row mb-2">
		  <div class="col-sm-6">
			<h1>Absensi Ibadah Umum</h1>
		  </div>
		  <div class="col-sm-6">
			<ol class="breadcrumb float-sm-right">
			  <li class="breadcrumb-item"><a href="#">Beranda</a></li>
			  <li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
			  <li class="breadcrumb-item active">Absensi..</li>
			</ol>
		  </div>
		</div>
	  </div><!-- /.container-fluid -->
	</section>
	<!-- /.content-header -->

	<section class="content">
		<!-- Profile Image -->
		<div class="card card-primary card-outline">
			<div class="card-body box-profile">
			<div class="text-center" style="padding:25px;">				
					<div class="profile-user-img img-fluid img-circle"
						style='background-image:url("<?=base_url('img/icons/Warning.png')?>");height:100px;width:100px;background-size:auto 100%;background-position:center;'
					>
					</div><br>
				<h4> Belum waktunya untuk Absensi Ibadah Umum </h4> 
			</div>
			</div>
			<!-- /.card-body -->
		</div>
		<!-- /.card -->
	</section>
</div>

<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>