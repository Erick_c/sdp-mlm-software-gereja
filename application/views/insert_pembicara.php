<?php $this->load->view('header_admin',['title'=>'pembicara baru']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'pembicara baru']);?>
<div class="content-wrapper" style="min-height: 1200.88px;">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pembicara Baru</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Beranda</a></li>
              <li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
              <li class="breadcrumb-item active">Pembicara Baru</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <form action="" method="post" role="form">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">
                                        Nama Pembicara
                                    </label>
                                    <input type="text" class="form-control" name="nama_pembicara" required=required>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        Alamat Pembicara
                                    </label>
                                    <input type="textarea" class="form-control" name="alamat_rumah" required=required>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        No Telp
                                    </label>
                                    <input type="number" class="form-control" name="no_telp" required=required>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pembicara')?>" class="btn btn-secondary"> Batal </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
</div>
<?php echo $pesan?>
<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>