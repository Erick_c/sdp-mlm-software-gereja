<?php $this->load->view('header_admin',['title'=>$_SESSION['NAMA_PELAYANAN']]);?>
<?php $this->load->view('sidebar_su')?>
<link rel="stylesheet" href="<?=base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Mengenai Saya</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Beranda</a></li>
              <li class="breadcrumb-item active">Profil Pengguna</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->    
	  <section class="content">
      <div class="row">
        <div class="col-md-3">
          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
            <div class="text-center">
              <div class="profile-user-img img-fluid img-circle" style='background-image:url("data:image/jpeg;base64,<?=base64_encode($_SESSION['FOTO'])?>");height :100px;width:100px;background-size:auto 100%;background-position:center;'></div>
            </div>

            <h3 class="profile-username text-center"><?=$_SESSION['NAMA_JEMAAT']?></h3>

            <p class="text-muted text-center"><?=$_SESSION['NAMA_PELAYANAN']?></p>
            <center><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">Ubah Password</button></center>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        
          <!-- About Me Box -->
          <div class="card card-primary">
            <div class="card-header">
            <h3 class="card-title">Biodata</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <strong><i class="fas fa-book mr-1"></i> Tanggal Lahir </strong>

            <p class="text-muted">
              <?=date('d F o',strtotime($_SESSION['TANGGAL_LAHIR']))?>
            </p>

            <hr>

            <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>

            <p class="text-muted"><?=$_SESSION['ALAMAT_JEMAAT']?></p>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card" style="min-height:50%;">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                <li class="nav-item"><a href="#Notifikasi" class="nav-link active">Notifikasi</a></li>
              </ul>
            </div><!-- /.card-header -->

            <div class="card-body">
              <div class="tab-content">
                <div id="Notifikasi">
                  <?php
                    if (count($data)===0)
                    {
                      echo '<!-- Post -->
                      <div class="post">
                        <p>
                        Tidak ada Notifikasi
                        </p>
                      </div>
                      <!-- /.post -->';
                    }
                    else
                    {
                      for ($i=0; $i<count($data);$i++)
                      {
                        $kegiatan=$data[$i]->NAMA_KATEGORI;
                        if ($data[$i]->NAMA_KEGIATAN!=null) $kegiatan = $data[$i]->NAMA_KEGIATAN;
                        
                        echo '
                          <div class="post">
                            <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="'.base_url('img/core-img/logo.png').'" alt="user image">
                            <span class="username">
                              <a href="#">Gereja Bethel Tabernakel</a>
                            </span>
                            <span class="description"> Mengingatkan anda </span>
                            </div>
                            
                            <p style="text-align:justify">
                            Halo, anda telah terdaftar pelayanan sebagai '.$data[$i]->NAMA_PELAYANAN.'. Pada acara '.$kegiatan.' di cabang '.$data[$i]->NAMA_CABANG.'. Pada tanggal '.date('d F o',strtotime($data[$i]->TANGGAL_KEGIATAN)).'. Karena anda bertugas untuk pelayanan mohon sebaiknya datang pada jam '.date('h:m',strtotime($data[$i]->WAKTU_KUMPUL)).' dikarenakan adanya persiapan yang harus dijalani.
                            <a href=""> Baca lebih lanjut <i class="fa fa-angle-double-right"></i></a>
                            </p>
                          </div>
                        ';
                      }
                    }
                  ?>
                </div>
              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
        
        <div class="modal fade" id="modal-overlay">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="overlay d-flex justify-content-center align-items-center">
                  <i class="fas fa-2x fa-sync fa-spin"></i>
              </div>
              <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p>One fine body&hellip;</p>
              </div>
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>
	  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>