<?php $this->load->view('header_admin',['title'=>$_SESSION['NAMA_PELAYANAN']]);?>
<?php $this->load->view('sidebar_su')?>
<?php date_default_timezone_set("Asia/Bangkok");?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Profile</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Beranda</a></li>
            <li class="breadcrumb-item active">Profil Pengguna</li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-3">

        <!-- Profile Image -->
        <div class="card card-primary card-outline">
            <div class="card-body box-profile">
            <div class="text-center">
            <div class="profile-user-img img-fluid img-circle" style='background-image:url(<?php if($_SESSION['FOTO']!='') echo "data:image/jpeg;base64,".base64_encode($_SESSION['FOTO']); else echo base_url('img/icons/Profile.png')?>);height :100px;width:100px;background-size:auto 100%;background-position:center;'></div>
            </div>

            <h3 class="profile-username text-center"><?=$_SESSION['NAMA_JEMAAT']?></h3>

            <p class="text-muted text-center"><?=$_SESSION['NAMA_PELAYANAN']?></p>

            <?php if(strtolower($_SESSION['NAMA_PELAYANAN'])!='admin absensi') {?><center><a class="btn btn-primary" href="#ubah_password" onclick="this.className='btn btn-primary'" id="btn_ubah_password" data-toggle="tab">Ubah Password</a></center><?php }?>
            <style>
                #btn_ubah_password:hover{
                    border-color:#0062cc;
                    background-color:#0062cc;
                }
                #btn_ubah_password{
                    border-color:#007bff;
                    background-color:#007bff;
                }
            </style>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

        <!-- About Me Box -->
        <div class="card card-primary">
            <div class="card-header">
            <h3 class="card-title">Biodata</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <strong><i class="fas fa-book mr-1"></i> Tanggal Lahir </strong>

            <p class="text-muted">
                <?php if($_SESSION['TANGGAL_LAHIR']!=null){ ?>
                    <?=date('d F o',strtotime($_SESSION['TANGGAL_LAHIR']))?>
                <?php } else echo 'Tidak ada'; ?>
            </p>

            <hr>

            <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>
            
            <p class="text-muted">
                <?=$_SESSION['ALAMAT_JEMAAT']?>
            </p>

            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
        <div class="card" style="min-height:50%;">
            <div class="card-header p-2">
            <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link <?=(isset($data_detail_pelayanan))?'':'active'?> " href="#notifikasi" data-toggle="tab">Notifikasi</a></li>
            </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
            <div class="tab-content">
                <div class="<?=(isset($data_detail_pelayanan))?'active':''?> tab-pane">
                  <?php 
                    for ($i=0; $i<count($data_detail_pelayanan);$i++){
                      $kegiatan=$data_detail_pelayanan[$i]->NAMA_KATEGORI;
                      if ($data_detail_pelayanan[$i]->NAMA_KEGIATAN!=null) $kegiatan = $data_detail_pelayanan[$i]->NAMA_KEGIATAN;
                      $list_acara = '';
                      if ($data_detail_pelayanan[$i]->LIST_ACARA!=null)
                        $list_acara = 'Berikut adalah list acara pada acara '.$kegiatan.' : '.$data_detail_pelayanan[$i]->LIST_ACARA;
                      $lagu_pujian = '';
                      if ($data_detail_pelayanan[$i]->LAGU_PUJIAN!=null)
                        $lagu_pujian = 'Berikut adalah lagu pujian yang akan di nyanyikan pada acara kebaktian '.$data_detail_pelayanan[$i]->NAMA_KEGIATAN.' : '.$data_detail_pelayanan[$i]->LAGU_PUJIAN;
                      echo '
                      <div class="post">
                        <div class="user-block">
                        <img class="img-circle img-bordered-sm" src="'.base_url('img/core-img/logo.png').'" alt="user image">
                        <span class="username">
                          Gereja Bethel Tabernakel
                        </span>
                        <span class="description"> Mengingatkan anda </span>
                        </div>
                        
                        <p style="text-align:justify">
                        Halo, anda telah terdaftar pelayanan sebagai '.$data_detail_pelayanan[$i]->NAMA_PELAYANAN.', pada acara '.$kegiatan.' di cabang '.$data_detail_pelayanan[$i]->NAMA_CABANG.'. Pada tanggal '.date('d F o',strtotime($data_detail_pelayanan[$i]->TANGGAL_KEGIATAN)).'. Karena anda bertugas untuk pelayanan mohon sebaiknya datang pada jam '.date('h:m',strtotime($data_detail_pelayanan[$i]->WAKTU_KUMPUL)).' dikarenakan adanya persiapan yang harus dijalani.
                        '.$list_acara.''.$lagu_pujian.'
                        </p>
                      </div>
                    ';
                    }
                  ?>
                </div>
                <div class="<?=(isset($data_detail_pelayanan))?'':'active'?> tab-pane" id="notifikasi">
                <?php
                    if (count($data)===0)
                    {
                      echo '<!-- Post -->
                      <div class="post">
                        <p>
                        Tidak ada Tugas Pelayanan
                        </p>
                      </div>
                      <!-- /.post -->';
                    }
                    else
                    {
                      for ($i=0; $i<count($data);$i++)
                      {
                        $kegiatan=$data[$i]->NAMA_KATEGORI;
                        if ($data[$i]->NAMA_KEGIATAN!=null) $kegiatan = $data[$i]->NAMA_KEGIATAN;
                        
                        echo '
                          <div class="post">
                            <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="'.base_url('img/core-img/logo.png').'" alt="user image">
                            <span class="username">
                              Gereja Bethel Tabernakel
                            </span>
                            <span class="description"> Mengingatkan anda </span>
                            </div>
                            
                            <p style="text-align:justify">
                            Halo, anda telah terdaftar pelayanan sebagai '.$data[$i]->NAMA_PELAYANAN.', pada acara '.$kegiatan.' di cabang '.$data[$i]->NAMA_CABANG.'. Pada tanggal '.date('d F o',strtotime($data[$i]->TANGGAL_KEGIATAN)).'. Karena anda bertugas untuk pelayanan mohon sebaiknya datang pada jam '.date('h:m',strtotime($data[$i]->WAKTU_KUMPUL)).' dikarenakan adanya persiapan yang harus dijalani.
                            <a href="'.base_url("beranda/pelayanan/beranda-admin/super-admin/profilpengguna/". $data[$i]->ID_KEGIATAN).'"> Baca lebih lanjut <i class="fa fa-angle-double-right"></i></a>
                            </p>
                          </div>
                        ';
                      }
                    }
                    ////////////////////////////maintenance////////////////////////////
                    if (count($data)===0)
                    {
                      echo '<!-- Post -->
                      <div class="post">
                        <p>
                        Tidak ada Tugas Maintenance
                        </p>
                      </div>
                      <!-- /.post -->';
                    }
                    else
                    {
                      for ($i=0; $i<count($data_maintenance);$i++)
                      {               
                         if ($data_maintenance[$i]->TANGGAL_MAINTENANCE>$data_maintenance[$i]->TERAKHIR_MAINTENANCE){
                         echo '
                          <div class="post">
                            <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="'.base_url('img/core-img/logo.png').'" alt="user image">
                            <span class="username">
                              Gereja Bethel Tabernakel
                            </span>
                            <span class="description"> Mengingatkan anda </span>
                            </div>
                            
                            <p style="text-align:justify">
                            Pengingat Maintenance untuk '.$data_maintenance[$i]->KETERANGAN.' '.$data_maintenance[$i]->NAMA_ATAU_JENIS_BARANG.' pada tanggal '.date('d F o',strtotime($data_maintenance[$i]->TANGGAL_MAINTENANCE)).'
                            </p>
                            <a href="'.base_url('beranda/pelayanan/beranda-admin/super-admin/mst-maintenance/update').'/'.$data_maintenance[$i]->ID_MAINTENANCE.'" class="btn btn-primary" >SUDAH MAINTENANCE</a>
                          </div>
                        ';
                        }
                      }
                    }
                  ?>
                </div>

                <div class="tab-pane" id="ubah_password">
                <form class="form-horizontal" method="post">
                    <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password">
                    </div>
                    </div>
                    <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                    </div>
                </form>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
            </div><!-- /.card-body -->
        </div>
        <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>

<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>