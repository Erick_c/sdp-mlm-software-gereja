<?php $this->load->view('header_admin',['title'=>'Hubungan Keluarga']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'list jemaat']);?>
<link rel="stylesheet" href="<?=base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2/css/select2.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')?>">
<style>
	span.select2-selection.select2-selection--single
	{
		height:initial;
	}
</style>
<div class="content-wrapper" style="min-height: 1200.88px;">
    <section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Hubungan Keluarga</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?=base_url('/')?>">Beranda</a></li>
						<li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
						<li class="breadcrumb-item active">Hubungan Keluarga</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
      <!-- Table Jemaat content -->    
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Hubungan Keluarga untuk Jemaat <b> <?=$data[0]->NAMA_JEMAAT; ?></b></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
				  <?php 
					$this->table->set_template([
						'table_open'=>'<table id="" class="without_search table table-bordered table-strip">',
					]);
					
					$tampilkan = [['Nama Jemaat','Status','Tanggal lahir','Tanggal pernikahan','Tempat pernikahan',['style'=>'min-width:50px']]];
					for ($i=0;$i<count($data_hubkel);$i++)
					{
						$tampilkan[] = [
							$data_hubkel[$i]->NAMA_JEMAAT,
							$data_hubkel[$i]->STATUS,
							$data_hubkel[$i]->TANGGAL_LAHIR,
							$data_hubkel[$i]->TANGGAL_PERNIKAHAN,
							$data_hubkel[$i]->TEMPAT_PERNIKAHAN,
							'<a href="'.base_url('beranda/pelayanan/beranda-admin/super-admin/mst-jemaat/deletehubkel').'/'.$data_hubkel[$i]->ID_JEMAAT.'/'.$data[0]->ID_JEMAAT.'" class="btn btn-primary" >HAPUS DATA</a>'
						];
					}

					echo $this->table->generate($tampilkan);
				  ?>
			  </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div><!-- /.container-fluid -->
      <!-- /.content -->
    </section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<form action="" role="form" method="post"enctype="multipart/form-data">
							<div class="card-body">								                                
								<div class="form-group">
									<label for="">
                                        Nama Jemaat Bersangkutan
                                    </label>
									<?php 
										$tampilan = [''=>'-- Pilih Jemaat Bersangkutan --'];
										for($i=0; $i<count($data_jemaat); $i++){
											$tampilan[$data_jemaat[$i]->ID_JEMAAT]=$data_jemaat[$i]->NAMA_JEMAAT;
										}
										echo form_dropdown('id_jemaat_bersangkutan',$tampilan,'',["class"=>"form-control select2"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Hubungan keluarga dengan Jemaat <b> <?=$data[0]->NAMA_JEMAAT; ?> </b>
                                    </label>
									<?php 
										$tampilan = [''=>'-- Pilih Status Keluarga --'];
										for($i=0; $i<count($data_jemaat); $i++){
											$tampilan2 = ['Suami'=>'Suami','Istri'=>'Istri','Saudara'=>'Saudara','Anak'=>'Anak','Orang Tua'=>'Orang Tua'];
										}
										echo form_dropdown('status',$tampilan2,'',["class"=>"form-control select2"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Tanggal Pernikahan (bila tidak ada tidak perlu diisi)
                                    </label>
									<input type="date" onkeydown="return false" class="form-control" min="1000" max="3000" name="tanggal_pernikahan">
								</div>
								<div class="form-group">
									<label for="">
                                        Tempat Pernikahan (bila tidak ada tidak perlu diisi)
                                    </label>
									<textarea class="form-control" style="resize:none;"name="tempat_pernikahan"></textarea>
								</div>
								<div class="form-group">
							</div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-jemaat')?>" class="btn btn-secondary"> Batal </a>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
<?=$pesan?>	
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<script src="<?=base_url('plugins/select2/js/select2.full.min.js')?>"></script>
<script src="<?=base_url('dist/js/bs-custom-file-input.min.js')?>"></script>
<script src="<?=base_url('dist/js/bs-custom-file-input.js')?>"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Initialize Select2 Elements
    $('.select2').select2()
  })

  $(document).ready(function () {
  bsCustomFileInput.init()
})
</script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>