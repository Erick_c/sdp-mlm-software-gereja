<?php $this->load->view('header_admin',['title'=>'Cabang Baru']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'inventaris']);?>
<link rel="stylesheet" href="<?=base_url('plugins/select2/css/select2.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')?>">
<style>
	span.select2-selection.select2-selection--single
	{
		height:initial;
	}
</style>
<div class="content-wrapper" style="min-height: 1200.88px;">
    <section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Ubah Inventaris</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?=base_url('/')?>">Beranda</a></li>
						<li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
						<li class="breadcrumb-item active">Ubah Inventaris</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<form action="" role="form" method="post">
							<div class="card-body">
								<div class="form-group">
									<label for="">
                                        Nama Barang Atau Jenis Barang
                                    </label>
									<input type="text" value="<?=$data[0]->NAMA_ATAU_JENIS_BARANG?>" class="form-control" name="nama_atau_jenis_barang" required>
								</div>
								<div class="form-group">
									<label for="">
                                        Nama Cabang
                                    </label>
									<?php 
										$tampilan = [''=>'-- Pilih Cabang --'];
										for($i=0; $i<count($data_cabang); $i++){
											$tampilan[$data_cabang[$i]->ID_CABANG]=$data_cabang[$i]->NAMA_CABANG;
										}
										echo form_dropdown('id_cabang',$tampilan,$data[0]->ID_CABANG,["class"=>"form-control select2"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Merk Atau Tipe Barang
                                    </label>
									<input type="text" value="<?=$data[0]->MERK_ATAU_TIPE_BARANG?>" class="form-control"name="merk_atau_tipe_barang" required>
								</div>
								<div class="form-group">
									<label for="">
                                        Tanggal Pembelian
                                    </label>
									<input type="date" class="form-control" min="1000" max="3000" name="tahun_pembelian" value="<?=$data[0]->TAHUN_PEMBELIAN?>" required>
								</div>
								<div class="form-group">
									<label for="">
                                        Jumlah Barang
                                    </label>
									<input type="number" class="form-control" min="1" max="999999999999" name="jumlah_barang" value="<?=$data[0]->JUMLAH_BARANG?>"required>
								</div>
								<div class="form-group">
									<label for="">
                                        Keterangan
                                    </label>
									<textarea class="form-control" style="resize:none;"name="keterangan" required><?=$data[0]->KETERANGAN?></textarea>
								</div>
							</div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-inventaris')?>" class="btn btn-secondary"> Batal </a>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
<?=$pesan?>	
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<script src="<?=base_url('plugins/select2/js/select2.full.min.js')?>"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Initialize Select2 Elements
    $('.select2').select2()
  })
  
</script>