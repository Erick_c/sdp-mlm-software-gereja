<?php $this->load->view('header_admin',['title'=>'Ubah Data Pembicara']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'list pembicara']);?>
<link rel="stylesheet" href="<?=base_url('plugins/select2/css/select2.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')?>">
<style>
	span.select2-selection.select2-selection--single
	{
		height:initial;
	}
</style>
<div class="content-wrapper" style="min-height: 1200.88px;">
    <section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Ubah Data Pembicara</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?=base_url('/')?>">Beranda</a></li>
						<li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
						<li class="breadcrumb-item active">Ubah Data Pembicara</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<form action="" role="form" method="post">
							<div class="card-body">
                                <div class="form-group">
                                    <label for="">
                                        Nama Pembicara
                                    </label>
                                    <input type="text" class="form-control" name="nama_pembicara" value="<?=$data_default[0]->NAMA_PEMBICARA;?>" required>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        Alamat Pembicara
                                    </label>
                                    <input type="textarea" class="form-control" name="alamat_rumah" value="<?=$data_default[0]->ALAMAT_RUMAH;?>" required>
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        No Telp
                                    </label>
                                    <input type="text" class="form-control" name="no_telp" value="<?=$data_default[0]->NO_TELP;?>" required>
                                </div>
							</div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pembicara')?>" class="btn btn-secondary"> Batal </a>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
<?=$pesan?>	
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<script src="<?=base_url('plugins/select2/js/select2.full.min.js')?>"></script>
<script src="<?=base_url('dist/js/bs-custom-file-input.min.js')?>"></script>
<script src="<?=base_url('dist/js/bs-custom-file-input.js')?>"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Initialize Select2 Elements
    $('.select2').select2()
  })

  $(document).ready(function () {
  bsCustomFileInput.init()
})
</script>