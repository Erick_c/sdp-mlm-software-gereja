<?php $this->load->view('header_admin',['title'=>'Master Super Admin']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'list inventaris']);?>
<link rel="stylesheet" href="<?=base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <div class="container-fluid">
		<div class="row mb-2">
		  <div class="col-sm-6">
			<h1>List Inventaris</h1>
		  </div>
		  <div class="col-sm-6">
			<ol class="breadcrumb float-sm-right">
			  <li class="breadcrumb-item"><a href="#">Beranda</a></li>
			  <li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
			  <li class="breadcrumb-item active">List Inventaris</li>
			</ol>
		  </div>
		</div>
	  </div><!-- /.container-fluid -->
	</section>
	<!-- /.content-header -->

    <section class="content">
      <!-- Table Cabang content -->    
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Semua Data</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
				  <?php 
					$this->table->set_template([
						'table_open'=>'<table id="" class="with_search table table-bordered table-strip">',
					]);
					$tampilkan = [['Nama atau Jenis Barang','Cabang','Merk atau Tipe Barang','Tahun Pembelian Barang','Jumlah', 'Keterangan','','']];
					for ($i=0;$i<count($data);$i++)
					{
						// [
						$tampilkan[$i+1][0] = $data[$i]->NAMA_ATAU_JENIS_BARANG;
						$tampilkan[$i+1][1] = $data[$i]->NAMA_CABANG;
						$tampilkan[$i+1][2] = $data[$i]->MERK_ATAU_TIPE_BARANG;
						$tampilkan[$i+1][3] = date('l d M Y',strtotime($data[$i]->TAHUN_PEMBELIAN));
						$tampilkan[$i+1][5] = $data[$i]->JUMLAH_BARANG;
						$tampilkan[$i+1][6] = $data[$i]->KETERANGAN;
						$tampilkan[$i+1][7] = '<a href="'.base_url('beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/update').'/'.$data[$i]->KODE_BARANG.'" class="btn btn-primary fa fa-edit nav-icon" ></a>';
						$tampilkan[$i+1][8] = '<a href="'.base_url('beranda/pelayanan/beranda-admin/super-admin/mst-inventaris/delete').'/'.$data[$i]->KODE_BARANG.'" class="btn btn-danger fa fa-trash nav-icon" ></a>';
					    // ];
					}
					echo $this->table->generate($tampilkan);
				  ?>
			  </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div><!-- /.container-fluid -->
      <!-- /.content -->
    </section>
  </div>
  <!-- /.content-wrapper -->
<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>