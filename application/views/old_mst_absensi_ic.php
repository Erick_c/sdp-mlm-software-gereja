<?php $this->load->view('header_admin',['title'=>'Master Super Admin']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'list absensi ic']);?>
<link rel="stylesheet" href="<?=base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <div class="container-fluid">
		<div class="row mb-2">
		  <div class="col-sm-6">
			<h1>Absensi IC</h1>
			<sup> Silahkan pilih jemaat untuk melihat grafik kedatangan </sup>
		  </div>
		  <div class="col-sm-6">
			<ol class="breadcrumb float-sm-right">
			  <li class="breadcrumb-item"><a href="#">Beranda</a></li>
			  <li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
			  <li class="breadcrumb-item active">Absensi IC</li>
			</ol>
		  </div>
		</div>
	  </div><!-- /.container-fluid -->
	</section>
	<!-- /.content-header -->

    <section class="content">
      <!-- Table Cabang content -->    
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Semua Data</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
				<style>
					table.table-responsive th{
						width:100%;
						min-width:200px;
					}
					
					table.table-responsive td
					{
						line-height:100px;
					}
				</style>
				  <?php 
					$this->table->set_template([
						'table_open'=>'<table id="" class="with_search table table-bordered table-strip table-responsive">',
					]);
					
					$tampilkan = [[['style'=>'width:100px;min-width:100px','data'=>'Foto'],'Nama','Jenis Kelamin','Alamat Jemaat',['style'=>'min-width:120px;']]];
					for($i=0;$i<count($data);$i++){
						$isi = [];
						$isi[] = '<div class="profile-user-img img-fluid img-circle"'.
							"style='".'background-image:url("data:image/jpeg;base64,'.base64_encode($data[$i]->FOTO).'"'.");height :100px;width:100px;background-size:auto 100%;background-position:center;'></div>";
						$isi[] = $data[$i]->NAMA_JEMAAT;
						$isi[] = (strtolower($data[$i]->JENIS_KELAMIN)=='l')?'Laki-laki':'Perempuan';
						$isi[] = $data[$i]->ALAMAT_JEMAAT;
						$isi[] = "<center><a href='".base_url('beranda/pelayanan/beranda-admin/super-admin/mst-absensi-ic/laporan-absensi-ic/'.$data[$i]->NO_IC)."' class='btn btn-secondary'>LIHAT ABSENSI</a></center>";
						$tampilkan[]=$isi;
					}
					echo $this->table->generate($tampilkan);
				  ?>
			  </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div><!-- /.container-fluid -->
      <!-- /.content -->
    </section>
  </div>
  <!-- /.content-wrapper -->
<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>