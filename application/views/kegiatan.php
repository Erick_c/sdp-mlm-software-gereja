<?php $this->load->view('header',['title'=>$nama]);?>
	<!-- ##### Breadcrumb Area Start ##### -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?=base_url('beranda')?>">Beranda</a></li>
                            <li class="breadcrumb-item"><a href="<?=base_url('beranda/kegiatan-rutin')?>">Kegiatan Rutin</a></li>
                            <li class="breadcrumb-item"><a href="<?=base_url('beranda/kegiatan-rutin/cabang')?>">Cabang</a></li>
                            <li class="breadcrumb-item"><a href="<?=base_url('beranda/kegiatan-rutin/cabang/'.$link)?>">...</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->
    <!-- ##### About Area Start ##### -->
    <section class="about-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h1><?=$nama?></h1>
                        <br><br>
                        <img src="<?=base_url('img/bg-img/3.jpg')?>" width='800' />
                    </div>
                </div>
            </div>
            <div class="row about-content justify-content-center">
                <!-- Single About Content -->
                <div class="col-48 col-md-24 col-lg-12" style="text-align:center">
                    <div class="about-us-content mb-100">
                        <div class='about-text' style="border-bottom:1px solid silver;">
                            <p style="font-size:16pt"><?=$nama?></p>
                        </div>
                        <div class='about-text col-5 col-md-6 col-lg-6' style="float:left;text-align:right;">
                            <br>
                            <p>
                                <b> Email : </b> 
                            </p>
                            <p>
                                <b> Telepon : </b> 
                            </p>
                            <p>
                                <b> Tempat Ibadah : </b> 
                            </p>
                        </div>
                        <div class='about-text col-5 col-md-6 col-lg-6' style="float:left;text-align:left;">
                            <br>
                            <p>
                                info.gbt_surabaya@gmail.com
                            </p>
                            <p>
                                08934859359
                            </p>
                            <p>
                                Jln. Sudirman Blok 8 Sidoarjo
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row about-content section-padding-100-0">
                <!-- Title About Content -->
                <div class="col-48 col-md-24 col-lg-12">
                    <div class="about-us-content mb-50">
                        <div class='about-text' style="border-bottom:1px solid silver;">
                            <p style="font-size:16pt;text-align:center;">Jadwal Kegiatan Rutin</p>
                        </div>
                    </div>
                </div>
				<!--
                <<'"!"'>-- Single About Content --<'""'>>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="about-us-content mb-100">
                        <div class='about-text' style="border-bottom:1px solid silver;">
                            <p style="font-size:16pt">Senin</p>
                        </div>
                        <div class='about-text'>
                            <br>
                            <p>
                                10.00 : Ibadah Umum I <br>
                                08.30 : Ibadah Umum II <br>
                                11.00 : Ibadah Umum III <br>
                                14.30 : Ibadah Umum IV <br>
                                17.00 : Ibadah Umum V <br>
                                19.30 : Ibadah Umum VI <br>
                            </p>
                        </div>
                    </div>
                </div>
                <<'"!"'>-- Single About Content --<'""'>>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="about-us-content mb-100">
                        <div class='about-text' style="border-bottom:1px solid silver;">
                            <p style="font-size:16pt">Selasa</p>
                        </div>
                        <div class='about-text'>
                            <br>
                            <p>
                                10.00 : Ibadah Umum I <br>
                                08.30 : Ibadah Umum II <br>
                                11.00 : Ibadah Umum III <br>
                                14.30 : Ibadah Umum IV <br>
                                17.00 : Ibadah Umum V <br>
                                19.30 : Ibadah Umum VI <br>
                            </p>
                        </div>
                    </div>
                </div>
                <<'"!"'>-- Single About Content --<'""'>>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="about-us-content mb-100">
                        <div class='about-text' style="border-bottom:1px solid silver;">
                            <p style="font-size:16pt">Rabu</p>
                        </div>
                        <div class='about-text'>
                            <br>
                            <p>
                                10.00 : Ibadah Umum I <br>
                                08.30 : Ibadah Umum II <br>
                                11.00 : Ibadah Umum III <br>
                                14.30 : Ibadah Umum IV <br>
                                17.00 : Ibadah Umum V <br>
                                19.30 : Ibadah Umum VI <br>
                            </p>
                        </div>
                    </div>
                </div>
                <<'"!"'>-- Single About Content --<'""'>>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="about-us-content mb-100">
                        <div class='about-text' style="border-bottom:1px solid silver;">
                            <p style="font-size:16pt">Kamis</p>
                        </div>
                        <div class='about-text'>
                            <br>
                            <p>
                                10.00 : Ibadah Umum I <br>
                                08.30 : Ibadah Umum II <br>
                                11.00 : Ibadah Umum III <br>
                                14.30 : Ibadah Umum IV <br>
                                17.00 : Ibadah Umum V <br>
                                19.30 : Ibadah Umum VI <br>
                            </p>
                        </div>
                    </div>
                </div>
                <<'"!"'>-- Single About Content --<'""'>>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="about-us-content mb-100">
                        <div class='about-text' style="border-bottom:1px solid silver;">
                            <p style="font-size:16pt">Jumat</p>
                        </div>
                        <div class='about-text'>
                            <br>
                            <p>
                                10.00 : Ibadah Umum I <br>
                                08.30 : Ibadah Umum II <br>
                                11.00 : Ibadah Umum III <br>
                                14.30 : Ibadah Umum IV <br>
                                17.00 : Ibadah Umum V <br>
                                19.30 : Ibadah Umum VI <br>
                            </p>
                        </div>
                    </div>
                </div>
				-->
                <!-- Single About Content -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="about-us-content mb-100">
                        <div class='about-text' style="border-bottom:1px solid silver;">
                            <p style="font-size:16pt">Sabtu</p>
                        </div>
                        <div class='about-text'>
                            <br>
                            <p>
                                <!--08.30 : Ibadah Umum I <br>-->
                                <!--10.00 : Ibadah Umum II <br>-->
                                <!--11.00 : Ibadah Umum III <br>-->
                                <!--14.30 : Ibadah Umum IV <br>-->
                                17.00 : Ibadah Umum V <br>
                                19.30 : Ibadah Umum VI <br>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- Single About Content -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="about-us-content mb-100">
                        <div class='about-text' style="border-bottom:1px solid silver;">
                            <p style="font-size:16pt">Minggu</p>
                        </div>
                        <div class='about-text'>
                            <br>
                            <p>
                                08.30 : Ibadah Umum I <br>
                                10.00 : Ibadah Umum II <br>
                                <!--11.00 : Ibadah Umum III <br>-->
                                <!--14.30 : Ibadah Umum IV <br>-->
                                17.00 : Ibadah Umum V <br>
                                19.30 : Ibadah Umum VI <br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>    
    <!-- ##### About Area End ##### -->

<?php $this->load->view('footer');?>