<?php $this->load->view('header_admin',['title'=>'Peminjaman']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'peminjaman inventaris']);?>
<link rel="stylesheet" href="<?=base_url('plugins/select2/css/select2.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')?>">
<style>
	span.select2-selection.select2-selection--single
	{
		height:initial;
	}
</style>
<div class="content-wrapper" style="min-height: 1200.88px;">
    <section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Peminjaman</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?=base_url('/')?>">Beranda</a></li>
						<li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
						<li class="breadcrumb-item active">Peminjaman</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<form action="" role="form" method="post">
							<div class="card-body">
								<div class="form-group">
									<label for="">
                                        Nama Peminjam
                                    </label>
									<?php 
										$tampilan = [''=>'-- Pilih Jemaat --'];
										for($i=0; $i<count($data_jemaat); $i++){
											$tampilan[$data_jemaat[$i]->ID_JEMAAT]=$data_jemaat[$i]->NAMA_JEMAAT;
										}
										echo form_dropdown('penanggung_jawab',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Nama Atau Jenis Barang
                                    </label>
									<?php 
										$tampilan = [''=>'-- Pilih Barang --'];
										for($i=0; $i<count($data_inventaris); $i++){
											$tampilan[$data_inventaris[$i]->KODE_BARANG]=$data_inventaris[$i]->NAMA_ATAU_JENIS_BARANG.' - '.$data_inventaris[$i]->MERK_ATAU_TIPE_BARANG;
										}
										echo form_dropdown('kode_barang',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Item
                                    </label>
									<input type="number" name="item" min=1 class="form-control">
								</div>
								<div class="form-group">
									<label for="">
                                        Nama Kegiatan
                                    </label>
									<?php 
										$tampilan = [''=>'-- Pilih Kegiatan --'];
										for($i=0; $i<count($data_kegiatan); $i++){
											if ($data_kegiatan[$i]->NAMA_KEGIATAN==null)
												$tampilan[$data_kegiatan[$i]->ID_KEGIATAN]=$data_kegiatan[$i]->NAMA_KATEGORI;
											else 
												$tampilan[$data_kegiatan[$i]->ID_KEGIATAN]=$data_kegiatan[$i]->NAMA_KEGIATAN;
										}
										echo form_dropdown('id_kegiatan',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
							</div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-inventaris')?>" class="btn btn-secondary"> Batal </a>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
<?=$pesan?>	
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<script src="<?=base_url('plugins/select2/js/select2.full.min.js')?>"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Initialize Select2 Elements
    $('.select2').select2()
  })
  
</script>