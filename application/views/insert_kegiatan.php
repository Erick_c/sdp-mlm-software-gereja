<?php $this->load->view('header_admin',['title'=>'Kegiatan Baru']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'kegiatan baru']);?>
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet" href="<?=base_url('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2/css/select2.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')?>">
<style>
	span.select2-selection.select2-selection--single
	{
		height:initial;
	}
</style>
<div class="content-wrapper" style="min-height: 1200.88px;">
    <section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Kegiatan Baru</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?=base_url('/')?>">Beranda</a></li>
						<li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
						<li class="breadcrumb-item active">Kegiatan Baru</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="card">
			<div class="card-header p-2">
				<ul class="nav nav-pills">
					<li class="nav-item"><a class="nav-link active" href="#mingguan" data-toggle="tab">Mingguan</a></li>
					<li class="nav-item"><a class="nav-link" href="#ic" data-toggle="tab">IC</a></li>
					<li class="nav-item"><a class="nav-link" href="#lainnya" data-toggle="tab">Lain-lain</a></li>
				</ul>
			</div>
			<!-- /.card-header -->
			<div class="card-body">
				<div class="tab-content">
					<div class="tab-pane active" id="mingguan">
						<form action="" method="post" role="form">
							<div class="card-body">
								<div class="form-group">
									<label for="">
										Topik Kegiatan Mingguan
									</label>
									<input type="text" name="nama_kegiatan" required class="form-control" autocomplete="off">
								</div>
								<!-- <div class="form-group">
									<label for="">
										Pembicara atau Pendeta
									</label>									
									<?php
										$tampilan = [''=>'-- Pilih Pembicara --'];
										for($i=0; $i<count($data_pembicara); $i++){
											$tampilan[$data_pembicara[$i]->ID_PEMBICARA]=$data_pembicara[$i]->NAMA_PEMBICARA;
										}
										echo form_dropdown('id_pembicara',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div> -->
								<div class="form-group">
									<label for="">
										Kategori Kegiatan
									</label>									
									<?php
										$tampilan = [''=>'-- Pilih Kategori --'];
										for($i=0; $i<count($data_kategori); $i++){
											$tampilan[$data_kategori[$i]->ID_KATEGORI]=$data_kategori[$i]->NAMA_KATEGORI;
										}
										echo form_dropdown('id_kategori',$tampilan,'',["class"=>"form-control select2","required"=>"required"]);
									?>
								</div>
								<div class="form-group">
									<label for="">
										Nama Cabang
									</label>
									<?php
										$tampilan = [''=>'-- Pilih Cabang --'];
										for($i=0; $i<count($data_cabang); $i++){
											$tampilan[$data_cabang[$i]->ID_CABANG]=$data_cabang[$i]->NAMA_CABANG;
										}
										echo form_dropdown('id_cabang',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<div class="form-group">
									<label for="">
										Waktu Kegiatan
									</label>
									<div class="input-group date" id="timepicker" data-target-input="nearest">
										<div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="far fa-clock"></i></div>
										</div>
										<input type="text" name="waktu_kegiatan" class="form-control datetimepicker-input" data-target="#timepicker" data-toggle="datetimepicker" onkeydown="return false;" autocomplete="off" required/>
									</div>
								</div>
								<div class="form-group">
									<label for="">
										Tanggal Kegiatan
									</label>
									<div class="input-group date" id="datepicker" data-target-input="nearest">
										<div class="input-group-append" data-target="#datepicker" data-toggle="datetimepicker">
											<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
										</div>
										<input type="text" name="tanggal_kegiatan" class="form-control datetimepicker-input" data-target="#datepicker" data-toggle="datetimepicker" onkeydown="return false;" autocomplete="off" required/>
									</div>
								</div>
								<div class="form-group">
									<label for="">
										Pembicara atau Pendeta
									</label>

									<?php 
										$this->table->set_template([
											'table_open'=>'<table id="" class="with_search table table-bordered table-strip">',
										]);
										
										$tampilan = [['','Nama Pembicara atau Pendeta','Asal Pembicara atau Pendeta']];

										for ($i=0; $i < count($data_pembicara); $i++) { 
											$tampilan[$i+1][] = "<input type='radio' style='float:right;margin-top:5px' name='id_pembicara' id='".$data_pembicara[$i]->ID_PEMBICARA."' value='".$data_pembicara[$i]->ID_PEMBICARA."'/>";
											$tampilan[$i+1][] = "<label for='".$data_pembicara[$i]->ID_PEMBICARA."' style='font-weight:inherit'>".$data_pembicara[$i]->NAMA_PEMBICARA."</label>";
											$tampilan[$i+1][] = "<label for='".$data_pembicara[$i]->ID_PEMBICARA."' style='font-weight:inherit'>".$data_pembicara[$i]->ALAMAT_RUMAH."</label>";
										}

										echo $this->table->generate($tampilan);
									?>
								</div>
							</div>
							<div class="card-footer">
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>
						</form>
					</div>
					<div class="tab-pane" id="ic">
						<form action="" method="post" role="form">
							<div class="card-body">
								<div class="form-group">
									<label for="">
										Topik Kegiatan IC
									</label>
									<input type="text" name="nama_kegiatan" required class="form-control" autocomplete="off">
								</div>
								<!-- <div class="form-group">
									<label for="">
										Pembicara atau Pendeta
									</label>									
									<?php
										$tampilan = [''=>'-- Pilih Pembicara --'];
										for($i=0; $i<count($data_pembicara); $i++){
											$tampilan[$data_pembicara[$i]->ID_PEMBICARA]=$data_pembicara[$i]->NAMA_PEMBICARA;
										}
										echo form_dropdown('id_pembicara',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div> -->
								<div class="form-group">
									<label for="">
                                        Lokasi Kegiatan
                                    </label>
									<input type="text" name="lokasi_kegiatan" class="form-control" autocomplete="off" required>
								</div>
								<div class="form-group">
									<label for="">
										IC Yang Bersangkutan
                                    </label>									
									<?php
										$tampilan = [''=>'-- Pilih IC --'];
										for($i=0; $i<count($data_ic); $i++){
											$tampilan[$data_ic[$i]->NO_IC]=$data_ic[$i]->NAMA_IC;
										}
										echo form_dropdown('no_ic',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
									<input type="hidden" name="id_kategori" value="<?php
										for ($i=0; $i < count($data_kategori); $i++) { 
											if(strpos(strtolower($data_kategori[$i]->NAMA_KATEGORI),'ic') > -1) echo $data_kategori[$i]->ID_KATEGORI;
										}	
									?>">
								</div>
								<div class="form-group">
									<label for="">
										Waktu Kegiatan
									</label>
									<div class="input-group date" id="timepicker2" data-target-input="nearest">
										<div class="input-group-append" data-target="#timepicker2" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="far fa-clock"></i></div>
										</div>
										<input type="text" name="waktu_kegiatan" class="form-control datetimepicker-input" data-target="#timepicker2" data-toggle="datetimepicker" onkeydown="return false;" autocomplete="off" required/>
									</div>
								</div>
								<div class="form-group">
									<label for="">
										Tanggal Kegiatan
									</label>
									<div class="input-group date" id="datepicker2" data-target-input="nearest">
										<div class="input-group-append" data-target="#datepicker2" data-toggle="datetimepicker">
											<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
										</div>
										<input type="text" name="tanggal_kegiatan" class="form-control datetimepicker-input" data-target="#datepicker2" data-toggle="datetimepicker" onkeydown="return false;" autocomplete="off" required/>
									</div>
								</div>
								<div class="form-group">
									<label for="">
										Pembicara atau Pendeta
									</label>

									<?php 
										$this->table->set_template([
											'table_open'=>'<table id="" class="with_search table table-bordered table-strip">',
										]);
										
										$tampilan = [['','Nama Pembicara atau Pendeta','Asal Pembicara atau Pendeta']];

										for ($i=0; $i < count($data_pembicara); $i++) { 
											$tampilan[$i+1][] = "<input type='radio' style='float:right;margin-top:5px' name='id_pembicara' id='".$data_pembicara[$i]->ID_PEMBICARA."2' value='".$data_pembicara[$i]->ID_PEMBICARA."'/>";
											$tampilan[$i+1][] = "<label for='".$data_pembicara[$i]->ID_PEMBICARA."2' style='font-weight:inherit'>".$data_pembicara[$i]->NAMA_PEMBICARA."</label>";
											$tampilan[$i+1][] = "<label for='".$data_pembicara[$i]->ID_PEMBICARA."2' style='font-weight:inherit'>".$data_pembicara[$i]->ALAMAT_RUMAH."</label>";
										}

										echo $this->table->generate($tampilan);
									?>
								</div>
							</div>
							<div class="card-footer">
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>
						</form>
					</div>
					<div class="tab-pane" id="lainnya">
						<form action="" method="post" role="form">
							<div class="card-body">
								<div class="form-group">
									<label for="">
										Topik Kegiatan 
									</label>
									<input type="text" name="nama_kegiatan" required class="form-control">
								</div>
								<div class="form-group">
									<label for="">
										Pembicara atau Pendeta
									</label>									
									<?php
										$tampilan = [''=>'-- Pilih Pembicara --'];
										for($i=0; $i<count($data_pembicara); $i++){
											$tampilan[$data_pembicara[$i]->ID_PEMBICARA]=$data_pembicara[$i]->NAMA_PEMBICARA;
										}
										echo form_dropdown('id_pembicara',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Lokasi Kegiatan
                                    </label>
									<input type="text" name="lokasi_kegiatan" class="form-control" autocomplete="off" required>
								</div>
								<div class="form-group">
									<label for="">
										IC Yang Bersangkutan
                                    </label>									
									<?php
										$tampilan = [''=>'-- Pilih IC --'];
										for($i=0; $i<count($data_ic); $i++){
											$tampilan[$data_ic[$i]->NO_IC]=$data_ic[$i]->NAMA_IC;
										}
										echo form_dropdown('no_ic',$tampilan,'',["class"=>"form-control select2"])
									?>
								</div>
								<div class="form-group">
									<label for="">
										Kategori Kegiatan
									</label>									
									<?php
										$tampilan = [''=>'-- Pilih Kategori --'];
										for($i=0; $i<count($data_kategori); $i++){
											$tampilan[$data_kategori[$i]->ID_KATEGORI]=$data_kategori[$i]->NAMA_KATEGORI;
										}
										echo form_dropdown('id_kategori',$tampilan,'',["class"=>"form-control select2","required"=>"required"]);
									?>
								</div>
								<div class="form-group">
									<label for="">
										Nama Cabang
									</label>
									<?php
										$tampilan = [''=>'-- Pilih Cabang --'];
										for($i=0; $i<count($data_cabang); $i++){
											$tampilan[$data_cabang[$i]->ID_CABANG]=$data_cabang[$i]->NAMA_CABANG;
										}
										echo form_dropdown('id_cabang',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<div class="form-group">
									<label for="">
										Waktu Kegiatan
									</label>
									<div class="input-group date" id="timepicker3" data-target-input="nearest">
										<div class="input-group-append" data-target="#timepicker3" data-toggle="datetimepicker">
											<div class="input-group-text"><i class="far fa-clock"></i></div>
										</div>
										<input type="text" name="waktu_kegiatan" class="form-control datetimepicker-input" data-target="#timepicker3" data-toggle="datetimepicker" onkeydown="return false;" autocomplete="off" required/>
									</div>
								</div>
								<div class="form-group">
									<label for="">
										Tanggal Kegiatan
									</label>
									<div class="input-group date" id="datepicker3" data-target-input="nearest">
										<div class="input-group-append" data-target="#datepicker3" data-toggle="datetimepicker">
											<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
										</div>
										<input type="text" name="tanggal_kegiatan" class="form-control datetimepicker-input" data-target="#datepicker3" data-toggle="datetimepicker" onkeydown="return false;" autocomplete="off" required/>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php /*
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<form action="" method="post" role="form">
							<div class="card-body">
								<div class="form-group">
									<label for="">
                                        Nama Kegiatan (*)
                                    </label>
									<input type="text" name="nama_kegiatan" class="form-control">
								</div>
								<div class="form-group">
									<label for="">
										Kategori Kegiatan
                                    </label>									
									<?php
										$tampilan = [''=>'-- Pilih Kategori --'];
										for($i=0; $i<count($data_kategori); $i++){
											$tampilan[$data_kategori[$i]->ID_KATEGORI]=$data_kategori[$i]->NAMA_KATEGORI;
										}
										echo form_dropdown('id_kategori',$tampilan,'',["class"=>"form-control select2"]);
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Nama Cabang (*)
                                    </label>
									<?php
										$tampilan = [''=>'-- Pilih Cabang --'];
										for($i=0; $i<count($data_cabang); $i++){
											$tampilan[$data_cabang[$i]->ID_CABANG]=$data_cabang[$i]->NAMA_CABANG;
										}
										echo form_dropdown('id_cabang',$tampilan,'',["class"=>"form-control select2"])
									?>
								</div>
								<div class="form-group">
									<label for="">
										Nama IC (*)
                                    </label>									
									<?php
										$tampilan = [''=>'-- Pilih IC --'];
										for($i=0; $i<count($data_ic); $i++){
											$tampilan[$data_ic[$i]->NO_IC]=$data_ic[$i]->NAMA_IC;
										}
										echo form_dropdown('no_ic',$tampilan,'',["class"=>"form-control select2"])
									?>
								</div>
								<div class="form-group">
									<label for="">
										Pembicara
                                    </label>									
									<?php
										$tampilan = [''=>'-- Pilih Pembicara --'];
										for($i=0; $i<count($data_pembicara); $i++){
											$tampilan[$data_pembicara[$i]->ID_PEMBICARA]=$data_pembicara[$i]->NAMA_PEMBICARA;
										}
										echo form_dropdown('id_pembicara',$tampilan,'',["class"=>"form-control select2","required"=>"required"])
									?>
								</div>
								<div class="form-group">
									<label for="">
                                        Lokasi Kegiatan
                                    </label>
									<input type="text" name="lokasi_kegiatan" class="form-control"required>
								</div>
								<div class="form-group">
									<label for="">
                                        Waktu Kegiatan
                                    </label>
									<input type="time" name="waktu_kegiatan" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="">
                                        Tanggal Kegiatan
                                    </label>
									<input type="date" name="tanggal_kegiatan" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="">
                                        Lokasi Berkumpul (*)
                                    </label>
									<input type="text" name="tempat_berkumpul" class="form-control" >
								</div>
								<div class="form-group">
									<label for="">
                                        Waktu Berkumpul (*)
                                    </label>
									<input type="time" name="waktu_berkumpul" class="form-control">
								</div>
							</div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="<?=base_url('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan')?>" class="btn btn-secondary"> Batal </a>
                            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	/** */ ?>
<?=$pesan?>
<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<script src="<?=base_url('plugins/select2/js/select2.full.min.js')?>"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Initialize Select2 Elements
    $('.select2').select2()
  })
  
</script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=base_url('plugins/moment/moment.min.js')?>"></script>
<script src="<?=base_url('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')?>"></script>
<script>
	$(function () {
		//Timepicker
		$('#timepicker').datetimepicker({
			format: 'H:m'
		});
		$('#timepicker2').datetimepicker({
			format: 'H:m'
		});
		$('#timepicker3').datetimepicker({
			format: 'H:m'
		});
		//Datepicker
		$('#datepicker').datetimepicker({
			format: 'Y-M-D'
		});
		$('#datepicker2').datetimepicker({
			format: 'Y-M-D'
		});
		$('#datepicker3').datetimepicker({
			format: 'Y-M-D'
		});
	});
</script>