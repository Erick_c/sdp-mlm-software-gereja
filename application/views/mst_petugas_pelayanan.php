<?php $this->load->view('header_admin',['title'=>$_SESSION['NAMA_PELAYANAN']]);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'pelayanan'])?>
<?php date_default_timezone_set("Asia/Bangkok");?>
<?php if(count($data_pelayanan)==0) redirect(base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan'));?>
<link rel="stylesheet" href="<?=base_url('plugins/select2/css/select2.min.css')?>">
<link rel="stylesheet" href="<?=base_url('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')?>">
<style>
	span.select2-selection.select2-selection--single
	{
		height:initial;
	}
</style>
<link rel="stylesheet" href="<?=base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>"><!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Petugas Pelayanan</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Beranda</a></li>
            <li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
            <li class="breadcrumb-item active">Petugas Pelayanan</li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            <div class="card-header p-2">
            <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#list_petugas" data-toggle="tab">List Petugas</a></li>
            </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
            <div class="tab-content">
                <div class="active tab-pane" id="pelayanan">
                    <h3>List Petugas <?=(count($data_pelayanan) > 0)?$data_pelayanan[0]->NAMA_PELAYANAN:''?></h3><br>
                    <form class="form-horizontal" action='<?=(count($data_pelayanan)>0)?base_url('beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-pelayanan/insert/'.$data_pelayanan[0]->ID_PELAYANAN):''?>' method="post">
                        <div class="form-group row">
                            <label for="nama_pelayanan" class="col-sm-2 col-form-label">Nama Jemaat</label>
                            <div class="col-sm-10">
                                <?php
                                    $tampilan = [''=>'-- Pilih Jemaat --'];
                                    for($i=0; $i<count($data_jemaat); $i++){
                                        $tampilan[$data_jemaat[$i]->ID_JEMAAT]=$data_jemaat[$i]->NAMA_JEMAAT;
                                    }
                                    echo form_dropdown('id_jemaat',$tampilan,'',["class"=>"form-control select2"]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </div>
                        </div>
                    </form><hr><br>
                    <style>
						table.table-responsive th{
							width:100%;
							min-width:200px;
						}
						
						table.table-responsive td
						{
							line-height:100px;
						}
					</style>
                    <?php 
						$this->table->set_template([
							'table_open'=>'<table id="" class="with_search table table-bordered table-strip table-responsive">',
						]);
						
						//$tampilkan = [[['style'=>'width:100px;min-width:100px','data'=>'Foto'],'Nama','Jenis Kelamin','Alamat Jemaat',['style'=>'min-width:120px;']]];
						$tampilkan = [[['style'=>'width:255px;min-width:255px', 'data'=>'FOTO'], 'NAMA PETUGAS',['style'=>'width:150px;min-width:150px']]];
						for($i=0;$i<count($table_tugas_pelayanan);$i++){
                            $data = [];
                            $data[] = '<div class="profile-user-img img-fluid img-circle"'."style='".'background-image:url("data:image/jpeg;base64,'.base64_encode($table_tugas_pelayanan[$i]->FOTO).'"'.");height :100px;width:100px;background-size:auto 100%;background-position:center;'></div><center style='line-height:20pt;'> ID : ".$table_tugas_pelayanan[$i]->ID_JEMAAT."</center>";
                            $data[] = $table_tugas_pelayanan[$i]->NAMA_JEMAAT;
                            $id_pelayanan = $table_tugas_pelayanan[$i]->ID_PELAYANAN;
                            $id_jemaat = $table_tugas_pelayanan[$i]->ID_JEMAAT;
                            $data[] = "<center><a href='".base_url('/beranda/pelayanan/beranda-admin/super-admin/mst-pelayanan/list-pelayanan/delete/'.$id_pelayanan.'/'.$id_jemaat)."' class='btn btn-danger'>Berhenti Pelayanan</a></center>";
                            $tampilkan[] = $data;
						}
						echo $this->table->generate($tampilkan);
					  ?>
                </div>
            </div>
            <!-- /.tab-content -->
            </div><!-- /.card-body -->
        </div>
        <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>

<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!--select2-->
<script src="<?=base_url('plugins/select2/js/select2.full.min.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Initialize Select2 Elements
    $('.select2').select2()
  })

  $(document).ready(function () {
  bsCustomFileInput.init()
})
</script>