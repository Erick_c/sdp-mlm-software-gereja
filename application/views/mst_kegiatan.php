<?php $this->load->view('header_admin',['title'=>'Master Super Admin']);?>
<?php $this->load->view('sidebar_su',['menu_open'=>'list kegiatan']);?>
<link rel="stylesheet" href="<?=base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.css')?>">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <div class="container-fluid">
		<div class="row mb-2">
		  <div class="col-sm-6">
			<h1>List Kegiatan</h1>
		  </div>
		  <div class="col-sm-6">
			<ol class="breadcrumb float-sm-right">
			  <li class="breadcrumb-item"><a href="#">Beranda</a></li>
			  <li class="breadcrumb-item"><a href="<?=base_url('beranda/pelayanan')?>">Super Admin</a></li>
			  <li class="breadcrumb-item active">List Kegiatan</li>
			</ol>
		  </div>
		</div>
	  </div><!-- /.container-fluid -->
	</section>
	<!-- /.content-header -->

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="tab-content">
						<div class="card-header p-2">
							<ul class="nav nav-pills">
								<li class="nav-item"><a class="nav-link active" href="#mendatang" id="tab1" data-toggle="tab">Mendatang</a></li>
								<li class="nav-item"><a class="nav-link" href="#semua" id="tab2" data-toggle="tab">Semua Kegiatan</a></li>
							</ul>	
						</div>
						<div class="card-body">
							<div class="tab-content">
								<div class="active tab-pane" id="mendatang">
									<?php 
										$this->table->set_template([
											'table_open'=>'<table id="" class="with_search table table-bordered table-strip">',
										]);
										
										$tampilkan_mendatang = [
											['Kategori Ibadah','IC','Cabang','Lokasi','Tanggal','','','']
										];

										$tampilkan_semua = [
											['Kategori Ibadah','IC','Cabang','Lokasi','Tanggal','','','']
										];
										
										for($i=0;$i<count($data);$i++){
											$tampilkan = [];
											$tampilkan[] = $data[$i]->NAMA_KATEGORI;
											
											if ($data[$i]->NAMA_IC=='') $tampilkan[] = 'Tidak ada IC yang tercantum';
											else $tampilkan[] = $data[$i]->NAMA_IC;
											
											if ($data[$i]->NAMA_CABANG=='') $tampilkan[] = 'Tidak ada cabang tercantum';
											else $tampilkan[] = $data[$i]->NAMA_CABANG;
											
											if ($data[$i]->LOKASI_KEGIATAN=='') $tampilkan[] = 'Di Gereja Cabang '.$data[$i]->NAMA_CABANG;
											else $tampilkan[] = $data[$i]->LOKASI_KEGIATAN;

											$tampilkan[] = Date('l d M Y', strtotime($data[$i]->TANGGAL_KEGIATAN));
											$tampilkan[] = "<center><a href='".base_url('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan/detail/'.$data[$i]->ID_KEGIATAN)."' class='btn btn-primary fa fa-info nav-icon'></a></center>";
											$tampilkan[] = "<center><a href='".base_url('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan/update/'.$data[$i]->ID_KEGIATAN)."' class='btn btn-primary fa fa-edit nav-icon'></a></center>";
											$tampilkan[] = "<center><a href='".base_url('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan/delete/'.$data[$i]->ID_KEGIATAN)."' class='btn btn-danger fa fa-trash nav-icon'></a></center>";
											// ];

											date_default_timezone_set('asia/bangkok');
											if (date('Y/m/d',strtotime($data[$i]->TANGGAL_KEGIATAN)) > date('Y/m/d')) {
												$tampilkan_mendatang[] = $tampilkan;
											}
											
											$tampilkan_semua[] = $tampilkan;
											
										}
										echo $this->table->generate($tampilkan_mendatang);
									?>
								</div>
								<div class="tab-pane" id="semua">
									<?php 
										echo $this->table->generate($tampilkan_semua);
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php /* ?>
    <section class="content">
      <!-- Table Cabang content -->    
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Semua Data</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
				  <?php 
					$this->table->set_template([
						'table_open'=>'<table id="" class="with_search table table-bordered table-strip">',
					]);
					
					$tampilkan = [
						['Nama','Kategori','IC','Pembicara','Cabang','Lokasi','Tanggal','Waktu','Tempat Kumpul','Waktu Kumpul','','']
					];
					
					for($i=0;$i<count($data);$i++){
						$tampilkan[] = [
							$data[$i]->NAMA_KEGIATAN,
							$data[$i]->NAMA_KATEGORI,
							$data[$i]->NAMA_IC,
							$data[$i]->NAMA_PEMBICARA,
							$data[$i]->NAMA_CABANG,
							$data[$i]->LOKASI_KEGIATAN,
							$data[$i]->TANGGAL_KEGIATAN,
							$data[$i]->WAKTU_KEGIATAN,
							$data[$i]->TEMPAT_BERKUMPUL,
							$data[$i]->WAKTU_BERKUMPUL,
							"<center><a href='".base_url('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan/update/'.$data[$i]->ID_KEGIATAN)."' class='btn btn-primary'>UBAH DATA</a></center>",
							"<center><a href='".base_url('beranda/pelayanan/beranda-admin/super-admin/mst-kegiatan/delete/'.$data[$i]->ID_KEGIATAN)."' class='btn btn-secondary'>HAPUS DATA</a></center>"
						];
					}
					echo $this->table->generate($tampilkan);
				  ?>
			  </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div><!-- /.container-fluid -->
      <!-- /.content -->
    </section>
	<?php /** */ ?>
  </div>
  <!-- /.content-wrapper -->
<!-- jQuery -->
<script src="<?=base_url('plugins/jquery/jquery.min.js')?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- DataTables -->
<script src="<?=base_url('plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('dist/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url('dist/js/demo.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- page script -->
<script>
  $(function () {
    $('.with_search').DataTable({
	  "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
	});
    $(".without_search").DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>