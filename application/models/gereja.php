<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gereja extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	public function update_user_password($data){
		$this->db->update('tugas_pelayanan',$data,['id_jemaat'=>$data['id_jemaat'],'id_pelayanan'=>$data['id_pelayanan']]);
		return true;
	}
	public function select_pemakaian($data=[])
	{
		$this->db->where($data);
		$this->db->join('jemaat j','j.id_jemaat=p.penanggung_jawab');
		$this->db->join('inventaris i','i.kode_barang=p.kode_barang');
		$this->db->join('cabang c','c.id_cabang=i.id_cabang');
		$this->db->join('kegiatan k','k.id_kegiatan=p.id_kegiatan');
		$this->db->join('kategori_kegiatan kk','kk.id_kategori=k.id_kategori');
		$query = $this->db->get('pemakaian p');

		return $query->result();
	}
	
	public function insert_pemakaian($data){
		$this->db->where("kode_barang='".$data['kode_barang']."' and id_kegiatan = '".$data['id_kegiatan']."' and penanggung_jawab = '".$data['penanggung_jawab']."'");
		$query = $this->db->get('pemakaian');

		date_default_timezone_set("Asia/Bangkok");
		$data['TANGGAL_DAN_WAKTU_PEMAKAIAN'] = Date('Y').'-'.Date('m').'-'.Date('d').' '.date('H:i:sa');
		$data['TANGGAL_DAN_WAKTU_PENGEMBALIAN'] = '';
		$data['STATUS_BARANG'] = '';
		
		if(count($query->result()) >= 1) {
			$q = $query->result();
			$item = $q[0]->ITEM;
			$data['item'] = $data['item']+$item;
			$this->db->update('pemakaian',$data,"kode_barang='".$data['kode_barang']."' and id_kegiatan = '".$data['id_kegiatan']."' and penanggung_jawab = '".$data['penanggung_jawab']."'");
			return true;
		}

		$this->db->insert('pemakaian',$data);
		
		return true;
	}
	
	public function update_pemakaian($data){
		$this->db->where("kode_barang='".$data['kode_barang']."' and id_kegiatan = '".$data['id_kegiatan']."' and penanggung_jawab = '".$data['penanggung_jawab']."'");
		$query = $this->db->get('pemakaian');
		
		$q = $query->result();
		if(count($query->result()) < 1) return false;

		
		date_default_timezone_set("Asia/Bangkok");
		$data['TANGGAL_DAN_WAKTU_PENGEMBALIAN'] = Date('Y').'-'.Date('m').'-'.Date('d').' '.date('H:i:sa');
		
		$this->db->update('pemakaian',$data,"kode_barang='".$data['kode_barang']."' and id_kegiatan = '".$data['id_kegiatan']."' and penanggung_jawab = '".$data['penanggung_jawab']."'");

		return true;
	}

	/* Untuk Absensi IC */
	public function select_jemaat_untuk_peric($data)
	{
		$this->db->where($data);
		$query = $this->db->get('jemaat');
		return $query->result();
	}
	
	public function select_jemaat($data)
	{
		$this->db->join('ic','ic.NO_IC=jemaat.NO_IC');
		$this->db->where($data);
		$query = $this->db->get('jemaat');
		return $query->result();
	}
	
	public function select_pelayanan_per_account($data)
	{
		$this->db->where($data);
		$this->db->join('pelayanan','pelayanan.id_pelayanan = tugas_pelayanan.id_pelayanan');
		$this->db->join('detail_pelayan_khusus','pelayanan.id_pelayanan = detail_pelayan_khusus.id_pelayanan');
		$this->db->join('kegiatan','kegiatan.id_kegiatan = detail_pelayan_khusus.id_kegiatan');
		$this->db->join('cabang','kegiatan.id_cabang = cabang.id_cabang');
		$this->db->join('kategori_kegiatan','kegiatan.id_kategori = kategori_kegiatan.id_kategori');
		$this->db->join('jemaat','jemaat.id_jemaat = tugas_pelayanan.id_jemaat');
		
		$query = $this->db->get('tugas_pelayanan');
		return $query->result();
	}
	/** Absensi Ibadah Umum */
	public function select_kegiatan_untuk_absensi($where){
		$this->db->where($where);
		$this->db->order_by('tanggal_kegiatan desc');
		$query = $this->db->get('kegiatan');
		
		return $query->result();
	}
	public function chart_select_absensi($where=[])
	{
		$this->db->select('count(id_jemaat) jml');
		$query = $this->db->get('jemaat');

		$jml_jemaat = $query->result()[0]->jml;
		
		$this->db->select('count(id_jemaat) JUMLAH_JEMAAT_HADIR, '.$jml_jemaat.'- count(id_jemaat) JUMLAH_JEMAAT_TIDAK_HADIR, '.$jml_jemaat.' JUMLAH_JEMAAT, NAMA_KEGIATAN');
		$this->db->join('kegiatan','kegiatan.id_kegiatan=daftar_kehadiran.id_kegiatan');
		$this->db->where($where);
		$this->db->order_by('tanggal_kegiatan desc');
		$query = $this->db->get('daftar_kehadiran');
		
		return $query->result();
	}
	public function chart_select_absensi_ic($no_ic='%%',$id_kegiatan='')
	{
		$this->db->where(['no_ic'=>$no_ic]);
		$this->db->select('count(id_jemaat) jml');
		$query = $this->db->get('jemaat');

		$jml_jemaat_ic= $query->result()[0]->jml;

		if ($id_kegiatan=='') $this->db->where(['jemaat.no_ic'=>$no_ic]);
		else $this->db->where(['jemaat.no_ic'=>$no_ic,'kegiatan.id_kegiatan'=>$id_kegiatan]);
		$this->db->join('kegiatan','kegiatan.id_kegiatan=daftar_kehadiran.id_kegiatan');
		$this->db->join('jemaat','jemaat.id_jemaat=daftar_kehadiran.id_jemaat');
		$this->db->select('count(jemaat.id_jemaat) JUMLAH_JEMAAT_HADIR, '.$jml_jemaat_ic.'- count(jemaat.id_jemaat) JUMLAH_JEMAAT_TIDAK_HADIR, '.$jml_jemaat_ic.' JUMLAH_JEMAAT, NAMA_KEGIATAN');
		$this->db->order_by('tanggal_kegiatan desc');
		$this->db->limit(10);
		$query = $this->db->get('daftar_kehadiran');

		return $query->result();
	}
	public function select_absensi_yang_tidak_hadir($id_kegiatan)
	{
		$this->db->from('jemaat');
		$this->db->where("not exists (select * from daftar_kehadiran where daftar_kehadiran.id_jemaat = jemaat.id_jemaat and id_kegiatan='".$id_kegiatan."')");
		$this->db->select("NAMA_JEMAAT, ID_JEMAAT, '".$id_kegiatan."'ID_KEGIATAN, FOTO");
		$query = $this->db->get();

		return $query->result();
	}
	public function select_absensi_yang_hadir($id_kegiatan)
	{
		$this->db->where(['id_kegiatan'=>$id_kegiatan]);
		$this->db->join('jemaat','jemaat.id_jemaat=daftar_kehadiran.id_jemaat');

		$query = $this->db->get('daftar_kehadiran');

		return $query->result();
	}
	public function select_absensi_ic_yang_tidak_hadir($no_ic, $id_kegiatan)
	{
		$this->db->where(['jemaat.no_ic'=>$no_ic]);
		$this->db->from('jemaat');
		$this->db->where("not exists (select * from daftar_kehadiran where daftar_kehadiran.id_jemaat = jemaat.id_jemaat and id_kegiatan='".$id_kegiatan."')");
		$this->db->select("NAMA_JEMAAT, ID_JEMAAT, '".$id_kegiatan."'ID_KEGIATAN, FOTO");
		$query = $this->db->get();

		return $query->result();
	}
	public function select_absensi_ic_yang_hadir($no_ic, $id_kegiatan)
	{
		$this->db->where(['jemaat.no_ic'=>$no_ic, 'id_kegiatan'=>$id_kegiatan]);
		$this->db->join('jemaat','jemaat.id_jemaat=daftar_kehadiran.id_jemaat');

		$query = $this->db->get('daftar_kehadiran');

		return $query->result();
	}
	
	public function insert_absensi($data)
	{
		$this->db->where(['id_kegiatan'=>$data['id_kegiatan'],'id_jemaat'=>$data['id_jemaat']]);
		$query = $this->db->get('daftar_kehadiran');
		
		if(count($query->result())==0) {
			$this->db->insert('daftar_kehadiran',$data);
			return true;
		}
		
		else {
			return false;
		}
	}

	public function delete_absensi_ic($data)
	{
		$this->db->delete('daftar_kehadiran',$data);		
		return true;
	}

	/* Untuk Log In */
	public function select_account($data)
	{
		$this->db->where($data);
		$this->db->where(['tanggal_berakhir_pelayanan'=>null]);
		$this->db->join('pelayanan','pelayanan.id_pelayanan = tugas_pelayanan.id_pelayanan');
		$this->db->join('jemaat','jemaat.id_jemaat = tugas_pelayanan.id_jemaat');
		$query = $this->db->get('tugas_pelayanan');
		return $query->result();
	}

	public function select_account_list_pelayanan_khusus($data)
	{
		$this->db->where($data);
		$this->db->join('pelayanan','pelayanan.id_pelayanan = tugas_pelayanan.id_pelayanan');
		$this->db->join('jemaat','jemaat.id_jemaat = tugas_pelayanan.id_jemaat');
		$query = $this->db->get('tugas_pelayanan');
		return $query->result();
	}

	public function select_cabang($id='%%')
	{
		$this->db->where(['id_cabang like'=>$id,'nama_cabang!='=>'']);
		$query = $this->db->get('cabang');
		return $query->result();
	}
	
	public function insert_cabang($data)
	{
		$this->db->select("concat('C',lpad(substr(ID_CABANG,2,4)+1, 4, 0)) ID_CABANG");
		$this->db->order_by('id_cabang desc');
		$this->db->limit(1);
		$query = $this->db->get('cabang');
		$id_cabang = $query->row('ID_CABANG');
		if ($id_cabang == null) $id_cabang = 'C0001';
		$data['ID_CABANG'] = $id_cabang;		
		$this->db->insert('cabang',$data);
		return true;
	}
	
	public function update_cabang($data, $id)
	{
		$this->db->update('cabang',$data,"id_cabang = '".$id."'");
		return true;
	}
	
	public function delete_cabang($id)
	{
		$idb = 'D'.substr($id,1,strlen($id)-1);
		$u=$this->db->update('cabang',['nama_cabang'=>null],['id_cabang'=>$id]);
		echo $this->db->last_query(); 
		return true;
	}
	
	public function select_ic($id='%%')
	{
		$this->db->where("no_ic like '".$id."' and nama_ic not like concat('%',NO_IC,'%')");
		$query = $this->db->get('ic');
		return $query->result();
	}
	
	public function insert_ic($data)
	{
		$this->db->select("concat('IC',lpad(substr(NO_IC,3,3)+1, 3, 0)) NO_IC");
		$this->db->order_by('NO_IC desc');
		$this->db->limit(1);
		$query = $this->db->get('IC');
		$no_ic = $query->row('NO_IC');
		if ($no_ic == null) $no_ic = 'IC'.'0'.'0'.'1';
		$data['NO_IC'] = $no_ic;		
		$this->db->insert('IC',$data);
		
		return true;
	}
	
	public function delete_ic($id)
	{
		$this->db->where(['no_ic'=>$id]);
		$query = $this->db->get('IC');
		$nama = $query->result()[0]->NAMA_IC;
		$this->db->update('ic',['nama_ic'=>$id.'-'.$nama],['no_ic'=>$id]);
		echo $this->db->last_query();
		// $this->db->delete('ic',['no_ic'=>$id]);
		return true;
	}
	
	public function update_ic($data, $id)
	{
		$this->db->update('ic',$data,"no_ic = '".$id."'");
		return true;
	}
	
	public function select_kategori_kegiatan($id='%%')
	{
		$this->db->where(['id_kategori like'=>$id]);
		$query = $this->db->get('kategori_kegiatan');
		return $query->result();
	}
	
	public function insert_kategori_kegiatan($data)
	{
		$this->db->select("concat('KK',lpad(substr(ID_KATEGORI,3,3)+1, 3, 0)) ID_KATEGORI");
		$this->db->order_by('id_kategori desc');
		$this->db->limit(1);
		$query = $this->db->get('kategori_kegiatan');
		$id_kategori = $query->row('ID_KATEGORI');
		$id_kategori = 'KK001';
		$data['ID_KATEGORI'] = $id_kategori ;		
		$this->db->insert('kategori_kegiatan',$data);
		return true;
	}
	
	public function update_kategori_kegiatan($data, $id)
	{
		$this->db->update('kategori_kegiatan',$data,"id_kategori= '".$id."'");
		return true;
	}
	
	public function delete_kategori_kegiatan($id)
	{
		$this->db->delete('kategori_kegiatan',['id_kategori'=>$id]);
		return true;
	}
	
	public function select_kegiatan($id='%%', $no_ic='', $kategori='')
	{
		$this->db->join('ic','ic.no_ic=kegiatan.no_ic','left');
		$this->db->join('cabang','cabang.id_cabang=kegiatan.id_cabang','left');
		$this->db->join('pembicara','pembicara.id_pembicara=kegiatan.id_pembicara','left');
		$this->db->join('kategori_kegiatan','kategori_kegiatan.id_kategori=kegiatan.id_kategori','left');
		$this->db->where("id_kegiatan like '".$id."' and nama_kegiatan not like concat('%',id_kegiatan,'%')");
		$this->db->order_by('tanggal_kegiatan desc');
		if ($no_ic!='') $this->db->where(['IC.no_ic like'=>$no_ic]);
		if ($kategori!='') $this->db->where(['kategori_kegiatan.nama_kategori'=>$kategori]);
		$query = $this->db->get('kegiatan');
		return $query->result();
	}
	
	public function insert_kegiatan($data)
	{
		$this->db->select("concat('K',lpad(substr(ID_KEGIATAN,2,4)+1, 4, 0)) ID_KEGIATAN");
		$this->db->order_by('id_kegiatan desc');
		$this->db->limit(1);
		$query = $this->db->get('kegiatan');
		$id_kegiatan = $query->row('ID_KEGIATAN');
		
		if ($id_kegiatan == null) $id_kegiatan = 'K0001';
		$data['ID_KEGIATAN'] = $id_kegiatan;		
		
		$this->db->insert('kegiatan',$data);
		return true;
	}
	
	public function update_kegiatan($data, $id)
	{
		if ($data['no_ic']=='') $data['no_ic']=NULL;
		if ($data['id_cabang']=='') $data['id_cabang']=NULL;
		$this->db->update('kegiatan',$data, "id_kegiatan = '".$id."'");
		return true;
	}
	
	public function delete_kegiatan($id)
	{
		$this->db->where(['id_kegiatan'=>$id]);
		$query = $this->db->get('kegiatan');
		$nama = $query->result()[0]->NAMA_KEGIATAN;
		$this->db->update('kegiatan',['nama_kegiatan'=>$id.'-'.$nama],['id_kegiatan'=>$id]);
		// $this->db->delete('kegiatan',['id_kegiatan'=>$id]);
		return true;
	}
	
	public function select_pembicara($id='%%')
	{
		$this->db->where(['id_pembicara like'=>$id]);
		$query = $this->db->get('pembicara');
		return $query->result();
	}
	
	public function select_inventaris($id='%%',$keadaan_barang='')
	{
		$this->db->join('cabang','cabang.id_cabang = inventaris.id_cabang');
		$this->db->where(['kode_barang like'=>$id,'keadaan_barang!='=>'SR']);
		if ($keadaan_barang!='') $this->db->where(['keadaan_barang!='=>'KB']);
		$query = $this->db->get('inventaris');
		return $query->result();
	}
	
	
	public function insert_inventaris($data)
	{
		$this->db->select("concat('KB',lpad(substr(KODE_BARANG,3,4)+1, 4, 0)) KODE_BARANG");
		$this->db->order_by('KODE_BARANG desc');
		$this->db->limit(1);
		$query = $this->db->get('INVENTARIS');
		$kode_barang = $query->row('KODE_BARANG');
		if($kode_barang==null) $kode_barang = 'KB001';
		$data['KODE_BARANG'] = $kode_barang;		
		$this->db->insert('INVENTARIS',$data);
		
		return true;
	}
	
	public function delete_inventaris($id)
	{
		$this->db->update('inventaris',['keadaan_barang'=>'SR'],['kode_barang'=>$id]);
		// $this->db->delete('inventaris',['kode_barang'=>$id]);
		return true;		
	}
	
	public function update_inventaris($data, $id)
	{
		$this->db->update('inventaris',$data,"kode_barang = '".$id."'");
		return true;
	}
	
	
	public function select_jemaat2($id='%%')
	{
		
		$this->db->where(['id_jemaat like'=>$id]);
		$query = $this->db->get('jemaat');
		return $query->result();
	}

	public function select_jemaat3($id)
	{
		$this->db->where(['id_jemaat like'=>$id]);
		$query = $this->db->get('jemaat');
		return $query->result();
	}

	public function select_jemaat4($id)
	{
		$this->db->where_not_in('id_jemaat',$id);
		$query = $this->db->get('jemaat');
		return $query->result();
	}

	public function select_hubkel($id)
	{
		$this->db->join('jemaat', 'jemaat.id_jemaat = hubungan_keluarga.id_jemaat_bersangkutan');
		$this->db->where('hubungan_keluarga.id_jemaat like',$id);
		$query = $this->db->get('hubungan_keluarga');
		return $query->result();
	}

	
	public function insert_jemaat($data)
	{
		$this->db->select("concat('J',lpad(substr(ID_JEMAAT,2,4)+1, 4, 0)) ID_JEMAAT");
		$this->db->order_by('ID_JEMAAT desc');
		$this->db->limit(1);
		$query = $this->db->get('JEMAAT');
		$id_jemaat = $query->row('ID_JEMAAT');
		if($id_jemaat==null)$id_jemaat="J0001";
		$data['foto'] = file_get_contents($data['foto']);
		$data['id_jemaat'] = $id_jemaat;
		$this->db->insert('JEMAAT',$data);
		// var_dump($data['foto']);
		return true;
	}

	public function update_jemaat($data, $id)
	{
		//$data['foto'] = file_get_contents($data['foto']);
		$this->db->update('jemaat',$data,"id_jemaat = '".$id."'");
		return true;
	}

	public function insert_hubkel($data, $id)
	{
		$data['id_jemaat'] = $id;
		var_dump($data);
		$this->db->insert('hubungan_keluarga',$data);
		return true;
	}

	public function delete_hubkel($id, $id2)
	{
		$this->db->where(['id_jemaat_bersangkutan like'=>$id]);
		$this->db->where(['id_jemaat like'=>$id2]);
		$this->db->delete('hubungan_keluarga');		
		return true;
	}

	public function insert_pembicara($data)
	{
		$this->db->select("concat('PB',lpad(substr(ID_PEMBICARA,3,5)+1, 4, 0)) ID_PEMBICARA ");
		$this->db->order_by('ID_PEMBICARA desc');
		$this->db->limit(1);
		$query = $this->db->get('PEMBICARA');
		$id_jemaat = $query->row('ID_PEMBICARA');
		$data['id_pembicara'] = $id_jemaat;
		$this->db->insert('PEMBICARA',$data);
		return true;
	}
		

	public function update_pembicara($data, $id)
	{
		$this->db->update('pembicara',$data,"id_pembicara = '".$id."'");
		return true;
	}
	
	//insert maintenance
	public function insert_maintenance($data)
	{
		$this->db->select("concat('M',lpad(substr(ID_MAINTENANCE,3,5)+1, 4, 0)) ID_MAINTENANCE ");
		$this->db->order_by('ID_MAINTENANCE desc');
		$this->db->limit(1);
		$query = $this->db->get('MAINTENANCE');
		$id = $query->row('ID_MAINTENANCE');
		$data['id_maintenance'] = $id;
		$this->db->insert('MAINTENANCE',$data);
		return true;
	}

	

	// public function select_inventaris_no_maintenance()
	// {
	// 	$this->db->join('maintenance m','m.kode_barang = i.kode_barang','right');
	// 	$query = $this->db->get('inventaris i');
	// 	return $query->result();
	// }

	public function select_petugas_maintenance()
	{
		$this->db->join('jemaat j','j.id_jemaat = t.id_jemaat','right');
		$this->db->select('j.ID_JEMAAT, j.NAMA_JEMAAT');
		$this->db->group_by('t.ID_JEMAAT');
		$query = $this->db->get('tugas_pelayanan t');
		return $query->result();
	}

	public function select_maintenance_penanggung_jawab($id)
	{
		$this->db->select('INVENTARIS.NAMA_ATAU_JENIS_BARANG, MAINTENANCE.*');//codingan timo
		$this->db->join('INVENTARIS', 'INVENTARIS.KODE_BARANG = MAINTENANCE.KODE_BARANG');
		$this->db->where('ID_JEMAAT',$id);
		$query = $this->db->get('MAINTENANCE');
		return $query->result();
	}

	public function select_maintenance($id)
	{
		$this->db->select('INVENTARIS.NAMA_ATAU_JENIS_BARANG, MAINTENANCE.*');
		$this->db->join('INVENTARIS', 'INVENTARIS.KODE_BARANG = MAINTENANCE.KODE_BARANG');
		$this->db->where('ID_MAINTENANCE',$id);
		$query = $this->db->get('MAINTENANCE');
		return $query->result();
	}

	public function select_maintenance2()
	{
		$this->db->select('INVENTARIS.NAMA_ATAU_JENIS_BARANG, JEMAAT.NAMA_JEMAAT, MAINTENANCE.*');
		$this->db->join('INVENTARIS', 'INVENTARIS.KODE_BARANG = MAINTENANCE.KODE_BARANG');
		$this->db->join('JEMAAT', 'JEMAAT.ID_JEMAAT = MAINTENANCE.ID_JEMAAT');
		$query = $this->db->get('MAINTENANCE');
		return $query->result();
	}

	public function update_tanggal_maintenance($data, $id)
	{
		$this->db->update('maintenance',$data,"id_maintenance = '".$id."'");
		return true;
	}

	public function update_maintenance($data, $id)
	{
		$this->db->update('maintenance',$data,"id_maintenance = '".$id."'");
		return true;
	}
	
	//select barang punya timo
	//punya timo bukan punya samuel
	public function select_barang_punya_timo(){
		$this->db->join("inventaris","inventaris.kode_barang = maintenance.kode_barang");
		return $this->db->get('maintenance')->result(); 
	}

	public function select_pelayanan($data = [])
	{
		$this->db->where($data);
		$query = $this->db->get('pelayanan');
		return $query->result();
	}

	public function insert_pelayanan($data)
	{
		$this->db->where($data);
		$query = $this->db->get('pelayanan');
		if(count($query->result())>0) return false;
		
		$this->db->select("concat('P',lpad(substr(ID_PELAYANAN,4,3)+1, 4, 0)) ID_PELAYANAN");
		$this->db->order_by('id_pelayanan desc');
		$query = $this->db->get('pelayanan');
		$result = $query->result();
		$data['id_pelayanan']=$result[0]->ID_PELAYANAN;
		$this->db->insert('pelayanan',$data);
		return true;
	}
	public function update_pelayanan($data)
	{
		return $this->db->update('pelayanan',$data,['id_pelayanan'=>$data['id_pelayanan']]);
	}
	public function select_jemaat_where_tugas_pelayanan($data)
	{
		date_default_timezone_set("Asia/Bangkok");
		$tgl = Date('Y').'-'.Date('m').'-'.Date('d');

		$this->db->where($data);
		$this->db->where(["tanggal_berakhir_pelayanan"=>null]);
		$this->db->group_by('j.id_jemaat');
		$this->db->join('jemaat j','j.id_jemaat=t.id_jemaat');
		$this->db->join('pelayanan p','p.id_pelayanan=t.id_pelayanan');
		$query = $this->db->get('tugas_pelayanan t');		
		return $query->result();
	}
	public function delete_petugas_pelayanan($data)
	{
		$id_jemaat = $data['id_jemaat'];
		$id_pelayanan = $data['id_pelayanan'];
		date_default_timezone_set("Asia/Bangkok");
		$tgl = Date('Y').'-'.Date('m').'-'.Date('d');
		$this->db->update('tugas_pelayanan',["tanggal_berakhir_pelayanan"=>$tgl],"id_jemaat = '$id_jemaat' and id_pelayanan = '$id_pelayanan'");
	}
	public function insert_petugas_pelayanan($data)
	{
		$query = $this->db->update('tugas_pelayanan',["tanggal_berakhir_pelayanan"=>null],['id_pelayanan'=>$data['id_pelayanan'],'id_jemaat'=>$data['id_jemaat']]);

		if($this->db->affected_rows()>=1) return true;

		$this->db->from('tugas_pelayanan t, pelayanan p');
		$this->db->where("
			id_jemaat = '".$data['id_jemaat']."' and 
			t.id_pelayanan = p.id_pelayanan and  
			(lower(nama_pelayanan) like '%admin%' or lower(nama_pelayanan) like '%ketua%')
		");
		$query = $this->db->get();

		$result = $query->result();

		var_dump($result);
		if (count($result)>0) {
			$id_pelayanan = $result[0]->ID_PELAYANAN;
			$this->db->where(['id_pelayanan'=>$data['id_pelayanan']]);
			$query = $this->db->get('pelayanan');
			$result = $query->result();
			var_dump($result);
			
			if(strpos(strtolower($result[0]->NAMA_PELAYANAN),'admin') > -1||strpos(strtolower($result[0]->NAMA_PELAYANAN),'ketua')>-1){
				$query = $this->db->update('tugas_pelayanan',["id_pelayanan"=>$data['id_pelayanan']],['id_pelayanan'=>$id_pelayanan,'id_jemaat'=>$data['id_jemaat']]);
				return false;
			}
		}

		$data['password']=md5($data['id_jemaat']);
		date_default_timezone_set("Asia/Bangkok");
		$tgl = Date('Y').'-'.Date('m').'-'.Date('d');
		$data['tanggal_mulai_pelayanan']=$tgl;
		$this->db->insert('tugas_pelayanan',$data);		
	}
	public function insert_data_detail_pelayanan($data)
	{
		$this->db->where(['id_kegiatan'=>$data['id_kegiatan'],'id_pelayanan'=>$data['id_pelayanan']]);
		$query = $this->db->get('detail_pelayan_khusus');
		
		var_dump($query->result());
		if(count($query->result())>0) return false;
		unset($data['files']);
		$query = $this->db->insert('detail_pelayan_khusus',$data);	
	}
	public function delete_data_detail_pelayanan($data)
	{
		$query = $this->db->delete('detail_pelayan_khusus',$data);		
	}
	public function select_data_detail_pelayanan_kegiatan($data)
	{
		$this->db->where($data);
		$this->db->join('detail_pelayan_khusus d','d.id_kegiatan=k.id_kegiatan');
		$this->db->join('pelayanan p','p.ID_PELAYANAN=d.ID_PELAYANAN');
		$query = $this->db->get('kegiatan k');

		return $query->result();
	}
	public function cekKegiatanIbadahUmumSesuaiTGL($tgl, $wkt)
	{
		$this->db->where(['TANGGAL_KEGIATAN'=>$tgl,'WAKTU_KEGIATAN >= '=>$wkt]);
		$query = $this->db->get('kegiatan');

		$result = $query->result();
		if (count($result)==0) return null;
		else return $result[0]->ID_KEGIATAN;
	}
}