<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelLaporanKeuangan extends CI_Model {
	public function __construct()
	{
		//model untuk laporan keuangan
		parent::__construct();
		$this->load->database();
	}
	
	
	static function cekTanggal($data){
		$tanggal = $data['tahun1'].'-'.$data['bulan1'].'-'.$data['hari1'];
		$tanggal2 = $data['tahun2'].'-'.$data['bulan2'].'-'.$data['hari2'];
		$hasilcek = [];
		$hasilcek["tanggal"] = $tanggal;
		$hasilcek["tanggal2"] = $tanggal2;
		$hasilcek["format"]='';
		$hasilcek["select"]="";
		$hasilcek["groupby"]='';
		$hasilcek["where"]='';
		if($data['tahun2']!=0){
			$hasilcek["where"]='tahun2';
			if($data['hari1']!=0){
				$hasilcek["format"]='%Y-%m-%d';
				$hasilcek["select"]="concat(day(tanggal),' ',monthname(tanggal),' ',year(tanggal)) label,";
				$hasilcek["groupby"]='year(tanggal),month(tanggal),day(tanggal)';
			}
			else if($data['bulan1']!=0){
				$hasilcek["format"]='%Y-%m';
				$hasilcek["select"]="concat(monthname(tanggal),' ',year(tanggal)) label,";
				$hasilcek["groupby"]='year(tanggal),month(tanggal)';
			}
			else{
				$hasilcek["format"]='%Y';
				$hasilcek["select"]='year(tanggal) label,';
				$hasilcek["groupby"]='year(tanggal)';
			}
		}
		else if($data['tahun1']!=0){
			$hasilcek["where"]='tahun1';
			if($data['hari1']!=0){
				$hasilcek["format"]='%Y-%m-%d';
				$hasilcek["select"]="concat(day(tanggal),' ',monthname(tanggal),' ',year(tanggal)) label,";
				$hasilcek["groupby"]='year(tanggal),month(tanggal),day(tanggal)';
			}
			else if($data['bulan1']!=0){
				$hasilcek["format"]='%Y-%m';
				$hasilcek["select"]="concat(day(tanggal),' ',monthname(tanggal)) label,";
				$hasilcek["groupby"]='month(tanggal),day(tanggal)';
			}
			else{
				$hasilcek["format"]='%Y';
				$hasilcek["select"]="concat(monthname(tanggal),' ',year(tanggal)) label,";
				$hasilcek["groupby"]='year(tanggal),month(tanggal)';
			}
		}
		return $hasilcek;
	}
	//query awal
	static function query($data){
		
		$CI =& get_instance();
		$CI->db->select($data['select'].' coalesce(sum(nominal_uang),0) uang');
		$CI->db->from("((select `jenis_transaksi`, `kategori`, `nominal_uang`, `tanggal` from keuangan) union all select NULL jenis_transaksi, coalesce(NULL) kategori, coalesce(NULL) nominal_uang, adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) tanggal from
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) uang");
		
		
		
		if($data['where']=='tahun2'){
			$CI->db->where("date_format(tanggal,'".$data['format']."') between date_format('".$data['tanggal']."','".$data['format']."') and date_format('".$data['tanggal2']."','".$data['format']."')");
		}
		else if($data['where']=='tahun1'){
			$CI->db->where("date_format(tanggal,'".$data['format']."') = date_format('".$data['tanggal']."','".$data['format']."')");
		}
		$CI->db->group_by($data['groupby']);
	}
	
	static function queryBalance($data,$where){
		
		$CI =& get_instance();
		$CI->db->select('coalesce(sum(nominal_uang),0) uang');
		$CI->db->from('keuangan');
		
		
		
		if($data['where']=='tahun2'){
			$CI->db->where("date_format(tanggal,'".$data['format']."') between date_format('".$data['tanggal']."','".$data['format']."') and date_format('".$data['tanggal2']."','".$data['format']."')");
		}
		else if($data['where']=='tahun1'){
			$CI->db->where("date_format(tanggal,'".$data['format']."') = date_format('".$data['tanggal']."','".$data['format']."')");
		}
		$CI->db->where("jenis_transaksi ='".$where."'");
		$query = $CI->db->get();
		//print_r($CI->db->last_query());
		return $query->result_array();
	}
	
	static function queryKas($data,$where){
		
		$CI =& get_instance();
		$CI->db->select('coalesce(sum(nominal_uang),0) uang');
		$CI->db->from('keuangan');
		
		
		
		if($data['where']=='tahun2'){
			$CI->db->where("date_format(tanggal,'".$data['format']."') <= date_format('".$data['tanggal2']."','".$data['format']."')");
		}
		else if($data['where']=='tahun1'){
			$CI->db->where("date_format(tanggal,'".$data['format']."') <= date_format('".$data['tanggal']."','".$data['format']."')");
		}
		$CI->db->where("jenis_transaksi ='".$where."'");
		$query = $CI->db->get();
		//print_r($CI->db->last_query());
		return $query->result_array();
	}
	
	//jenis transaksi
	//bisa 'K' atau 'D'
	public function jenisTransaksi($data,$jenis){
		$dataTanggal = self::cekTanggal($data);
		self::query($dataTanggal);
		$this->db->where("(jenis_transaksi ='".$jenis."' or jenis_transaksi is null)");
		
		$query = $this->db->get();
		return $query->result_array();
	}
	//kategori transaksi
	public function kategoriTransaksi($data,$kategori){
		$dataTanggal = self::cekTanggal($data);
		self::query($dataTanggal);
		$this->db->where("(kategori ='".$kategori."' or kategori is null)");
		
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function balance($data){
		$balance = [];
		$dataTanggal = self::cekTanggal($data);
		$keluar = self::queryBalance($dataTanggal,'K');
		$masuk = self::queryBalance($dataTanggal,'D');
		$balance['total_pengeluaran'] = $keluar[0]['uang'];
		$balance['total_pemasukkan'] = $masuk[0]['uang'];
		$balance['total_balance'] = (int)$balance['total_pemasukkan'] - (int)$balance['total_pengeluaran'];
		$balance['total_kas'] = (int)self::queryKas($dataTanggal,'D')[0]['uang'] - (int)self::queryKas($dataTanggal,'K')[0]['uang'];
		return $balance;
	}
	//kategori lainnya bisa dicopi dari kolekte
	
}