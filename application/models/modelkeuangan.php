<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelKeuangan extends CI_Model {
	//list kategori
	//kemungkinan dibagi dua untuk kredit dan debit
	//hanya data dummy, kemungkinan kategori harus lebih banyak
	//tanya pak ronny?
	static $data_debit= array("Kolekte","Persepuluh","Sumbangan");//debit
	static $data_kredit= array("Maintenance","Peralatan","Perlengkapan","Travel","Konsumsi","Kesehatan","Listrik","Air","Penginapan","Sedekah");//kredit
	
	public function __construct()
	{
		//model untuk keuangan
		parent::__construct();
		$this->load->database();
	}
	
	//select uang
	public function select($id = ""){
		$test = "SELECT U.ID_KEUANGAN,(SELECT NAMA_JEMAAT FROM JEMAAT J WHERE J.ID_JEMAAT = U.ID_PENANGGUNG_JAWAB) AS TANGGUNG,";
		$test .= " (SELECT NAMA_JEMAAT FROM JEMAAT WHERE ID_JEMAAT = U.ID_PEMASUKKAN) AS MASUK,C.NAMA_CABANG, U.ID_MAINTENANCE,";
		$test .= " I.NAMA_ATAU_JENIS_BARANG, U.JENIS_TRANSAKSI, U.KATEGORI, U.NOMINAL_UANG, U.DESKRIPSI, U.TANGGAL,";
		$test .= " U.ID_PENANGGUNG_JAWAB, I.KODE_BARANG, C.ID_CABANG";//untuk user friendly update
		$test .= " FROM KEUANGAN U LEFT JOIN CABANG C ON U.ID_CABANG = C.ID_CABANG ";
		$test .= " LEFT JOIN MAINTENANCE M ON U.ID_MAINTENANCE = M.ID_MAINTENANCE";
		$test .= " LEFT JOIN INVENTARIS I ON U.KODE_BARANG = I.KODE_BARANG";
		if($id != ""){$test .= " WHERE U.ID_KEUANGAN = '".$id."'";}
		$test .= " ORDER BY U.TANGGAL";
		$query = $this->db->query($test);
		
		return $query->result();
	}
	
	//insert uang
	public function insert($data = ""){
		$this->db->select("concat('KU',lpad(substr(ID_KEUANGAN,3,9)+1, 8, 0)) ID_UANG");
		$this->db->order_by('id_keuangan desc');
		$this->db->limit(1);
		$query = $this->db->get('keuangan');
		$id = $query->row('ID_UANG');
		if($id==null)$id = 'KU001';
		$data['id_keuangan'] = $id;		
		
		if ($data['id_maintenance']=='') $data['id_maintenance']=NULL;
		if ($data['id_cabang']=='') $data['id_cabang']=NULL;
		if ($data['kode_barang']=='') $data['kode_barang']=NULL;
		
		$data['id_pemasukkan'] = $this->session->userdata('ID_JEMAAT');
		
		$this->db->insert('keuangan',$data);
		return true;
	}
	
	//update uang
	public function update($data = "",$id = ""){
		if ($data['id_maintenance']=='') $data['id_maintenance']=NULL;
		if ($data['id_cabang']=='') $data['id_cabang']=NULL;
		if ($data['kode_barang']=='') $data['kode_barang']=NULL;
		$data['id_pemasukkan'] = $this->session->userdata('ID_JEMAAT');
		$this->db->update('keuangan',$data, "id_keuangan = '".$id."'");
		return true;
	}
}