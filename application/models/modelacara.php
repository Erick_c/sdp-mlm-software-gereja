<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelAcara extends CI_Model {
	public function __construct()
	{
		//model untuk acara
		parent::__construct();
		$this->load->database();
	}
	//mencari list acara
	//mungkin dibuat compatible untuk paging
	
	public function countAcara($baru = false,$search){
		$this->db->from('kegiatan');
		$this->db->join('ic','ic.no_ic=kegiatan.no_ic','left');
		$this->db->join('cabang','cabang.id_cabang=kegiatan.id_cabang','left');
		$this->db->join('pembicara','pembicara.id_pembicara=kegiatan.id_pembicara','left');
		$this->db->join('kategori_kegiatan','kategori_kegiatan.id_kategori=kegiatan.id_kategori','left');
		if($search['tanggal_acara'] != ''){ $this->db->where("date_format(tanggal_kegiatan,'%Y-%m-%d')","date_format('".$search['tanggal_acara']."','%Y-%m-%d')",false);}
		if($search['lokasi_acara'] != ''){$this->db->where('kegiatan.lokasi_kegiatan like','"%'.$search['lokasi_acara'].'%"',false);}
		if($search['kata_yang_terkait'] != ''){$this->db->where('kegiatan.nama_kegiatan like','"%'.$search['kata_yang_terkait'].'%"',false);}
		$this->db->where('kategori_kegiatan.nama_kategori != "Ibadah Umum" AND kategori_kegiatan.nama_kategori != "Ibadah IC"');
		$this->db->where("kegiatan.nama_kegiatan not like concat('%',kegiatan.id_kegiatan,'%')");
		if($baru){$this->db->where("kegiatan.tanggal_kegiatan > sysdate()");}
		$this->db->order_by("TANGGAL_KEGIATAN","DESC");
		$query = $this->db->get();
		return $query->num_rows();
	}	

	public function listAcara($baru = false,$limit = 0,$page = 0,$search){
		$this->db->from('kegiatan');
		$this->db->join('ic','ic.no_ic=kegiatan.no_ic','left');
		$this->db->join('cabang','cabang.id_cabang=kegiatan.id_cabang','left');
		$this->db->join('pembicara','pembicara.id_pembicara=kegiatan.id_pembicara','left');
		$this->db->join('kategori_kegiatan','kategori_kegiatan.id_kategori=kegiatan.id_kategori','left');
		if($search['tanggal_acara'] != ''){ $this->db->where("date_format(tanggal_kegiatan,'%Y-%m-%d')","date_format('".$search['tanggal_acara']."','%Y-%m-%d')",false);}
		if($search['lokasi_acara'] != ''){$this->db->where('kegiatan.lokasi_kegiatan like','"%'.$search['lokasi_acara'].'%"',false);}
		if($search['kata_yang_terkait'] != ''){$this->db->where('kegiatan.nama_kegiatan like','"%'.$search['kata_yang_terkait'].'%"',false);}
		$this->db->where('kategori_kegiatan.nama_kategori != "Ibadah Umum" AND kategori_kegiatan.nama_kategori != "Ibadah IC"');
		$this->db->where("kegiatan.nama_kegiatan not like concat('%',kegiatan.id_kegiatan,'%')");
		if($baru){$this->db->where("kegiatan.tanggal_kegiatan > sysdate()");}
		$this->db->order_by("TANGGAL_KEGIATAN","DESC");
		if($limit != 0){
			if($page != 0){
			$this->db->limit($limit,$page);
			}
			else{
				$this->db->limit($limit);
			}
		}
		$query = $this->db->get();
		return $query->result();
	}
	
	public function cariAcara($id_kegiatan=""){
		$this->db->from('kegiatan');
		$this->db->join('ic','ic.no_ic=kegiatan.no_ic','left');
		$this->db->join('cabang','cabang.id_cabang=kegiatan.id_cabang','left');
		$this->db->join('pembicara','pembicara.id_pembicara=kegiatan.id_pembicara','left');
		$this->db->join('kategori_kegiatan','kategori_kegiatan.id_kategori=kegiatan.id_kategori','left');
		$this->db->where('kegiatan.id_kegiatan',$id_kegiatan);
		$query = $this->db->get();
		return $query->result();
	}
	
	//cari pelayannya
	public function cariPelayan($id_kegiatan){
		$this->db->from("detail_pelayan_khusus");//mungkin lebih baik jemaat daripada jenis pelayanan
		$this->db->join('tugas_pelayanan','detail_pelayan_khusus.id_pelayanan=tugas_pelayanan.id_pelayanan');
		$this->db->join('pelayanan','tugas_pelayanan.id_pelayanan=pelayanan.id_pelayanan');
		$this->db->join('jemaat','tugas_pelayanan.id_jemaat=jemaat.id_jemaat');
		$this->db->where('detail_pelayan_khusus.id_kegiatan',$id_kegiatan);
		
		//sudah mikir tapi mungkin hasil tidak pas
		//ya sudah waktunya
		$query = $this->db->get();
		return $query->result();
	}
	
	//cari pelayanan yang ada di kegiatan tersebut
	public function caripelayanan($id_kegiatan){
		$this->db->select('detail_pelayan_khusus.id_pelayanan, pelayanan.nama_pelayanan NAMA_PELAYANAN');
		$this->db->from("detail_pelayan_khusus");
		$this->db->join('pelayanan','detail_pelayan_khusus.id_pelayanan=pelayanan.id_pelayanan');
		$this->db->where('id_kegiatan',$id_kegiatan);
		
		$query = $this->db->get();
		return $query->result();
	}
}