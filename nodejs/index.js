// Download the helper library from https://www.twilio.com/docs/node/install
// Your Account Sid and Auth Token from twilio.com/console
// DANGER! This is insecure. See http://twil.io/secure
const accountSid = 'AC2015be8f18f3364faa0f19478bac4e74';
const authToken = 'd612470edd9dcd9c8c309254f4a4ee48';
const client = require('twilio')(accountSid, authToken);
const express = require("express");
const app = express();
const mysql = require("mysql");

const pool = mysql.createPool({
    connectionLimit : 10,
    host :"localhost",
    database :"gereja",
    user :"root",
    password :""
});

app.get("/", function(req, res){
    pool.getConnection(function(err,conn){
        if(err) throw err;

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0');
        var yyyy = today.getFullYear();
        var today = mm + '-' + dd;
        
        
        const q = `select NAMA_JEMAAT, TANGGAL_LAHIR, NO_TELPON from jemaat where tanggal_lahir like '%${today}%';`
        conn.query(q, function(err, result){
            conn.release();
            if(err) throw err;
            res.send(result);
            for(let i =0; i< result.length;i++){
                
                var str = result[i].TANGGAL_LAHIR;
                var tahunlahir = str.getFullYear();
                var telp = result[i].NO_TELPON;
                var nama = result[i].NAMA_JEMAAT;
                var umur = yyyy - tahunlahir;
                

                client.messages
              .create({
                 body: 'Selamat Ulang Tahun '+ nama + ' yang ke-' + umur + ' dari Gereja Bethel Tabernakel, Tuhan Memberkati',
                 from: '+16165223433',
                 to: '+62'+ telp
               })
              .then(message => console.log(message.sid));
            }
        })
    })
});

app.listen(3000, function(){
    console.log("listening on port 3000...")
    
});